const fs = require('fs');

fs.copyFile('robots.txt', 'dist/browser/robots.txt', (err) => {
  if (err) throw err;
  console.log('robots.txt was copied to dist/browser');
});

fs.copyFile('google510afffe8475800f.html', 'dist/browser/google510afffe8475800f.html', (err) => {
  if (err) throw err;
  console.log('google.html was copied to dist/browser');
});

fs.copyFile('yandex_661cfdbc34aef258.html', 'dist/browser/yandex_661cfdbc34aef258.html', (err) => {
  if (err) throw err;
  console.log('yandex1.html was copied to dist/browser');
});

fs.copyFile('yandex_be45b2d5c83629e8.html', 'dist/browser/yandex_be45b2d5c83629e8.html', (err) => {
  if (err) throw err;
  console.log('yandex_2 was copied to dist/browser');
});

fs.copyFile('sp-push-manifest.json', 'dist/browser/sp-push-manifest.json', (err) => {
  if (err) throw err;
  console.log('sp-push-manifest.json was copied to dist/browser');
});

fs.copyFile('sp-push-worker-fb.js', 'dist/browser/sp-push-worker-fb.js', (err) => {
  if (err) throw err;
  console.log('sp-push-worker-fb.js was copied to dist/browser');
});


