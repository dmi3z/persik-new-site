import { ModalController } from './../../services/modal.controller';
import { LocalStorageService } from '../../services/media-state/local-storage.service';
import { Router } from '@angular/router';
import { ContentType, ContentName } from '../../models/vod';
import { Component, ViewChild, ElementRef, EventEmitter, OnInit, OnDestroy, Input } from '@angular/core';
import * as Hls from 'hls.js';
import {
  AuthService,
  DataService,
  ImageService,
  TimeService,
  PaymentService

} from '../../services';
import { PlayerEvents } from '../../models/player';
import { Channel, ModalName } from '../../models';

declare var ya: any;

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss']
})

export class PlayerComponent implements OnInit, OnDestroy {

  @Input() isAlartNeeded = true;
  @ViewChild('video') video: ElementRef;

  public isShowLoginError = false;
  public isShowSubscribeError = false;
  public errorMessage = '';
  public contentPoster: string;
  public playerEvents: EventEmitter<string> = new EventEmitter<string>();

  private hls: any;
  private player: HTMLVideoElement;
  private video_id: number | string;
  private contentType: string;
  public channel: Channel;

  constructor(
    private authService: AuthService,
    private dataService: DataService,
    private imageService: ImageService,
    private timeService: TimeService,
    private router: Router,
    private paymentService: PaymentService,
    private localStorageService: LocalStorageService,
    private modalController: ModalController
  ) { }

  ngOnInit() {
    this.player = this.video.nativeElement;
    this.hls = new Hls();

    /* if (this.player) {
      const containerTag = document.getElementsByClassName('player')[0];
      console.log(containerTag);
      ya.videoAd.loadModule('AdSDK').then((m) => {
        m.initForVideoNode(
          {
            partnerId: 290692,
            category: 1,
          },
          this.player,
          containerTag,
          (publicController) => {
            console.log('playerInited: ', publicController);
          },
          (error) => {
            console.error('playerError: ', error.message);
          },
        );
      });
    } */


    /* ya.videoAd.loadModule('AdLoader')
      .then((mod) => {
        return mod.AdLoader.create({ partnerId: 290692, category: 1 });
      })
      .then((adLoader) => {
        return adLoader.loadAd();
      })
      .then((adViewer) => {
        const player = document.getElementById('video_player');
        const slot = document.getElementsByClassName('player')[0];
        adViewer.showAd(player, slot);
      })
      .then((adViewer) => {
        adViewer.preload({
          videoSlot: this.player,
          desiredBitrate: 1000,
        });

        return new Promise((resolve) => {
          resolve(adViewer);
        });
      })
      .then((adViewer) => {
        const player = document.getElementById('video_player');
        const slot = document.getElementsByClassName('player')[0];
        const adPlaybackController = adViewer.createPlaybackController(player, slot);
        adPlaybackController.subscribe('AdStopped', function () {
          console.log('Ad stopped playing');
        });
        adPlaybackController.playAd();
      })
      .catch((error) => {
        console.error(error);
      }); */
  }

  public showFastLoginModal(): void {
    this.modalController.present(ModalName.FAST_AUTH);
  }

  public play(id: number | string, type: ContentType) {
    this.video_id = id;
    this.contentType = type;
    if (this.localStorageService.getData(this.video_id)) {
      const currentVideoMediaState = this.localStorageService.getData(this.video_id);
      this.seek(currentVideoMediaState.seek);
    }
    if (this.isAuthorized) {
      this.isShowLoginError = false;
      switch (type) {
        case ContentName.CHANNEL:
          this.playChannel(+id);
          this.dataService.loadChannel(+id).then(res => {
            this.channel = res;
          });
          break;

        case ContentName.VIDEO:
          this.playVideo(+id);
          break;

        case ContentName.TV:
          this.playTvshow(id.toString());
          break;

        case ContentName.RADIO:
          this.dataService.loadChannel(+id).then(res => {
            this.channel = res;
          });
          this.playRadio(+id);
          break;

        default:
          break;
      }
    } else {
      this.contentPoster = this.imageService.getChannelFrame(+id, this.timeService.currentTime, 'crop', 1920, 1080);
      this.errorMessage = `Для просмотра ${type === 'channel' ? 'каналов' : 'видео'} необходимо авторизоваться`;
      this.isShowLoginError = true;
      this.playerEvents.emit(PlayerEvents.PLAYER_ERROR_LOGIN);
    }
  }

  public get isRadio(): boolean {
    return this.contentType === ContentName.RADIO;
  }

  public stop() {
    if (this.hls) {
      this.hls.stopLoad();
      this.hls.detachMedia();
      this.destroyHls();
    }
    this.pause();
    this.player.src = null;
  }

  public seek(value: number) {
    this.player.currentTime = value;
  }

  public get volume() {
    return this.player.volume;
  }

  public set volume(level) {
    this.player.volume = level;
  }

  public get duration(): number {
    return this.player.duration;
  }

  public get currentTime(): number {
    return this.player.currentTime;
  }

  public redirectToAuthPage(): void {
    this.router.navigate(['private/user/profile']);
  }

  public redirectToSubscribePage(): void {
    if (this.video_id && this.contentType !== ContentName.CHANNEL) {
      this.dataService.getVideoInfo(this.video_id).then(res => {
        if (res.category_id === 6) {
          this.authService.getTariffs().then(tariffs => {
            const product = tariffs.products.find(prod => prod.product_id === res.in_products[0].product_id);
            const option = product.options[0];

            this.paymentService.setTariff(option, option.name);
            this.router.navigate(['private/payment']);
          });
        } else {
          this.router.navigate(['private/payment']);
        }
      });
    } else {
      this.router.navigate(['private/tv-tarify']);
    }
  }

  public resume(): void {
    this.player.play();
  }

  public pause(): void {
    this.player.pause();
  }

  private destroyHls() {
    if (this.hls) {
      this.hls.destroy();
      const tmpHls = this.hls;
      setTimeout(() => {
        if (tmpHls.abrController && tmpHls.abrController._bwEstimator) {
          tmpHls.abrController._bwEstimator.hls = null;
        }
        if (tmpHls.coreComponents && tmpHls.coreComponents.length) {
          tmpHls.coreComponents.forEach((component) => {
            component.hls = null;
            component.media = null;
            component.cea608Parser = null;
          });
        }
        if (tmpHls.networkControllers && tmpHls.networkControllers.length) {
          tmpHls.networkControllers.forEach((component) => {
            component.hls = null;
          });
        }
        tmpHls.coreComponents = null;
        tmpHls.networkControllers = null;
      }, 5000);
      this.hls = null;
    }
  }

  private playChannel(id: number): void {
    this.contentPoster = this.imageService.getChannelFrame(+id, this.timeService.currentTime, 'crop', 1920, 1080);
    this.isShowSubscribeError = false;
    this.dataService.getChannelStream(id).then(res => {
      this.playUrl(this.parseStreamUrl(res.stream_url));
    }).catch(() => {
      this.playerEvents.emit(PlayerEvents.PLAYER_ERROR_SUBSCRIPTION); // Нет подписки
      this.isShowSubscribeError = true;
      this.errorMessage = 'Необходимо приобрести подписку';
    });
  }

  private playRadio(id: number): void {
    // this.contentPoster = this.imageService.getChannelFrame(+id, this.timeService.currentTime, 'crop', 1920, 1080);
    this.isShowSubscribeError = false;
    this.dataService.getChannelStream(id).then(res => {
      this.playUrl(this.parseStreamUrl(res.stream_url));
    }).catch(() => {
      this.playerEvents.emit(PlayerEvents.PLAYER_ERROR_SUBSCRIPTION); // Нет подписки
      this.isShowSubscribeError = true;
      this.errorMessage = 'Необходимо приобрести подписку';
    });
  }

  private parseStreamUrl(url: string): string {
    if (url.includes('https')) {
      return url;
    } else {
      let new_url = url.replace('http', 'https');
      new_url = new_url.replace(':82', '');
      return new_url;
    }
  }

  private playTvshow(id: string) {
    this.dataService.getTvshowStream(id).then(res => {
      this.playUrl(this.parseStreamUrl(res.stream_url));
    }).catch(err => {
      if (err.status === 403) {
        this.isShowSubscribeError = true;
        this.errorMessage = 'Необходимо приобрести подписку';
        this.contentPoster = '';
        this.playerEvents.emit(PlayerEvents.PLAYER_ERROR_SUBSCRIPTION);
      }
    });
  }

  private playVideo(id: number): void {
    this.dataService.getVideoStream(id).then(res => {
      this.playUrl(this.parseStreamUrl(res.stream_url));
    }).catch(err => {
      if (err.status === 403) {
        this.isShowSubscribeError = true;
        this.errorMessage = 'Необходимо приобрести подписку';
        this.contentPoster = '';
        this.playerEvents.emit(PlayerEvents.PLAYER_ERROR_SUBSCRIPTION);
      }
    });
  }

  private onPlayerStateChange(): void {
    this.playerEvents.emit(PlayerEvents.PLAYER_READY);
  }

  private onPlayerPause(): void {
    this.playerEvents.emit(PlayerEvents.PLAYER_PAUSE);
  }

  private onPlayerPlay(): void {
    this.playerEvents.emit(PlayerEvents.PLAYER_PLAY);
  }

  private playUrl(url: string) {
    this.hls = new Hls();
    this.hls.loadSource(url);
    this.hls.attachMedia(this.player);
    this.hls.on(Hls.Events.MANIFEST_PARSED, () => {
      this.player.play();
      this.player.addEventListener('loadedmetadata', this.onPlayerStateChange.bind(this));
      this.player.addEventListener('play', this.onPlayerPlay.bind(this));
      this.player.addEventListener('pause', this.onPlayerPause.bind(this));
    });
    this.hls.on(Hls.Events.ERROR, function (e, data) {
      if (data.fatal) {
        switch (data.type) {
          case Hls.ErrorTypes.NETWORK_ERROR:
            // fatal network error encountered, try to recover
            break;
          case Hls.ErrorTypes.MEDIA_ERROR:
            // fatal media error encountered, try to recover
            break;
          default:
            // hls destroy
            this.hls.destroy();
            break;
        }
      }
    }
    );
  }

  private get isAuthorized(): boolean {
    return this.authService.isLogin;
  }

  private get getMediaState(): string {
    let state: string;
    if (this.contentType !== ContentName.CHANNEL) {
      state = (this.currentTime > (this.duration - (this.duration * 0.05))) ? 'finished' : 'inProgress';
      return state;
    } else {
      state = 'channel';
      return state;
    }
  }

  ngOnDestroy() {
    if (this.contentType !== ContentName.CHANNEL) {
      if (this.video_id) {
        this.localStorageService.set({
          id: this.video_id.toString(),
          type: this.contentType,
          // state: (this.currentTime > (this.duration - (this.duration * 0.05))) ? "finished" : "inProgress",
          state: this.getMediaState,
          channel: this.channel,
          seek: this.currentTime,
          timestamp: + new Date()
        });
      }
    }
    this.stop();
    this.player.removeEventListener('loadedmetadata', this.onPlayerStateChange.bind(this));
    this.player.removeEventListener('play', this.onPlayerPlay.bind(this));
    this.player.removeEventListener('pause', this.onPlayerPause.bind(this));
    document.removeEventListener('resume', this.resume.bind(this), false);
    document.removeEventListener('pause', this.pause.bind(this), false);
  }

}
