import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Options } from 'ng5-slider';
import { VideoControlEventInfo, ControlEvents } from '../../../models/player';

@Component({
  selector: 'app-sound-controller',
  templateUrl: './tv-sound-controller.component.html',
  styleUrls: ['./tv-sound-controller.component.scss']
})

export class SoundControllerComponent {

  @Input() volume: number;
  @Output() controllerEvents: EventEmitter<VideoControlEventInfo> = new EventEmitter();

  public volumeOptions: Options = {
    vertical: true,
    showSelectionBar: true,
    floor: 0,
    ceil: 100,
    translate: () => {
      return '';
    }
  };

  constructor() {}

  public volumeChange(value: number): void {
    this.controllerEvents.emit(new VideoControlEventInfo(ControlEvents.CONTROL_VOLUME, value));
  }

}
