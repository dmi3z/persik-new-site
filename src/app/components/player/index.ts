export { PlayerComponent } from './player.component';
export { VideoControllerComponent } from './video-controller/video-controller.component';
export { ChannelControllerComponent } from './channel-controller/channel-controller.component';
export { TvshowControllerComponent } from './tvshow-controller/tvshow-controller.component';
export { TvshowControllerItemComponent } from './tvshow-controller-item/tvshow-controller-item.component';
export { SoundControllerComponent } from './tv-sound-controller/tv-sound-controller.component';
