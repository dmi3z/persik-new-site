import { Channel } from './../../../models/channel';
import { TranslitService } from './../../../services/translit.service';
import { Component, Input, OnInit, OnDestroy, OnChanges } from '@angular/core';
import { Router } from '@angular/router';
import { TimeService, DataService, ImageService } from '../../../services';
import { Video, Genre, Tvshow } from './../../../models';
import { Subscription } from 'rxjs';


@Component({
  selector: 'tvshow-controller-item',
  templateUrl: './tvshow-controller-item.component.html',
  styleUrls: ['./tvshow-controller-item.component.scss']
})

export class TvshowControllerItemComponent implements OnInit, OnDestroy, OnChanges {

  @Input() tvshow: Tvshow;
  public videoInformation: Video;
  public progress: number;
  public cover: string;
  public genres: string[] = [];

  private channel: Channel;
  private timeControllerSubscriber: Subscription;

  constructor(
    private timeService: TimeService,
    private dataService: DataService,
    private imageService: ImageService,
    private router: Router,
    private translitService: TranslitService
  ) {}

  ngOnInit() {
    this.videoInformation = null;
    this.timeControllerSubscriber = this.timeService.timeController.subscribe(() => {
      this.progress = this.calculateProgress();
    });
  }

  ngOnChanges(): void {
      this.dataService.getChannelById(+this.tvshow.channel_id).then(channel => {
        this.channel = channel;
      });
      this.loadVideoInformation();
  }

  public get isCurrent(): boolean {
    const currentTime: number = this.timeService.currentTime;
    return this.tvshow.start < currentTime && this.tvshow.stop >= currentTime;
  }

  public calculateProgress(): number {
    const currentTime: number = this.timeService.currentTime;
    return ((currentTime - this.tvshow.start) / (this.tvshow.stop - this.tvshow.start)) * 100;
  }

  public openTvshow(): void {
    if (!this.isCurrent) {
      const cpu: string = this.translitService.convertCyrillToLatin(this.videoInformation.name);
      this.router.navigate(['/details', `${cpu}-${this.tvshow.tvshow_id}`]);
    }
  }

  public get isArchive(): boolean {
    if (this.channel && this.channel.dvr_sec > 0) {
      const currentTime: number = this.timeService.currentTime;
      return ((this.tvshow.start >= (currentTime - this.channel.dvr_sec)) && this.tvshow.stop < currentTime);
    }
    return false;
  }

  private loadVideoInformation(): void {
    if (this.tvshow && this.tvshow.video_id) {
      this.dataService.getVideoInfo(+this.tvshow.video_id).then(res => {
        this.videoInformation = res;
        this.loadThumbnail();
        this.loadGenres();
      });
    }
  }

  private loadThumbnail(): void {
    if (this.tvshow.cover) {
      this.cover = this.tvshow.cover;
    } else {
      if (this.videoInformation && this.videoInformation.cover) {
        this.cover = this.videoInformation.cover;
      } else {
        const time: number = this.tvshow.start + ((this.tvshow.stop - this.tvshow.start) / 2);
        this.cover = this.imageService.getChannelFrame(this.tvshow.channel_id, time);
      }
    }
  }

  private loadGenres(): void { // Получение списка жанров для видео по id жанров
    this.dataService.loadVodCategories().then(categories => {
      const allGenresArray = categories.map(cat => cat.genres);
      const allGenres: Genre[] = [];
      allGenresArray.forEach(genre => {
        allGenres.push(...genre);
      });

      this.genres = allGenres.filter(genre => {
        return this.videoInformation.genres.some(genreId => genreId === genre.id);
      }).map(res => {
        const name = res.name;
        const part = name.split(')');
        if (part[1]) {
          return part[1];
        }
        return name;
      });
    });
  }

  ngOnDestroy() {
    if (this.timeControllerSubscriber) {
      this.timeControllerSubscriber.unsubscribe();
    }
  }

}
