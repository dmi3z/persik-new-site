import { Component, Input, Output, EventEmitter, SimpleChange, OnDestroy } from '@angular/core';
import { VideoControlEventInfo, ControlEvents } from '../../../models/player';
import { Options } from 'ng5-slider';

@Component({
  selector: 'app-video-controller',
  templateUrl: './video-controller.component.html',
  styleUrls: ['./video-controller.component.scss']
})

export class VideoControllerComponent implements OnDestroy {

  @Input() currentTime: number;
  @Input() duration: number;
  @Input() isPlaying: boolean;
  @Input() volume: number;
  @Output() controllerEvents: EventEmitter<VideoControlEventInfo> = new EventEmitter();

  public rangeOptions: Options = {
    showSelectionBar: true,
    translate: (): string => {
      return '';
    }
  };

  public volumeRangeOptions: Options = {
    showSelectionBar: true,
    floor: 0,
    ceil: 100,
    translate: (): string => {
      return '';
    }
  };

  constructor() {}


  ngOnChanges(changes: SimpleChange): void {
    for (const propName in changes) {
      if (propName !== 'isPlaying' && propName !== 'volume') {
        if (!this.rangeOptions.ceil) {
          const newOptions: Options = Object.assign({}, this.rangeOptions);
          newOptions.ceil = this.duration;
          newOptions.floor = 0;
          this.rangeOptions = newOptions;
        }
      }
    }
  }

  public valueChange(value) {
    this.currentTime = value;
  }

  public onRangeChange(): void {
    this.controllerEvents.emit(new VideoControlEventInfo(ControlEvents.CONTROL_PAUSE));
  }

  public onRangeChangeEnd(): void {
    this.controllerEvents.emit(new VideoControlEventInfo(ControlEvents.CONTROL_SEEK, this.currentTime));
    setTimeout(() => {
      this.controllerEvents.emit(new VideoControlEventInfo(ControlEvents.CONTROL_PLAY));
    }, 100);
  }

  public onRangeChangeStart(): void {
    this.controllerEvents.emit(new VideoControlEventInfo(ControlEvents.CONTROL_PAUSE));
  }

  public togglePlayerState(): void {
    if (this.isPlaying) {
      this.controllerEvents.emit(new VideoControlEventInfo(ControlEvents.CONTROL_PAUSE));
    } else {
      this.controllerEvents.emit(new VideoControlEventInfo(ControlEvents.CONTROL_PLAY));
    }
  }

  public volumeChange(value: number): void {
    this.controllerEvents.emit(new VideoControlEventInfo(ControlEvents.CONTROL_VOLUME, value));
  }

  public toggleFullscreen(): void {
    if (this.isFullscreenActive) {
      this.closeFullscreen();
    } else {
      this.openFullscreen();
    }
  }

  private get isFullscreenActive(): boolean {
    return ((document['fullScreenElement'] && document['fullScreenElement']
      !== null) || document['mozFullScreen'] || document['webkitIsFullScreen']);
  }

  private openFullscreen(): void {
    const elem =  document.body;
    const methodToBeInvoked = elem['requestFullscreen'] || elem['mozRequestFullScreen'] || elem['webkitRequestFullscreen'];
    if (methodToBeInvoked) {
      methodToBeInvoked.call(elem);
    }
  }

  private closeFullscreen(): void {
    const elem =  document;
    const methodToBeInvoked = elem['cancelFullScren'] || elem['mozCancelFullScreen'] || elem['webkitCancelFullScreen'];
    if (methodToBeInvoked) {
      methodToBeInvoked.call(elem);
    }
  }

  ngOnDestroy(): void {
    if (this.isFullscreenActive) {
      this.closeFullscreen();
    }
  }

}
