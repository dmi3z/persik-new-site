import { CheckWidthService, DataService, TimeService } from '../../../services';
import { Tvshow } from '../../../models';
import { Component, Input, OnChanges, SimpleChanges, ViewChild, OnInit, OnDestroy } from '@angular/core';
import { CdkVirtualScrollViewport } from '@angular/cdk/scrolling';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-tvshow-controller',
  templateUrl: './tvshow-controller.component.html',
  styleUrls: ['./tvshow-controller.component.scss']
})

export class TvshowControllerComponent implements OnChanges, OnInit, OnDestroy {

  @Input() channel_id: number;
  @ViewChild('viewport') viewport: CdkVirtualScrollViewport;
  public tvshows: Tvshow[] = [];
  private loadDelayTimer: any;
  private selectedTvshowIndex: number;
  private windowWidth: number;
  public itemWidth = 650;
  private widthServiceSubscription: Subscription;

  constructor(private dataService: DataService, private timeService: TimeService, private checkWindowService: CheckWidthService) {}

  ngOnInit(): void {
    this.windowWidth = this.checkWindowService.getWidth();
    this.checkDeviceWidth(this.windowWidth);
    this.widthServiceSubscription = this.checkWindowService.screenWidthEvent.subscribe(width => {
      this.windowWidth = width;
      this.checkDeviceWidth(width);
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.channel_id && changes['channel_id'].previousValue !== changes['channel_id'].currentValue) {
      clearTimeout(this.loadDelayTimer);
      setTimeout(() => {
        this.tvshows = [];
        this.loadTvshows();
      }, 1000);
    }
  }

  private checkDeviceWidth(width): void {
    switch (true) {
      case width <= 767:
        this.itemWidth = 200;
        break;

      default:
        this.itemWidth = 650;
        break;
    }
  }

  private loadTvshows(): void {
    this.dataService.getTvshows(this.channel_id).then(res => {
      this.tvshows = res;
      setTimeout(() => {
        this.scrollToCurrent();
      }, 1000);
    });
  }

  private scrollToCurrent(): void {
    if (this.tvshows.length > 0) {
      const currentTime: number = this.timeService.currentTime;
      const currentTvshow: Tvshow = this.tvshows.find(tvshow => {
        return tvshow.start < currentTime && tvshow.stop >= currentTime;
      });
      const currentTvshowIndex: number = this.tvshows.indexOf(currentTvshow);
      if (currentTvshowIndex && this.viewport) {
        const viewportElement: HTMLElement = this.viewport.elementRef.nativeElement;
        viewportElement.classList.remove('smoth-scroll');
        this.selectedTvshowIndex = currentTvshowIndex;
        setTimeout(() => {
          this.scrollToIndex();
          viewportElement.classList.add('smoth-scroll');
        }, 0);
      }
    }
  }

  public slidePrev(): void {
    this.selectedTvshowIndex--;
    this.scrollToIndex();
  }

  public slideNext(): void {
    this.selectedTvshowIndex++;
    this.scrollToIndex();
  }

  private scrollToIndex(): void {
    const t = document.getElementsByTagName('cdk-virtual-scroll-viewport')[0];
    if (t) {
      t.scrollTo(this.itemWidth * (this.selectedTvshowIndex) + 0.2 * this.itemWidth, 0);
    }
  }

  ngOnDestroy() {
    if (this.widthServiceSubscription) {
      this.widthServiceSubscription.unsubscribe();
    }
  }
}
