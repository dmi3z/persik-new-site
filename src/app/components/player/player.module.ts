import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { Ng5SliderModule } from 'ng5-slider';

import {
  PlayerComponent,
  VideoControllerComponent,
  ChannelControllerComponent,
  TvshowControllerComponent,
  TvshowControllerItemComponent,
  SoundControllerComponent
} from './';
import { PipesModule } from '../../pipes/pipes.module';


@NgModule({
  declarations: [
    PlayerComponent,
    VideoControllerComponent,
    ChannelControllerComponent,
    TvshowControllerComponent,
    TvshowControllerItemComponent,
    SoundControllerComponent
  ],
  imports: [
    FormsModule,
    CommonModule,
    PipesModule,
    ScrollingModule,
    Ng5SliderModule
  ],
  exports: [
    PlayerComponent,
    VideoControllerComponent,
    ChannelControllerComponent,
    TvshowControllerComponent,
    TvshowControllerItemComponent,
    SoundControllerComponent
  ]
})

export class PlayerModule {}
