import { DataService } from './../../../services';
import { Component, Input, Output, EventEmitter, OnInit, AfterViewInit } from '@angular/core';
import { Channel } from '../../../models';

@Component({
  selector: 'app-channel-controller',
  templateUrl: './channel-controller.component.html',
  styleUrls: ['./channel-controller.component.scss']
})

export class ChannelControllerComponent implements OnInit, AfterViewInit {

  @Input() selectedChannelId: number;
  public channels: Channel[] = [];
  public activeChannelId: number;
  private currentChannel: Channel;
  @Output() changeActiveChannel: EventEmitter<Channel> = new EventEmitter(); // Канал который воспроизводится
  @Output() changeSelectedChannel: EventEmitter<number> = new EventEmitter(); // Канал, ленту которого сейчас просматриваем

  constructor(private dataService: DataService) {}

  ngOnInit(): void {
    this.activeChannelId = this.selectedChannelId;
    this.loadChannels();
  }

  public isActiveChannel(channel_id: number): boolean {
    return this.activeChannelId === channel_id;
  }

  public selectChannel(channel_id: number): void {
    this.selectedChannelId = channel_id;
    this.activeChannelId = channel_id;
    this.changeActiveChannel.emit(this.channels.find(channel => channel.channel_id === channel_id));
    setTimeout(() => {
      this.centerChannel();
    }, 100);
  }

  /* private getLogo(channel_id): string {
    return this.imageService.getChannelLogo(channel_id);
  }

  public scrollChannels(event: WheelEvent): void {
    if (this.scrollPause) {
      return;
    }
    this.scrollPause = true;
    if (event.deltaY > 0) {
      this.nextChannel();
    } else {
      this.prevChannel();
    }
    setTimeout(() => {
      this.scrollPause = false;
    }, 300);
  }

  public nextChannel(): void {
    if (this.selectedChannelIndex < this.channels.length) {
      this.selectedChannelIndex++;
      this.selectedChannelId = this.channels[this.selectedChannelIndex].channel_id;
      this.changeSelectedChannel.emit(this.selectedChannelId);
    }
  }

  public prevChannel(): void {
    if (this.selectedChannelIndex > 0) {
      this.selectedChannelIndex--;
      this.selectedChannelId = this.channels[this.selectedChannelIndex].channel_id;
      this.changeSelectedChannel.emit(this.selectedChannelId);
    }
  } */

  private loadChannels(): void {
    this.dataService.getChannels().then(channels => {
      if (channels.length > 0) {
        this.currentChannel = channels.find(ch => ch.channel_id === this.activeChannelId);
        if (this.currentChannel.genres.includes(1299)) {
          this.channels = channels.filter(ch => ch.genres.includes(1299));
        } else {
          this.channels = channels.filter(ch => !ch.genres.includes(1299));
        }
        this.selectChannel(this.selectedChannelId);
      }
    });
  }

  private centerChannel(): void {
    const elem = document.getElementsByClassName('tape__channel_active')[0];
      if (elem) {
        elem.scrollIntoView({block: 'center', inline: 'center', behavior: 'smooth'});
      }
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.centerChannel();
    }, 1000);
  }

}
