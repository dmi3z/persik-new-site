import { AuthService } from '../../services/auth.service';
import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-adult-control',
  templateUrl: 'adult-control.component.html',
  styleUrls: ['adult-control.component.scss']
})

export class AdultControlComponent {

  public pin: string;
  public isWrongPin: boolean;
  @Output() modalEvent: EventEmitter<boolean> = new EventEmitter();

  constructor(private authService: AuthService) {}

  public closeModal(): void {
    this.modalEvent.emit();
  }

  public checkPin(): void {
    this.authService.getAccountInfo().then(info => {
      if (this.pin === info.pass_code) {
        this.modalEvent.emit(true);
      } else {
        this.isWrongPin = true;
      }
    });
  }
}
