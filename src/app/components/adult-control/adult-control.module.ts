import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AdultControlComponent } from './adult-control.component';


@NgModule({
  declarations: [
    AdultControlComponent
  ],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [
    AdultControlComponent
  ]
})

export class AdultControlModule {}
