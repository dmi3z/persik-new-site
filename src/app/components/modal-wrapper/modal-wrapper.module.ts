import { ModalBeforeViewComponent } from './modal-before-view/modal-before-view.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModalWrapperComponent } from './modal-wrapper.component';
import { ModalForgotComponent } from './modal-forgot/modal-forgot.component';
import { ModalInfoComponent } from './modal-info/modal-info.component';
import { ModalLoginComponent } from './modal-login/modal-login.component';
import { VendorSendComponent } from './retail-send/retail-send.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    ModalWrapperComponent,
    ModalForgotComponent,
    ModalInfoComponent,
    ModalLoginComponent,
    VendorSendComponent,
    ModalBeforeViewComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule
  ],
  exports: [
    ModalWrapperComponent
  ]
})

export class ModalWrapperModule {}
