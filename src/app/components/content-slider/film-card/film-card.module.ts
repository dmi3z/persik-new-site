 import { PipesModule } from './../../../pipes/pipes.module';
 import { CommonModule } from '@angular/common';
 import { FilmCardComponent } from './film-card.component';
 import { NgModule } from '@angular/core';
 import { DeferLoadDirective } from '../../../directives/defer-load.directive';

 @NgModule({
   declarations: [
     FilmCardComponent,
     DeferLoadDirective
   ],
   imports: [
     CommonModule,
     PipesModule
   ],
   exports: [
     FilmCardComponent
   ]
 })

 export class FilmCardModule {}
