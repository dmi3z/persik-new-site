import { Component, Input, OnInit, Inject, PLATFORM_ID, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { Router } from '@angular/router';
import { Video, Genre } from './../../../models';
import { DataService, TranslitService, ImageService } from '../../../services';

@Component({
  selector: 'app-film-card',
  templateUrl: './film-card.component.html',
  styleUrls: ['./film-card.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})

export class FilmCardComponent implements OnInit {

  @Input() content_id: number | string;
  @Input() type: string;
  @Input() category: string;
  @Input() category_id: number;
  @Input() genre: string;
  @Input() video?: Video;
  @Input() isCanRedirect = true;
  public videoInformation: Video = new Video();
  public videoGenres: string[] = [];
  public isBrowser: boolean;
  // public channelLogo: string;
  public showElement: boolean;

  public isCourse: boolean;

  constructor(
    private dataService: DataService,
    private imageService: ImageService,
    private router: Router,
    private transliteService: TranslitService,
    @Inject(PLATFORM_ID) private platform: any,
    private cdr: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    if (isPlatformBrowser(this.platform)) {
      this.isBrowser = true;
    }
    if (!this.isStub && !this.video) {
      if (this.content_id) {
        if (!this.isTvshow) {
          this.dataService.getVideoInfo(+this.content_id).then(res => {
            this.videoInformation = res;
            this.isCourse = res.category_id === 6;
            this.loadGenres();
          });
        } else {
          this.dataService.getTvshowInfo(this.content_id.toString()).then(res => {
            // this.channelLogo = this.imageService.getChannelLogo(res.channel_id);
            this.dataService.getVideoInfo(+res.video_id).then(info => {
              this.isCourse = info.category_id === 6;
              this.videoInformation = info;
              if (!this.videoInformation.cover) {
                this.videoInformation.cover = this.imageService.getChannelFrame(res.channel_id, res.start + (res.stop - res.start) / 2);
              }
              this.loadGenres();
            });
          });
        }
      }
      return;
    }

    if (this.video) {
      this.videoInformation = this.video;
      if (this.videoGenres.length === 0) {
        this.loadGenres();
      }
    }
  }

  public get isPladform(): boolean {
    return this.videoInformation ? this.videoInformation.is_pladform : false;
  }

  public showAll(): void {
    if (this.category === 'peredachi') {
      this.router.navigate(['peredachi']);
      return;
    }
    if (this.category === 'blogs') {
      this.router.navigate(['blogers']);
      return;
    }
    this.router.navigate([this.category], { queryParams: { genre: this.genre } });
  }

  public get isStub(): boolean {
    return this.content_id === -1;
  }

  private get isTvshow(): boolean {
    return this.content_id.toString().length > 10;
  }

  public showDescription(): void {
    if (this.isCanRedirect) {
      if (this.isCourse) {
        const ids = [
          1047632, // Photoshop
          966238,  // Android
          1606986, // Phyton,
          1041757  // English
        ];
        if (ids.includes(+this.content_id)) {
          this.router.navigate(['/course-page', this.content_id]);
        } else {
          this.router.navigate(['/details', this.contentName]);
        }
      } else {
        this.router.navigate(['/details', this.contentName]);
      }
    }
  }

  public get isHaveRatings(): boolean {
    if (this.isHaveKinopoisk || this.isHaveImdb) {
        return true;
      }
    return false;
  }

  public get isHaveImdb(): boolean {
    return (this.videoInformation.ratings && this.videoInformation.ratings.imdb && this.videoInformation.ratings.imdb.value > 0);
  }

  public get isHaveKinopoisk(): boolean {
    return (this.videoInformation.ratings && this.videoInformation.ratings.kinopoisk && this.videoInformation.ratings.kinopoisk.value > 0);
  }

  private get contentName(): string {
    if (this.videoInformation) {
      if (this.videoInformation.international_name && this.videoInformation.international_name.length > 0) {
        return this.transliteService.convertCyrillToLatin(this.videoInformation.international_name, this.content_id.toString());
      } else {
        const name: string = this.transliteService.convertCyrillToLatin(this.videoInformation.name, this.content_id.toString());
        return name;
      }
    }
  }

  private loadGenres(): void { // Получение списка жанров для видео по id жанров
    this.dataService.loadVodCategories().then(categories => {
      const allGenresArray = categories.map(cat => cat.genres);
      const allGenres: Genre[] = [];
      allGenresArray.forEach(genre => {
        allGenres.push(...genre);
      });

      this.videoGenres = allGenres.filter(genre => {
        return this.videoInformation.genres.some(genreId => genreId === genre.id);
      }).map(res => {
        const name = res.name;
        const part = name.split(')');
        if (part[1]) {
          return part[1];
        }
          return name;
      });
      this.cdr.markForCheck();
    });
  }

}
