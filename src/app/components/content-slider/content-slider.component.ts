import { Router } from '@angular/router';
import { Component, Input, PLATFORM_ID, Inject, OnInit } from '@angular/core';
import { ContentType, ContentName } from '../../models/vod';
import { isPlatformBrowser } from '@angular/common';

@Component({
  selector: 'app-content-slider',
  templateUrl: './content-slider.component.html',
  styleUrls: ['./content-slider.component.scss'],
})

export class ContentSliderComponent implements OnInit {

  @Input() type: ContentType;
  @Input() content: Array<any>;
  @Input() title: string;
  @Input() isRedirectNeeded: boolean;
  @Input() category_id: number;
  @Input() slidesToShow = 4;
  @Input() category: string;
  @Input() genre: string;

  public isCanShowAll: boolean;
  public isBrowser: boolean;

  public slideConfig: any;

  constructor(private router: Router, @Inject(PLATFORM_ID) private platform: any) {}

  ngOnInit(): void {
    if (isPlatformBrowser(this.platform)) {
      this.isBrowser = true;
      this.setSlidesConfig();
      setTimeout(() => {
        this.isCanShowAll = true;
      }, 2000);
    }
  }

  private setSlidesConfig(): void {
    this.slideConfig = {
      'slidesToShow': this.slidesToShow,
      'slidesToScroll': 1,
      'arrows': true,
      'dots': false,
      'fade': false,
      'centerMode': false,
      'autoplay': false,
      'infinite': false,
      'responsive': [
        {
          'breakpoint': 1600,
          'settings': {
            'dots': false,
            'slidesToShow': this.slidesToShow
          }
        },
        {
          'breakpoint': 1400,
          'settings': {
            'dots': false,
            'slidesToShow': this.slidesToShow
          }
        },
        {
          'breakpoint': 1200,
          'settings': {
            'dots': false,
            'slidesToShow': 3
          }
        },
        {
          'breakpoint': 992,
          'settings': {
            'dots': false,
            'slidesToShow': 2
          }
        },
        {
          'breakpoint': 767,
          'settings': {
            'dots': false,
            'slidesToShow': 1,
            'arrows': false,
            'infinity': false
          }
        },
      ]
    };
  }

  /* public get category(): string {
    if (this.title) {
      const title: string = this.title.toLowerCase();
      switch (title) {
        case 'фильмы':
          return 'films';
        case 'сериалы':
          return 'serialy';
        case 'передачи':
          return 'peredachi';
        case 'мультфильмы':
          return 'multfilmy';
        case 'курсы':
          return 'kursy-online';
        default:
          break;
      }
    }
    return null;
  } */

  public get isChannels(): boolean {
    return this.type === ContentName.CHANNEL;
  }

  public get isVideos(): boolean {
    return this.type === ContentName.VIDEO;
  }

  public get isTvshows(): boolean {
    return this.type === ContentName.TV;
  }

  public get isBooks(): boolean {
    return this.type === ContentName.BOOK;
  }

  public getId(item: any) {
    if (item) {
      if (item.video_id) {
        return item.video_id;
      }
      if (item.tvshow_id) {
        return item.tvshow_id;
      }
    }
  }

  public goTo() {
    switch (this.title.toLowerCase()) {
      case 'каналы':
        this.router.navigate(['/tv-review/']);
        break;
      case 'фильмы':
        this.router.navigate(['/films']);
        break;
      case 'сериалы':
        this.router.navigate(['/serialy']);
        break;
      case 'передачи':
        this.router.navigate(['/peredachi/']);
        break;
      case 'мультфильмы':
        this.router.navigate(['/multfilmy']);
        break;
      case 'курсы':
        this.router.navigate(['/kursy-online/']);
        break;
      case 'комедии':
      case 'биография':
      case 'ужасы':
        this.router.navigate(['/films'], { queryParams: { genre: this.genre }});
        break;
      default:
        this.router.navigate(['/tv-review/']);
        break;
    }
  }
}
