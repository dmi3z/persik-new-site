import { ModalController } from './../../../services/modal.controller';
import { TimeService } from './../../../services/time.service';
import { ImageService } from './../../../services/image.service';
import { DataService } from './../../../services/data.service';
import { Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Channel, Tvshow, ModalName } from '../../../models';
import { TranslitService } from '../../../services';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-channel-card',
  templateUrl: './channel-card.component.html',
  styleUrls: ['./channel-card.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})

export class ChannelCardComponent implements OnInit, OnDestroy {

  @Input() channel: Channel;
  @Input() isNewDesign: boolean;
  public tvshow: Tvshow = new Tvshow();
  public cover: string;
  public isBrowser: boolean;
  public progress: number;
  public isShow: boolean;

  private timeController: Subscription;

  constructor(
    private dataService: DataService,
    private imageService: ImageService,
    private timeService: TimeService,
    private router: Router,
    private transliteService: TranslitService,
    @Inject(PLATFORM_ID) private platform: any,
    private modalController: ModalController
  ) { }

  ngOnInit(): void {
    if (isPlatformBrowser(this.platform)) {
      this.isBrowser = true;

      if (!this.isStub) {
        this.loadChannel();
        this.cover = this.getFrame();
        this.loadCurrentTvshow();
      }
    }
    this.setVisibleItem();
  }

  public get isStub(): boolean {
    if (this.channel) {
      return this.channel.channel_id === -1;
    }
    return false;
  }

  private setVisibleItem() {
    this.isShow = true;
    this.timeController = this.timeService.timeController.subscribe(() => {
      this.checkTvshow();
      this.progress = this.getProgress();
      this.cover = this.getFrame();
    });
  }

  public showAllChannels(): void {
    this.router.navigate(['tv-review']);
  }

  private getFrame(): string {
    if (this.channel && this.channel.channel_id) {
      const time = (Math.round(this.timeService.currentTime / 60)) * 60; // Так нужно nginx
      return this.imageService.getChannelFrame(this.channel.channel_id, time);
    }
  }

  private getProgress(): number {
    if (!this.isNewDesign && this.tvshow) {
      const start: number = this.tvshow.start;
      const stop: number = this.tvshow.stop;
      const progress = ((this.timeService.currentTime - start) / (stop - start)) * 100;
      if (progress > 100 || progress < 0) {
        if (progress > 100) {
          return 100;
        }
        if (progress < 0) {
          return 0;
        }
      } else {
        return progress;
      }
    }
  }

  public openChannel(): void {
    // console.log(this.channel)
    // if (this.channel.available) {
    const cpu: string = this.transliteService.convertCyrillToLatin(this.channel.name, this.channel.channel_id.toString());
    this.router.navigate(['channel-player', cpu]);
    /* } else {
      console.log('Iam here');
      this.modalController.present(ModalName.FAST_AUTH);
    } */
  }

  private checkTvshow(): void {
    if (this.tvshow && this.tvshow.stop && this.timeService.currentTime >= this.tvshow.stop) {
      this.loadCurrentTvshow();
    }
  }

  public openChannelPage(): void {
    this.router.navigate(['/channels', this.contentName]);
  }

  private get contentName(): string {
    if (this.channel) {
      const name: string = this.transliteService.convertCyrillToLatin(this.channel.name, this.channel.channel_id.toString());
      return name;
    }
  }

  private loadChannel(): void {
    this.dataService.getChannelById(this.channel.channel_id).then(res => {
      this.channel = res;
    });
  }

  private loadCurrentTvshow(): void {
    this.dataService.getCurrentTvShow(this.channel.channel_id).then(response => {
      this.tvshow = response;
      this.progress = this.getProgress();
    });
  }

  ngOnDestroy(): void {
    if (this.timeController) {
      this.timeController.unsubscribe();
    }
  }

}
