import { CommonModule } from '@angular/common';
import { ChannelCardComponent } from './channel-card.component';
import { NgModule } from '@angular/core';

@NgModule({
  declarations: [
    ChannelCardComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    ChannelCardComponent
  ]
})

export class ChannelCardModule {}
