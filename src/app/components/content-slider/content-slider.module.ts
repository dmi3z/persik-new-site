import { FilmCardModule } from './film-card/film-card.module';
import { ChannelCardModule } from './channel-card/channel-card.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PipesModule } from '../../pipes/pipes.module';
import { SlickModule } from 'ngx-slick';
import { DeviceDetectorModule, DeviceDetectorService } from 'ngx-device-detector';

import {
  ContentSliderComponent
} from './';
import { AudiobookCardModule } from '../../pages/online/audiobooks/audiobook-card/audiobook-card.module';


@NgModule({
  declarations: [
    ContentSliderComponent
  ],
  imports: [
    CommonModule,
    PipesModule,
    ChannelCardModule,
    FilmCardModule,
    SlickModule.forRoot(),
    DeviceDetectorModule,
    AudiobookCardModule
  ],
  providers: [
    DeviceDetectorService
  ],
  exports: [
    ContentSliderComponent
  ]
})
export class ContentSliderModule { }
