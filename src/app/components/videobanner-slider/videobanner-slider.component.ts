import { Banner } from './../../models/banner';
import { DataService } from '../../services';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-videobanner-slider',
  templateUrl: './videobanner-slider.component.html',
  styleUrls: ['./videobanner-slider.component.scss']
})

export class VideobannerSliderComponent implements OnInit {

  public banners: Banner[] = [];
  public banner: Banner;

  constructor(private dataService: DataService) {}

  ngOnInit() {
    this.loadBanners();
  }

  public setActiveBanner(banner: Banner): void {
    this.banner = banner;
  }

  public isBannerActive(id: number): boolean {
    return this.banner.banner_id === id;
  }

  public onVideoEnded(): void {
    const index = this.banners.indexOf(this.banner);
    if (index < this.banners.length - 1) {
      this.banner = this.banners[index + 1];
    } else {
      this.banner = this.banners[0];
    }
  }

  private loadBanners(): void {
    this.dataService.getBanners().then(res => {
      this.banners = res;
      this.banner = this.banners && this.banners.length > 0 ? this.banners[0] : null;
    });
  }
}
