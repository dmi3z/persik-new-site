import { Router } from '@angular/router';
import { Banner, ContentName } from './../../../models';
import { Component, Input, ElementRef, ViewChild, EventEmitter, Output, AfterViewInit, OnChanges, OnDestroy } from '@angular/core';
import { DataService, TranslitService, AuthService, AnalyticService } from '../../../services';


@Component({
  selector: 'app-video-banner-item',
  templateUrl: './video-banner-item.component.html',
  styleUrls: ['./video-banner-item.component.scss']
})

export class VideoBannerItemComponent implements AfterViewInit, OnChanges, OnDestroy {

  @Input() banner: Banner;
  @ViewChild('videotag') private videotag: ElementRef;
  private video: HTMLVideoElement;
  public bannerInfo: BannerVideoInfo;
  public isDesktop: boolean;

  private videoEndedHandler: any;

  @Output() videoPlayEnded: EventEmitter<null> = new EventEmitter();

  constructor(
    private router: Router,
    private translitService: TranslitService,
    private dataService: DataService,
    private authService: AuthService,
    // private modalController: ModalController,
    private analyticService: AnalyticService
  ) {}

  ngAfterViewInit() {
    this.video = this.videotag.nativeElement;
    this.videoEndedHandler = this.videoEnded.bind(this);
    this.video.addEventListener('canplay', this.videoCanPlay);
    this.video.addEventListener('ended', this.videoEndedHandler);
  }

  ngOnChanges() {
    if (this.banner) {
      this.loadBannerInfo();
    }
  }

  private videoEnded() {
    const player = <HTMLVideoElement>document.getElementById('test');
    player.pause();
    this.videoPlayEnded.emit();
  }

  private videoCanPlay() {
    const player = <HTMLVideoElement>document.getElementById('test');
    player.volume = 0;
    player.muted = true;
    player.play();
  }

  public get isLogin(): boolean {
    return this.authService.isLogin;
  }

  public openBannerLink(): void {
    const cpu: string = this.translitService.convertCyrillToLatin(this.bannerInfo.name);
    if (this.banner.element_type === ContentName.VIDEO) {
      this.router.navigate(['/details', `${cpu}-${this.banner.element_id}`]);
    }
    if (this.banner.element_type === ContentName.CHANNEL) {
      this.router.navigate(['channel-player', `${cpu}-${this.banner.element_id}`]);
    }
    if (this.banner.element_type === 'site') {
      this.router.navigate(['/details', `${cpu}-${this.banner.element_id}`]);
    }
  }

  public goToRegistration(): void {
    // this.modalController.present(ModalName.REGISTER);
    this.analyticService.targetSend('poprobovat', 47723575);
    this.router.navigate(['private/tv-tarify'], { fragment: 'mounth' });
  }

  private loadBannerInfo(): void {
    if (this.banner.element_type === ContentName.VIDEO && this.banner.element_id > 0) {
      this.dataService.getVideoInfo(+this.banner.element_id).then(videoInfo => {
        if (videoInfo) {
          /* if (videoInfo.cast && videoInfo.cast.length > 0) {
            this.dataService.loadActors(videoInfo.cast).then(res => {
              const actorNames = res.map(actor => actor.name);
              this.bannerInfo = null;
              this.bannerInfo = {
                id: this.banner.element_id,
                name: this.getNameWithColor(videoInfo.name),
                actors: actorNames,
                type: ContentName.VIDEO
              };
            });
          } else { */
            this.bannerInfo = null;
            this.bannerInfo = {
              id: this.banner.element_id,
              name: this.getNameWithColor(videoInfo.name),
              type: ContentName.VIDEO
            };
          // }
        } else {
          this.bannerInfo = null;
        }
      });
    } else if (this.banner.element_type === ContentName.CHANNEL && this.banner.element_id > 0) {
      this.dataService.getChannelById(+this.banner.element_id).then(channel => {
        this.bannerInfo = null;
        this.bannerInfo = {
          id: this.banner.element_id,
          name: this.getNameWithColor(channel.name),
          type: ContentName.CHANNEL
        };
      });
    } else if (this.banner.element_type === 'site') {
      this.bannerInfo = null;
      this.bannerInfo = {
        id: this.banner.element_id,
        name: this.getNameWithColor(this.banner.name),
        type: ContentName.CHANNEL
      };
    }
  }

  private getNameWithColor(name: string): string {
    if (name) {
      const nameArray: string[] = name.split(' ');
      if (nameArray.length === 1) {
        return name;
      }
      let colorString: string = nameArray.shift();
      colorString += ' <span class="orange-text">';
      const lastText: string = nameArray.toString();
      colorString += lastText.replace(new RegExp(',', 'g'), ' ');
      colorString += '</span>';
      return colorString;
    }
  }

  ngOnDestroy() {
    this.video.removeEventListener('canplay', this.videoCanPlay);
    this.video.removeEventListener('ended', this.videoEndedHandler);
  }
}


interface BannerVideoInfo {
  id: number;
  name: string;
  actors?: string[];
  type: string;
}
