import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { VideoBannerItemComponent } from './video-banner-item/video-banner-item.component';
import { VideobannerSliderComponent } from './videobanner-slider.component';

@NgModule({
  declarations: [
    VideoBannerItemComponent,
    VideobannerSliderComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    VideobannerSliderComponent
  ]
})

export class VideobannerComponentModule {}

