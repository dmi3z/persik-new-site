import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-main-channel-card',
    templateUrl: './main-channel-card.component.html',
    styleUrls: ['./main-channel-card.component.scss']
})

export class MainChannelCardComponent {
    @Input() showBtn: boolean;
}
