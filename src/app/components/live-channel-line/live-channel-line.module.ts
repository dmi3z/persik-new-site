import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LiveChannelCardComponent } from './live-channel-card/live-channel-card.component';
import { LiveChannelLineComponent } from './live-channel-line.component';
import { VirtualScrollerModule } from 'ngx-virtual-scroller';

import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    LiveChannelCardComponent,
    LiveChannelLineComponent
  ],
  imports: [
    CommonModule,
    VirtualScrollerModule,
    PipesModule,
  ],
  exports: [
    LiveChannelLineComponent
  ]
})

export class LiveChannelLineModule {}
