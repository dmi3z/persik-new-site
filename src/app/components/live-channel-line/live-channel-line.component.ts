import { DataService } from './../../services/data.service';
import { Component, Input, OnChanges } from '@angular/core';
import { Channel, Tvshow } from '../../models';

@Component({
  selector: 'app-live-channel-line',
  templateUrl: './live-channel-line.component.html',
  styleUrls: ['./live-channel-line.component.scss']
})

export class LiveChannelLineComponent implements OnChanges {

  @Input() channel: Channel;

  public tvshows: Tvshow[] = [];

  constructor(private dataService: DataService) {}

  ngOnChanges() {
    this.loadTvshows();
  }

  public loadTvshows() {
    return this.dataService.getTvshows(this.channel.channel_id).then(res => {
      this.tvshows = res;
    });
  }

}
