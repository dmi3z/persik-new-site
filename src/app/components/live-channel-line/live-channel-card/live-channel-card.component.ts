import { Component, Input, AfterViewInit, ViewChild, ElementRef, OnInit, OnChanges } from '@angular/core';
import { Tvshow, Video, Genre } from './../../../models';
import { TimeService, DataService } from '../../../services';

@Component({
  selector: 'app-live-channel-card',
  templateUrl: './live-channel-card.component.html',
  styleUrls: ['./live-channel-card.component.scss']
})

export class LiveChannelCardComponent implements AfterViewInit, OnInit, OnChanges {

  @Input() tvshow: Tvshow;
  @ViewChild('item') domElement: ElementRef;

  public content: Video;
  public genres: string[] = [];

  constructor(private timeService: TimeService, private dataService: DataService) {}

  ngOnInit(): void {}

  ngOnChanges(): void {
    this.dataService.getVideoInfo(+this.tvshow.video_id).then(res => {
      this.content = res;
      this.tvshow.cover = this.cover;
    });
  }

  ngAfterViewInit() {
    this.scrollIfCurrent();
  }

  public get isCurrent(): boolean {
    const currentTime = this.timeService.currentTime;
    return (this.tvshow.start <= currentTime) && (this.tvshow.stop > currentTime);
  }

  private get cover(): string {
    if (this.content && this.content.cover && this.content.cover.length > 0) {
      return this.content.cover;
    }
    if (this.tvshow.cover && this.tvshow.cover.length > 0) {
      return this.content.cover;
    }
    return null;
  }

  private scrollIfCurrent(): void {
    if (this.isCurrent) {
      const element: HTMLElement = this.domElement.nativeElement;
      element.scrollIntoView({
        block: 'start',
        inline: 'start'
      });
    }
  }

  // private loadGenres(): void { // Получение списка жанров для видео по id жанров
  //   this.dataService.loadVodCategories().then(categories => {
  //     const allGenresArray = categories.map(cat => cat.genres);
  //     const allGenres: Genre[] = [];
  //     allGenresArray.forEach(genre => {
  //       allGenres.push(...genre);
  //     });

  //     this.genres = allGenres.filter(genre => {
  //       return this.videoInformation.genres.some(genreId => genreId === genre.id);
  //     }).map(res => {
  //       const name = res.name;
  //       const part = name.split(')');
  //       if (part[1]) {
  //         return part[1];
  //       }
  //       return name;
  //     });
  //   });
  // }

}
