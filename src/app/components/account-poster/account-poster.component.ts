import { Component } from '@angular/core';
import { Router } from '@angular/router';
// import { PackageItem } from '../../models';
// import { PaymentService } from '../../services';

@Component({
  selector: 'app-account-poster',
  templateUrl: './account-poster.component.html',
  styleUrls: ['./account-poster.component.scss']
})

export class AccountPosterComponent {

  /* private selectedTariff: PackageItem = {
    id: 176,
    name: 'Все включено ТВ',
    cost: 34.9,
  }; */

  // private period = '50% скидка! Все ТВ на 1 год';

  constructor(private router: Router) { }

  public openTariffs(): void {
    // this.paymentService.setTariff(this.selectedTariff, this.period);
    this.router.navigate(['private/tv-tarify'], { fragment: 'god' });
    // this.router.navigate(['/payment']);
  }

}
