import { CommonModule } from '@angular/common';
import { SeriesComponent } from './series.component';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { GenreFilterModule } from '../genre-filter/genre-filter.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { FilmCardModule } from '../../../components/content-slider/film-card/film-card.module';

@NgModule({
  declarations: [
    SeriesComponent
  ],
  imports: [
    CommonModule,
    GenreFilterModule,
    NgSelectModule,
    FormsModule,
    FilmCardModule,
    InfiniteScrollModule,
    RouterModule.forChild([
      {
        path: '',
        component: SeriesComponent
      }
    ])
  ]
})

export class SeriesPageModule { }

