import { MetaDefinition } from '@angular/platform-browser';
import { MetaData } from '../../../models/meta';

export function setGenreName(genre_name?: string): MetaData {
  const tags: MetaDefinition[] = [
    {
      name: 'keywords',
      content: `сериалы онлайн${genre_name ? ' в жанре ' + genre_name : ''}`
    },
    {
      name: 'description',
      content: `Смотрите новые сериалы ${genre_name ? 'в жанре ' + genre_name : ''} вместе с Persik. ✅Выгодные тарифы.
      ✅Новинки сериалов всего за 30 копеек! ✅Архив 24 дня, Full HD качество, программа передач.
      info@persik.by`
    }
  ];

  const title = `Смотреть сериалы онлайн${genre_name ? ' в жанре ' + genre_name : ''}`;

  return {
    title,
    tags
  };
}


