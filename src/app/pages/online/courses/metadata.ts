import { MetaDefinition } from '@angular/platform-browser';
import { MetaData } from '../../../models/meta';

export function setGenreName(genre_name?: string): MetaData {
  const tags: MetaDefinition[] = [
    {
      name: 'keywords',
      content: `курсы онлайн${genre_name ? ' в жанре ' + genre_name : ''}`
    },
    {
      name: 'description',
      content: `Курсы, семинары, тренинги онлайн${genre_name ? ' в жанре ' + genre_name : ''} по выгодным ценам. Онлайн-обучение.`
    }
  ];

const title = `Смотреть курсы онлайн${genre_name ? ' в жанре ' + genre_name : ''}`;

  return {
    title,
    tags
  };
}



