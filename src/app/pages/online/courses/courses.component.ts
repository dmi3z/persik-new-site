import { SharedService } from './../../../services/shared.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, Inject, PLATFORM_ID, OnDestroy } from '@angular/core';
import { Location, isPlatformBrowser } from '@angular/common';
import { Country, RatingFilter, Genre } from '../../../models';
import { LoadingService, DataService, MetaService, TranslitService } from '../../../services';
import { ratingFilters, dateFilters } from '../filtersdata';
import { setGenreName } from './metadata';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.scss']
})

export class CoursesComponent implements OnInit, OnDestroy {
  public genres: Genre[] = [];
  public activeGenre: Genre;
  public film_ids: any[] = [];
  public isBrowser = false;
  public countries: Country[] = [];
  private selectedCountry: Country;
  private selectedRatingFilter: RatingFilter;

  public ratingFilters: RatingFilter[] = ratingFilters;
  public dateFilter: RatingFilter[] = dateFilters;

  private category_id: number;
  private totalCount: number;
  private skip = 0;
  private readonly loadPart: number = 16; // Количество разово загружаемых видео на отображение в ленте

  constructor(
    private dataService: DataService,
    private activatedRoute: ActivatedRoute,
    private translitService: TranslitService,
    private loadingService: LoadingService,
    private location: Location,
    @Inject(PLATFORM_ID) private platform: any,
    private metaService: MetaService,
    private router: Router,
    private sharedService: SharedService
  ) {}

  ngOnInit(): void {
    this.sharedService.enableEdu();
    this.metaService.updateMeta(setGenreName());
    this.metaService.createCanonicalURL();
    if (isPlatformBrowser(this.platform)) {
      this.isBrowser = true;
    }
    this.loadingService.startLoading();
    this.dataService.loadVodCategories().then(categories => {
      if (categories[4]) {
        this.category_id = categories[4].id;
        this.genres = categories[4].genres.filter(genre => genre.is_main);
        this.activeGenre = this.genres[0];
        this.checkRouteFilter();
        this.loadContent();
      }
    });
  }

  public get isHaveContent(): boolean {
    return this.film_ids.length > 0;
  }

  public get isHaveGenres(): boolean {
    return this.genres.length > 0;
  }

  public onGenreChange(genre: Genre): void {
    if (genre) {
      this.activeGenre = genre;
      if (genre.name === 'Все жанры') {
        this.activeGenre.id = 1;
        this.metaService.updateMeta(setGenreName());
        this.location.go(`/kursy-online`);
        this.metaService.updateMeta(setGenreName());
      } else {
        this.metaService.updateMeta(setGenreName(genre.name));
        const routeName = genre.name_en ? genre.name_en : '';
        const cpu: string = this.translitService.convertCyrillToLatin(routeName);
        this.location.go(`/kursy-online?genre=${cpu}`);
      }
    } else {
      this.activeGenre = this.genres[0];
      this.location.go(`/kursy-online`);
      this.metaService.updateMeta(setGenreName());
    }
    this.skip = 0;
    this.film_ids = [];
    this.loadContent();
  }

  private checkRouteFilter(): void {
    const param = this.activatedRoute.snapshot.queryParams.genre;
    if (param) {
      this.activeGenre = this.genres.find(genre => this.translitService.convertCyrillToLatin(genre.name_en) === param);
      if (!this.activeGenre) {
        this.router.navigate(['404']);
      }
    }
  }

  private loadContent(): void {
    if (this.activeGenre) {
      this.dataService.getVideoContent(this.category_id, this.activeGenre.id, this.loadPart,
        this.skip, this.selectedCountry, this.selectedRatingFilter).then(result => {
        this.totalCount = result.total;
        this.film_ids.push(...result.videos.map(item => {
          return item.tvshow_id ? item.tvshow_id : item.video_id;
        }));
        this.loadingService.stopLoading();
        this.skip += this.loadPart;
      });
    }
  }

  public onScroll() {
    if ((this.totalCount - this.skip) > 0) {
      this.loadContent();
    }
  }

  ngOnDestroy() {
    this.sharedService.disableEdu();
  }
}
