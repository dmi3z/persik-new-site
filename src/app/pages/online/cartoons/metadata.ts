import { MetaDefinition } from '@angular/platform-browser';
import { MetaData } from '../../../models/meta';

export function setGenreName(genre_name?: string): MetaData {
  const tags: MetaDefinition[] = [
    {
      name: 'keywords',
      content: `мультфильмы онлайн${genre_name ? ' в жанре ' + genre_name : ''}`
    },
    {
      name: 'description',
      content: `Смотрите новые мультфильмы ${genre_name ? 'в жанре ' + genre_name : ''} вместе с Persik. ✅Выгодные тарифы.
      ✅Всего 30 копеек за новые мультфильмы! ✅Архив 24 дня, Full HD качество, программа передач.
      info@persik.by`
    }
  ];

  const title = `Смотреть мультфильмы онлайн${genre_name ? ' в жанре ' + genre_name : ''}`;

  return {
    title,
    tags
  };
}


