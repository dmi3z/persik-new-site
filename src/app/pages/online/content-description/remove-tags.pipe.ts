import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'removeTagsPipe'
})

export class RemoveTagsPipe implements PipeTransform {
  transform(text: string) {
    const pattern = /(\<(\/?[^>]+)>)/gi;
    return text.replace(pattern, '');
  }
}
