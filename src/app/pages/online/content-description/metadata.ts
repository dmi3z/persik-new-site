import { MetaDefinition } from '@angular/platform-browser';
import { MetaData } from '../../../models/meta';


export function setContentData(type: string, content_name: string, videoInfortamion): MetaData {
  const tags: MetaDefinition[] = [
    {
      name: 'keywords',
      content: `cмотреть ${type} ${content_name} онлайн`
    },
    {
      name: 'description',
      content: `Смотреть фильм ${type} ${content_name} online в hd качестве.
      ✅ Первый месяц всего за 0,30 руб! ✅ Архив 24 дня, Full HD качество, 100 каналов, мультирум.
      info@persik.by`
    },
    {
      property: 'og:image',
      content: videoInfortamion.cover
    }
  ];

  const title = `Смотреть ${type} ${content_name} онлайн в хорошем качестве`;

  return {
    title,
    tags
  };
}

export function setCoursesMetadata(course_name: string, videoInfortamion): MetaData {
  const tags: MetaDefinition[] = [
    {
      name: 'keywords',
      content: `курсы онлайн, смотреть курсы онлайн`
    },
    {
      name: 'description',
      content: `Курсы ${course_name}, семинары, тренинги онлайн по выгодным ценам - интернет-телевидение Persik. Онлайн-обучение в Минске.`
    },
    {
      property: 'og:image',
      content: videoInfortamion.cover
    }
  ];

  const title = `Смотреть курсы ${course_name} онлайн, обучение`;

  return {
    title,
    tags
  };
}
