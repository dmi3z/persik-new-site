import { Book } from './../../audiobooks/litres.model';
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { BookFile } from '../../audiobooks/litres.model';

@Component({
  selector: 'app-book-chapter',
  templateUrl: 'book-chapter.component.html',
  styleUrls: ['book-chapter.component.scss']
})

export class BookChapterComponent {

  constructor() { }

  @Input() chapter: Book;
  @Output() playState = new EventEmitter();
  public isOpen: boolean;


  public toggleSeason(): void {
    this.isOpen = !this.isOpen;
  }

  public playItem(item: BookFile): void {
    this.playState.emit(item);
  }

}
