import { PipesModule } from './../../../pipes/pipes.module';
import { NgModule } from '@angular/core';
import { ContentDescriptionComponent } from './content-description.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FilmCardModule } from '../../../components/content-slider/film-card/film-card.module';
import { RemoveTagsPipe } from './remove-tags.pipe';
import { BookChapterComponent } from './book-сhapter/book-chapter.component';
import { DurationTimePipe } from './duration-time.pipe';

@NgModule({
  declarations: [
    ContentDescriptionComponent,
    RemoveTagsPipe,
    BookChapterComponent,
    DurationTimePipe
  ],
  imports: [
    CommonModule,
    PipesModule,
    FilmCardModule,
    RouterModule.forChild([
      {
        path: '',
        component: ContentDescriptionComponent
      }
    ])
  ]
})

export class ContentDescriptionPageModule { }
