import { trigger, state, style, transition, animate } from '@angular/animations';


export const Animation = {
    tooltipAnimation: trigger('tooltip', [
    state('normal', style({
      opacity: 0,
      transform: 'scale(0.6)'
    })),
    state('fadeIn', style({
      opacity: 1,
      transform: 'scale(1)'
    })),
    state('freeze', style({
      opacity: 1
    })),
    state('moveOut', style({
      opacity: 0,
      transform: 'translateX(300px) scale(0.4)'
    })),
    transition('normal => fadeIn', animate(100)),
    transition('fadeIn => freeze', animate(700)),
    transition('freeze => moveOut', animate(500))
  ])
}