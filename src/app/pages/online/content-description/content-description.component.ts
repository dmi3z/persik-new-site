import { Book, BookFile } from './../audiobooks/litres.model';
import { PaymentService } from './../../../services/payment.service';
import { PackageItem } from './../../../models/user';
import { LitresService } from './../audiobooks/litres.service';
import { Subscription } from 'rxjs';
import { MetaData } from './../../../models/meta';
import { MetaService } from './../../../services/meta.service';
import { isPlatformBrowser } from '@angular/common';
import { ModalName } from '../../../models/modal';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, PLATFORM_ID, Inject, OnDestroy, Renderer2 } from '@angular/core';
import { Video, Episode, Genre, Person, ContentName, ContentType, FavoriteSettings } from './../../../models';
import { Animation } from './tooltip.animation';
import { setContentData, setCoursesMetadata } from './metadata';
import {
  AuthService,
  TranslitService,
  DataService,
  ModalController,
  TimeService,
  FavoriteService,
  SharedService
} from './../../../services';
import { Option } from '../../../models/tariff';

@Component({
  selector: 'app-content-description',
  templateUrl: './content-description.component.html',
  styleUrls: ['./content-description.component.scss'],
  animations: [Animation.tooltipAnimation]
})

export class ContentDescriptionComponent implements OnInit, OnDestroy {

  public state = 'normal'; // animation state

  public genres: string[] = [];
  public actorNames: string[] = [];
  public directors: string[] = [];
  public authors: string[] = [];
  public contentInformation: Video;
  private content_id: string;
  public actors: Person[];
  public isLoadError: boolean;
  public isCanPlay: boolean;
  public archiveAt: number;
  public channelName: string;
  public isBrowser: boolean;
  public contentName: string;
  public isAudiobook: boolean;

  public isTrialPlaying: boolean;
  public isFullPlaying: boolean;

  private activeSeason: string;
  private category_id: number;
  public series: Episode[] = [];
  public seriescontentInformation: Video[] = [];
  public similarVideos: Video[] = [];

  public contentDescription: string;
  public isContentDescriptionSliced: boolean;
  public isContentDescriptionOpen: boolean;

  public isBought: boolean;
  public bookInfo: Book;

  private activatedRouteSubscriber: Subscription;

  public slideConfig = {
    'slidesToShow': 6,
    'slidesToScroll': 1,
    'arrows': true,
    'dots': false,
    'fade': false,
    'centerMode': false,
    'autoplay': false,
    'responsive': [
      {
        'breakpoint': 1200,
        'settings': {
          'slidesToShow': 4,
        }
      },
      {
        'breakpoint': 992,
        'settings': {
          'slidesToShow': 3,
        }
      },
      {
        'breakpoint': 767,
        'settings': {
          'slidesToShow': 1,
        }
      },
    ]
  };

  constructor(
    private activatedRoute: ActivatedRoute,
    private dataService: DataService,
    private authService: AuthService,
    private router: Router,
    private translitService: TranslitService,
    private timeService: TimeService,
    private favoriteService: FavoriteService,
    private modalController: ModalController,
    @Inject(PLATFORM_ID) private platform: any,
    private metaService: MetaService,
    private sharedService: SharedService,
    private litresService: LitresService,
    private renderer: Renderer2,
    private paymentService: PaymentService
  ) {
  }

  ngOnInit() {
    if (isPlatformBrowser(this.platform)) {
      this.isBrowser = true;
    }
    const id: string = this.activatedRoute.snapshot.params.id;
    const params = id.split('-');
    this.content_id = params[params.length - 1];
    this.isAudiobook = params.includes('book');
    if (this.isAudiobook) {
      this.loadAudioBookInfo();
    } else {
      this.loadInfo();
    }
  }

  public isShowByCategoryPath(path: string): boolean {
    return this.category.path === path;
  }

  public get isPladform(): boolean {
    if (this.contentInformation) {
      return this.contentInformation.is_pladform;
    }
    return false;
  }

  public get isLogin(): boolean {
    return this.authService.isLogin;
  }

  public get isHaveActors(): boolean {
    if (this.actors) {
      return this.actors.length > 0;
    }
    return false;
  }

  public get isSeries(): boolean {
    if (this.contentInformation) {
      return this.contentInformation.is_series;
    }
    return false;
  }

  public splitFirstName(e): string {
    const firstName = e.split(' ');
    return firstName[0];
  }

  public goToOnline(): void {
    this.router.navigate(['']);
  }

  public splitLastName(e): string {
    const lastName = e.split(' ').slice(1).join(' ');
    return lastName;
  }

  public get isHaveDirectors(): boolean {
    return this.directors.length > 0;
  }

  public playVideo(): void {
    if (!this.contentInformation.is_pladform) {
      const cpu: string = this.translitService.convertCyrillToLatin(this.contentInformation.name);
      if (this.isSeries) {
        if (this.series[0].video_id) {
          this.dataService.getVideoStream(+this.content_id)
            .then(_ => this.router.navigate(['video-player', `${cpu}-${this.series[0].video_id}`]))
            .catch(_ => this.modalController.present(ModalName.FAST_AUTH));
        } else {
          this.dataService.getTvshowStream(this.content_id)
            .then(_ => this.router.navigate(['video-player', `${cpu}-${this.series[0].tvshow_id}`]))
            .catch(_ => this.modalController.present(ModalName.FAST_AUTH));
        }
      } else {
        if (this.content_id.length < 10) {
          this.dataService.getVideoStream(+this.content_id)
            .then(_ => this.router.navigate(['video-player', `${cpu}-${this.content_id}`]))
            .catch(_ => this.modalController.present(ModalName.FAST_AUTH));
        } else {
          this.dataService.getTvshowStream(this.content_id)
            .then(_ => this.router.navigate(['video-player', `${cpu}-${this.content_id}`]))
            .catch(_ => this.modalController.present(ModalName.FAST_AUTH));
        }
      }
    } else {
      if (this.contentInformation.is_series) {
        this.router.navigate(['player', this.seriescontentInformation[0].pladform_id]);
      } else {
        this.router.navigate(['player', this.contentInformation.pladform_id]);
      }
    }
  }

  public get uniqueSeasons(): string[] { // Получение массива уникальных названий сезонов
    return this.contentInformation.episodes.map(item => item.season).filter((item, index, self) => self.indexOf(item) === index).sort();
  }

  public loadSeriesBySeason(season: string): void { // Получение серий по номеру сезона
    this.activeSeason = season;
    this.series = [];
    this.seriescontentInformation = [];
    this.series = this.contentInformation.episodes.filter(item => item.season === this.activeSeason).sort((a, b) => {
      return +a.episode.split(' ')[1] - +b.episode.split(' ')[1];
    });
    for (let i = 0; i < this.series.length; i++) {
      if (this.series[i].video_id) {
        this.dataService.getVideoInfo(this.series[i].video_id).then(res => {
          this.seriescontentInformation[i] = res;
        }).catch(er => console.log(er));
      } else {
        this.dataService.getTvshowInfo(this.series[i].tvshow_id).then(res => {
          if (res) {
            this.dataService.getVideoInfo(+res.video_id).then(info => {
              this.seriescontentInformation[i] = info;
              this.seriescontentInformation[i].tvshow_id = this.series[i].tvshow_id;
              this.seriescontentInformation[i].name = this.series[i].episode;
            }).catch(er => console.log(er));
          }
        });
      }
    }
  }

  public selectSeason(season: string): void {
    this.activeSeason = season;
  }

  public isSeasonActive(season: string): boolean {
    return this.activeSeason === season;
  }

  public showVideo(id: number): void {
    if (!this.contentInformation.is_pladform) {
      const name: string = this.contentInformation.international_name ?
        this.contentInformation.international_name : this.contentInformation.name;
      const cpu: string = this.translitService.convertCyrillToLatin(name);
      this.router.navigate(['video-player/', `${cpu}-${id}`]);
    } else {
      /* this.dataService.getVideoInfo(id).then(res => {
        this.router.navigate(['player', res.pladform_id]);
      }); */
      const serie = this.seriescontentInformation.find(s => s.video_id === id);
      this.router.navigate(['player', serie.pladform_id]);
    }
  }

  public showSerie(serie: Video): void {
    if (serie.tvshow_id) {
      const name: string = this.contentInformation.international_name ?
        this.contentInformation.international_name : this.contentInformation.name;
      const cpu: string = this.translitService.convertCyrillToLatin(name);
      this.router.navigate(['video-player/', `${cpu}-${serie.tvshow_id}`]);
    } else {
      this.showVideo(serie.video_id);
    }
  }

  public get isFavorite(): boolean {
    if (this.content_id.length > 0) {
      switch (this.type) {
        case ContentName.TV:
          return this.favoriteService.isTvshowFavorite(this.content_id);
        case ContentName.BOOK:
          return this.favoriteService.isBookFavorite(+this.content_id);
        default:
          return this.favoriteService.isVideoFavorite(+this.content_id);
      }
    }
  }


  public addToFavorite(): void {
    this.state = 'fadeIn';
    this.favoriteService.addToFavorite(new FavoriteSettings(this.content_id, this.type));
  }

  public removeFromFavorite(): void {
    this.state = 'fadeIn';
    this.favoriteService.removeFromFavorite(new FavoriteSettings(this.content_id, this.type));
  }

  public animationEnded(event): void {
    const toState: string = event.toState;
    switch (toState) {
      case 'fadeIn':
        this.state = 'freeze';
        break;
      case 'freeze':
        this.state = 'moveOut';
        break;
      case 'moveOut':
        this.state = 'normal';
        break;
      default:
        break;
    }
  }

  public get type(): ContentType {
    if (this.isVideo) {
      return ContentName.VIDEO;
    }
    if (this.isAudiobook) {
      return ContentName.BOOK;
    }
    return ContentName.TV;
  }

  public login(): void {
    this.modalController.present(ModalName.LOGIN);
  }

  public toggleContentDescription(): void {
    if (this.isContentDescriptionOpen) {
      this.isContentDescriptionOpen = false;
      this.sliceContentDescription(this.contentDescription);
    } else {
      this.isContentDescriptionOpen = true;
      this.contentDescription = this.contentInformation.description;
    }
  }

  public get isVideo(): boolean {
    return this.content_id.length < 10 && !this.isAudiobook;
  }

  public get category(): { name: string; path: string } {
    switch (this.category_id) {
      case 1:
        return {
          name: 'Фильмы',
          path: 'films'
        };
      case 3:
        return {
          name: 'Мультфильмы',
          path: 'multfilmy'
        };
      case 2:
        return {
          name: 'Сериалы',
          path: 'serialy'
        };
      case 4:
        return {
          name: 'Передачи',
          path: 'peredachi'
        };
      case 6:
        return {
          name: 'Курсы',
          path: 'kursy-online'
        };
      case 7:
        return {
          name: 'Аудиокниги',
          path: 'audio-books'
        };
      default:
        return {
          name: '',
          path: ''
        };
    }
  }

  /* public get isHaveSimilar(): boolean {
    return this.similarVideos.length > 0 && this.category.path !== 'kursy-online';
  }

  private loadSimilar(): void {
    this.dataService.getSimilar(this.content_id).then(response => {
      if (response) {
        this.similarVideos = response;
      }
    });
  } */

  private loadAudioBookInfo(): void {
    this.litresService.getUsersBooks().then(res => {
      if (res) {
        const boughtBook = res.find(book => book.id === +this.content_id);
        this.isBought = !!boughtBook;
      }
    });
    this.litresService.getBookInfo([+this.content_id])
      .then(data => {
        this.bookInfo = data[0];
        this.contentInformation = {
          description: this.bookInfo.annotation,
          name: this.bookInfo.title,
          age_rating: this.bookInfo.adult ? this.bookInfo.adult.toString() : null,
          category_id: 7,
          duration: this.bookInfo.chars,
          genres: [],
          episodes: null,
          is_pladform: false,
          is_series: this.bookInfo.file_groups[1] && this.bookInfo.file_groups[1].files && this.bookInfo.file_groups[1].files.length > 1 ? true : false,
          international_name: null,
          year: this.bookInfo.date_written_s ? this.bookInfo.date_written_s.toString() : null,
          ratings: null,
          tvshow_id: null,
          video_id: null,
          cover: this.litresService.getCover(+this.content_id, this.bookInfo.cover),
          countries: null,
          cast: [],
          art: null,
          director: []
        };
        this.category_id = 7;
        this.genres = this.bookInfo.genres.map(genre => genre.title);
        this.authors = this.bookInfo.authors.map(author => {
          return author['first-name'] + ' ' + author['last-name'];
        });
        this.contentName = this.getNameWithColor(this.bookInfo.title);
        this.sliceContentDescription(this.contentInformation.description);
      });
  }

  public get isBook(): boolean {
    return this.bookInfo && (this.bookInfo.type === 0 || this.bookInfo.type === 4);
  }

  public buyBook(): void {
    const item: Option = {
      price: this.bookInfo.price_byn,
      product_option_id: +this.content_id,
      name: this.contentInformation.name,
      is_book: true,
    };
    this.paymentService.setTariff(item, item.name);
    this.router.navigate(['private/payment']);
  }

  public playTrial() {
    const audio = document.querySelector('.audio-player') as HTMLAudioElement;
    this.isTrialPlaying = true;
    if (audio) {
      return audio.play();
    }
    this.createAudioTag(this.litresService.getTrial(+this.content_id));
  }

  public playBook() {
    const audio = document.querySelector('.audio-player') as HTMLAudioElement;
    this.isFullPlaying = true;
    if (audio) {
      return audio.play();
    }
    this.createAudioTag(this.bookInfo.file_groups[1].files[0].stream);
  }

  private playBookStream(stream: string) {
    const audio = document.querySelector('.audio-player') as HTMLAudioElement;
    this.isFullPlaying = true;
    if (audio) {
      this.destroyAudioTag();
    }
    this.createAudioTag(stream);
  }

  public onPlayChapter(book: BookFile): void {
    this.playBookStream(book.stream);
  }

  public get downloadBookLink(): string {
    if (this.contentInformation && this.contentInformation.is_series && this.bookInfo.file_groups[3]) {
      return this.bookInfo.file_groups[3].files[0].download;
    }
    if (this.bookInfo && this.bookInfo.file_groups[1]) {
      return this.bookInfo.file_groups[1].files[0].download;
    }
  }

  public get downloadTrialBookLink(): string {
    if (this.bookInfo.type === 0) {
      return `https://litres.ru/gettrial/?art=${this.bookInfo.id}&lfrom=677360965&format=fb2.zip&clean=1`;
    }
    if (this.bookInfo.type === 4) {
      return `https://partnersdnld.litres.ru/get_pdf_trial/${this.bookInfo.id}.pdf`;
    }
  }

  private destroyAudioTag(): void {
    const audio = document.querySelector('.audio-player') as HTMLAudioElement;
    if (audio) {
      audio.pause();
      this.isFullPlaying = false;
      this.isTrialPlaying = false;
      const container = document.querySelector('.content-desc__info');
      this.renderer.removeChild(container, audio);
    }
  }

  private createAudioTag(url: string): void {
    const audio = this.renderer.createElement('audio');
    this.renderer.setAttribute(audio, 'src', url);
    this.renderer.setAttribute(audio, 'controls', 'true');
    this.renderer.addClass(audio, 'audio-player');
    const container = document.querySelector('.content-desc__info');
    this.renderer.appendChild(container, audio);
    audio.play();
    this.isTrialPlaying = true;
    this.isFullPlaying = true;
    audio.addEventListener('ended', () => {
      this.isTrialPlaying = false;
      this.isFullPlaying = false;
    });
  }

  public pauseTrial(): void {
    const audio = document.querySelector('audio');
    if (audio) {
      audio.pause();
      this.isTrialPlaying = false;
    }
  }

  public pauseBook(): void {
    const audio = document.querySelector('audio');
    if (audio) {
      audio.pause();
      this.isFullPlaying = false;
    }
  }

  private loadInfo(): void {
    if (this.isVideo) {
      this.loadVideoInfo(+this.content_id);
    } else {
      this.dataService.getTvshowInfo(this.content_id).then(response => {
        if (response) {
          this.loadVideoInfo(+response.video_id);
          this.dataService.getChannelById(response.channel_id).then(channel => {
            if (channel) {
              const currentTime: number = this.timeService.currentTime;
              if (response.start > (currentTime - channel.dvr_sec) && response.stop < currentTime) {
                this.archiveAt = response.start;
                this.channelName = channel.name;
                this.isCanPlay = true;
              } else {
                this.isCanPlay = false;
              }
            }
          });
        } else {
          this.isCanPlay = false;
          this.isLoadError = true;
        }
      });
    }
  }

  private loadVideoInfo(id: number): void {
    this.dataService.getVideoInfo(id).then(response => {
      if (response) {
        this.category_id = response.category_id;
        this.contentInformation = response;
        let meta: MetaData;
        if (response.category_id !== 6) {
          meta = setContentData(this.getCategory(response.category_id), response.name, this.contentInformation);
        } else {
          this.sharedService.enableEdu();
          meta = setCoursesMetadata(response.name, this.contentInformation);
        }
        this.metaService.updateMeta(meta);
        this.contentInformation.description = this.clearDescription(response.description);
        this.contentName = this.getNameWithColor(response.name);
        this.loadGenres();
        this.loadPersons();
        if (this.isSeries) {
          this.loadSeriesBySeason(this.uniqueSeasons[0]);
        }
        this.isCanPlay = true;
        if (this.contentInformation.description) {
          this.sliceContentDescription(this.contentInformation.description);
        }

      } else {
        this.isCanPlay = false;
        this.isLoadError = true;
      }
    });
  }

  private getCategory(id: number): string {
    switch (id) {
      case 1:
        return 'фильм';
      case 2:
        return 'сериал';
      case 3:
        return 'мультфильм';
      case 4:
        return 'передачу';
      default:
        break;
    }
  }

  private sliceContentDescription(description: string): void {
    this.contentDescription = description;
    this.isContentDescriptionOpen = true;
    if (description.length > 220) {
      this.contentDescription = description.slice(0, 220) + '...';
      this.isContentDescriptionSliced = true;
      this.isContentDescriptionOpen = false;
    }
  }

  private loadGenres(): void { // Получение списка жанров для видео по id жанров
    this.dataService.loadVodCategories().then(res => {
      const allGenresArray = res.map(cat => cat.genres);
      const allGenres: Genre[] = [];
      allGenresArray.forEach(genre => {
        allGenres.push(...genre);
      });

      this.genres = allGenres.filter(genre => {
        return this.contentInformation.genres.some(genreId => genreId === genre.id);
      }).map(resp => {
        const name = resp.name;
        const part = name.split(')');
        if (part[1]) {
          return part[1];
        }
        return name;
      });
    });

  }

  private loadPersons(): void {
    if (this.contentInformation.cast && this.contentInformation.cast.length > 0) {
      this.dataService.loadActors(this.contentInformation.cast).then(res => {
        this.actorNames = res.map(actor => actor.name);
        this.actors = res;
      });
    }
    if (this.contentInformation.director && this.contentInformation.director.length > 0) {
      this.dataService.loadActors(this.contentInformation.director).then(res => {
        this.directors = res.map(director => director.name);
      });
    }
  }

  private getNameWithColor(name: string): string {
    if (name) {
      const nameArray: string[] = name.split(' ');
      if (nameArray.length === 1) {
        return name;
      }
      let colorString: string = nameArray.shift();
      colorString += ' <span class="orange-text">';
      const lastText: string = nameArray.toString();
      colorString += lastText.replace(new RegExp(',', 'g'), ' ');
      colorString += '</span>';
      return colorString;
    }
  }

  private clearDescription(description: string): string {
    return description.replace(new RegExp(/\&[a-z]*\;/, 'g'), ' ');
  }

  ngOnDestroy() {
    this.sharedService.disableEdu();
    if (this.activatedRouteSubscriber) {
      this.activatedRouteSubscriber.unsubscribe();
    }
  }

}
