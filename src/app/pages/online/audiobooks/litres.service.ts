import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BookGenre, Book } from './litres.model';

@Injectable({ providedIn: 'root' })

export class LitresService {

  private readonly LITRES_BASE_URL = 'https://api.persik.by/v2/litres';
  private readonly TRIAL_MP3_URL = 'https://partnersdnld.litres.ru/get_mp3_trial/';

  private genres: BookGenre[] = [];

  constructor(private http: HttpClient) { }

  getGenres(type = 'audio'): Promise<BookGenre[]> {
    return new Promise((resolve, reject) => {
      if (this.genres.length > 0) {
        resolve(this.genres);
        return;
      }

      this.http.get<BookGenre[]>(this.LITRES_BASE_URL.concat('/genres?', 'type=', type)).toPromise()
        .then(data => {
          this.genres = data;
          data.unshift({
            count: 100000,
            litres_genre_id: -1,
            name: 'Бестселлеры',
            type: null,
            token: null
          });
          resolve(data);
        })
        .catch(() => reject());
    });
  }

  getList(genreId: number, offset: number, size: number, type = 'audio'): Promise<Book[]> {
    const paramsData = {
      size,
      offset,
      type
    };
    if (genreId !== -1) {
      Object.assign(paramsData, { genre_id: genreId });
    }
    let params = new HttpParams();
    Object.keys(paramsData).forEach(key => {
      params = params.set(key, paramsData[key]);
    });
    return this.http.get<Book[]>(this.LITRES_BASE_URL, { params }).toPromise();
  }

  getBookInfo(ids: number[]): Promise<Book[]> {
    let paramString = '';
    ids.forEach(id => {
      paramString += `id[]=${id}&`;
    });
    paramString.slice(-1);
    return this.http.get<Book[]>(this.LITRES_BASE_URL.concat('/item?', paramString)).toPromise();
  }

  getUsersBooks(): Promise<Book[]> {
    const params = new HttpParams()
      .set('bought', '1');
    return this.http.get<Book[]>(this.LITRES_BASE_URL, { params }).toPromise();
  }

  getCover(id: number, coverFormat = 'jpg'): string {
    return 'https://partnersdnld.litres.ru/pub/c/cover/' + id + '.' + coverFormat;
  }

  getTrial(id: number): string {
    return 'https://partnersdnld.litres.ru/get_mp3_trial/' + id + '.mp3';
  }

}
