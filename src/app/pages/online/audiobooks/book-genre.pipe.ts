import { Genre } from './../../../models/category';
import { BookGenre } from './litres.model';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'bookGenrePipe'
})

export class BookGenrePipe implements PipeTransform {
  transform(value: BookGenre[]): Genre[] {
    if (value.length > 0) {
      const genres: Genre[] = value.map(bookGenre => {
        const genre: Genre = {
          id: bookGenre.litres_genre_id,
          is_main: true,
          name: bookGenre.name,
          name_en: bookGenre.token
        };
        return genre;
      });
      return genres;
    }

  }
}
