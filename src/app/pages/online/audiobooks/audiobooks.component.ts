import { Component, OnInit, Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { Genre } from '../../../models';
import { LoadingService, MetaService, DataService } from '../../../services';
import { setGenreName } from './metadata';
import { LitresService } from './litres.service';
import { BookGenre, Book } from './litres.model';

@Component({
  selector: 'app-films',
  templateUrl: './audiobooks.component.html',
  styleUrls: ['./audiobooks.component.scss']
})

export class AudioBooksPageComponent implements OnInit {

  public bookGenres: BookGenre[] = [];
  public activeCategory: BookGenre;
  public isBrowser = false;
  public books: Book[] = [];
  private totalCount: number;
  private skip = 0;
  private readonly loadPart: number = 16; // Количество разово загружаемых аудиокниг на отображение в ленте
  public bannerUrl: string;


  constructor(
    private loadingService: LoadingService,
    @Inject(PLATFORM_ID) private platform: any,
    private metaService: MetaService,
    private dataService: DataService,
    private litresService: LitresService
  ) {}

  ngOnInit(): void {
    this.metaService.updateMeta(setGenreName());
    this.metaService.createCanonicalURL();
    if (isPlatformBrowser(this.platform)) {
      this.isBrowser = true;
    }
    this.loadingService.startLoading();
    this.dataService.getBannersForBook().then(res => this.bannerUrl = res[0].img_url_desktop);
    this.litresService.getGenres().then(genres => {
      this.bookGenres = genres;
      this.activeCategory = genres[0]; // this.activeCategory.genres[0];
      this.checkRouteFilter();
      this.loadContent();
    });
  }

  public onCategoryChange(category: Genre): void {
    this.activeCategory = this.bookGenres.find(bg => bg.litres_genre_id === category.id);
  // this.activeCategory.genres[0];
    this.skip = 0;
    this.books = [];
    this.loadContent();

    /* if (genre) {
      this.activeGenre = genre;
      if (genre.name === 'Все жанры') {
        this.activeGenre.id = 0;
        this.metaService.updateMeta(setGenreName());
        this.location.go(`/audio-books`);
      } else {
        const routeName = genre.name_en ? genre.name_en : genre.name;
        const cpu: string = this.translitService.convertCyrillToLatin(routeName);
        this.location.go(`/audio-books?genre=${cpu}`);
        this.metaService.updateMeta(setGenreName(genre.name));
      }
    } else {
      this.activeGenre = this.genres[0];
      this.metaService.updateMeta(setGenreName());
      this.location.go(`/audio-books`);
    } */
  }

  public get isHaveGenres(): boolean {
    return this.bookGenres.length > 0;
  }

  private checkRouteFilter(): void {
    /* const param = this.activatedRoute.snapshot.queryParams.genre;
    if (param) {
      this.activeGenre = this.genres.find(genre => this.translitService.convertCyrillToLatin(genre.name_en) === param);
      if (!this.activeGenre) {
        this.router.navigate(['404']);
      }
    } */
  }

  private loadContent(): void {
    if (this.activeCategory) {
      this.loadingService.startLoading();
      this.litresService.getList(this.activeCategory.litres_genre_id, this.skip, this.loadPart).then(res => {
        this.totalCount = this.activeCategory.count;
        this.skip += this.loadPart;
        this.books.push(...res);
      }).finally(() => this.loadingService.stopLoading());
    }
  }

  public onScroll() {
    if ((this.totalCount - this.skip) > 0) {
      this.loadContent();
    }
  }
}
