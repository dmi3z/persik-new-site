import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { NgSelectModule } from '@ng-select/ng-select';
import { GenreFilterModule } from './../genre-filter/genre-filter.module';
import { AudioBooksPageComponent } from './audiobooks.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { AudiobookCardModule } from './audiobook-card/audiobook-card.module';
import { BooksPipeModule } from './books-pipe.module';

@NgModule({
  declarations: [
    AudioBooksPageComponent
  ],
  imports: [
    CommonModule,
    GenreFilterModule,
    NgSelectModule,
    FormsModule,
    RouterModule,
    AudiobookCardModule,
    InfiniteScrollModule,
    BooksPipeModule,
    RouterModule.forChild([
      {
        path: '',
        component: AudioBooksPageComponent
      }
    ])
  ]
})

export class AudiobooksPageModule { }
