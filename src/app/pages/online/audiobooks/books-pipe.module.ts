import { NgModule } from "@angular/core";
import { ToGenreConverterPipe } from './to-genre-converter.pipe';
import { BookGenrePipe } from './book-genre.pipe';
import { CommonModule } from '@angular/common';

@NgModule({
    declarations: [
        ToGenreConverterPipe,
        BookGenrePipe
    ],
    imports: [
        CommonModule
    ],
    exports: [
        ToGenreConverterPipe,
        BookGenrePipe
    ]
})

export class BooksPipeModule { }
