import { Genre } from './../../../models/category';
import { BookGenre } from './litres.model';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'toGenreConverterPipe'
})

export class ToGenreConverterPipe implements PipeTransform {
  transform(bookGenre: BookGenre): Genre {
    return {
      id: bookGenre.litres_genre_id,
      is_main: true,
      name: bookGenre.name,
      name_en: bookGenre.token
    }
  }
}
