import { CommonModule } from '@angular/common';
import { AudioBookCardComponent } from './audiobook-card.component';
import { NgModule } from '@angular/core';

@NgModule({
  declarations: [
    AudioBookCardComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    AudioBookCardComponent
  ]
})

export class AudiobookCardModule { }
