import { LitresService } from './../litres.service';
import { TranslitService } from './../../../../services/translit.service';
import { Router } from '@angular/router';
import { isPlatformBrowser } from '@angular/common';
import { Component, Inject, PLATFORM_ID, OnInit, Input } from '@angular/core';
import { Book } from '../litres.model';

@Component({
  selector: 'app-audiobook-card',
  templateUrl: 'audiobook-card.component.html',
  styleUrls: ['audiobook-card.component.scss']
})

export class AudioBookCardComponent implements OnInit {

  public isBrowser: boolean;
  public cover: string;

  @Input() isBigPoster: boolean;
  @Input() book: Book;

  constructor(
    @Inject(PLATFORM_ID) private platform: any,
    private router: Router,
    private transliteService: TranslitService,
    private litresService: LitresService
  ) { }

  ngOnInit() {
    if (isPlatformBrowser(this.platform)) {
      this.isBrowser = true;
    }
    this.loadCover();
  }

  public openDescription(): void {
    this.router.navigate(['/details', this.urlName]);
  }

  private get urlName(): string {
    let transliteName = this.transliteService.convertCyrillToLatin(this.book.title);
    transliteName = transliteName + '-book-' + this.book.id.toString();
    return transliteName;
  }

  private loadCover(): void {
    this.cover = this.litresService.getCover(this.book.id, this.book.cover);
  }

}
