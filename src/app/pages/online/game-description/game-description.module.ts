import { NgModule } from "@angular/core";
import { GameDescriptionComponent } from './game-description.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

@NgModule({
    declarations: [
        GameDescriptionComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild([
            {
                path: '',
                component: GameDescriptionComponent
            }
        ])
    ]
})

export class GameDescriptionModule { }
