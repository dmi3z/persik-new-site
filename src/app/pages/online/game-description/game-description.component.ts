import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { GAMES_DATA } from '../games/games-data';
import { Game } from '../games/games-page.component';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'app-game-description',
    templateUrl: 'game-description.component.html',
    styleUrls: ['game-description.component.scss']
})

export class GameDescriptionComponent implements OnInit {

    public games: Game[] = GAMES_DATA;
    public game: Game;
    public gameName: string;

    public isContentDescriptionSliced: boolean;
    public isContentDescriptionOpen: boolean;
    public contentDescription: string;

    @ViewChild('gamearea') gamearea: ElementRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router) { }

    ngOnInit() {
        const id = this.activatedRoute.snapshot.params.id;
        this.game = this.games.find(game => game.id === Number(id));
        if (this.game) {
            this.gameName = this.getNameWithColor(this.game.name);

            if (this.game.description) {
                this.sliceContentDescription(this.game.description);
            }
        }
    }

    /* ngAfterViewInit() {
        if (this.game) {
            this.createIframe(this.game.url);
        }
    } */

    public get isLoadError(): boolean {
        return !this.game;
    }

    public toggleContentDescription(): void {
        if (this.isContentDescriptionOpen) {
            this.isContentDescriptionOpen = false;
            this.sliceContentDescription(this.contentDescription);
        } else {
            this.isContentDescriptionOpen = true;
            this.contentDescription = this.game.description;
        }
    }

    public play(): void {
        this.router.navigate(['game-player', this.game.id]);
    }

    private getNameWithColor(name: string): string {
        if (name) {
            const nameArray: string[] = name.split(' ');
            if (nameArray.length === 1) {
                return name;
            }
            let colorString: string = nameArray.shift();
            colorString += ' <span class="orange-text">';
            const lastText: string = nameArray.toString();
            colorString += lastText.replace(new RegExp(',', 'g'), ' ');
            colorString += '</span>';
            return colorString;
        }
    }

    private sliceContentDescription(description: string): void {
        this.contentDescription = this.game.description;
        this.isContentDescriptionOpen = true;
        if (description.length > 220) {
            this.contentDescription = description.slice(0, 220) + '...';
            this.isContentDescriptionSliced = true;
            this.isContentDescriptionOpen = false;
        }
    }

    /* private createIframe(url: string): void {
        const iframe = this.renderer.createElement('iframe');
        this.renderer.addClass(iframe, 'gameframe');
        this.renderer.setAttribute(iframe, 'allow', 'fullscreen');
        this.renderer.setAttribute(iframe, 'src', url);
        this.renderer.appendChild(this.gamearea.nativeElement, iframe);
    } */
}
