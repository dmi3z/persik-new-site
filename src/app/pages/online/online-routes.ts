import { AuthGuard } from "./../../services/auth.guard";
import { Routes } from "@angular/router";
import { OnlineComponent } from "./online.component";

export const onlineRoutes: Routes = [
  {
    path: "",
    component: OnlineComponent,
    children: [
      // {
      //   path: '',
      //   loadChildren: './featured/featured.module#FeaturedPageModule'
      // },
      {
        path: "",
        loadChildren: "./review/review.module#ReviewPageModule",
      },
      {
        path: "serialy",
        loadChildren: "./series/series.module#SeriesPageModule",
      },
      {
        path: "films",
        loadChildren: "./films/films.module#FilmsPageModule",
      },
      {
        path: "multfilmy",
        loadChildren: "./cartoons/cartoons.module#CartoonsPageModule",
      },
      {
        path: "peredachi",
        loadChildren: "./tvshows/tvshows.module#TvshowsPageModule",
      },
      {
        path: "kursy-online",
        loadChildren: "./courses/courses.module#CoursesPageModule",
      },
      {
        path: "details/:id",
        loadChildren:
          "./content-description/content-description.module#ContentDescriptionPageModule",
      },
      {
        path: "tv-guide",
        loadChildren: "./tv-guide/tv-guide.module#TvGuideModule",
      },
      {
        path: "izbrannoe",
        loadChildren: "./favorites/favorite-page.module#FavoritePageModule",
        canActivate: [AuthGuard],
      },
      {
        path: "search-results",
        loadChildren: "./search/search.module#SearchPageModule",
      },
      {
        path: "channels/:id",
        loadChildren: "./channel-page/channel-page.module#ChannelPageModule",
      },
      {
        path: "course-page/:id",
        loadChildren: "./course-page/course-page.module#CoursePageModule",
      },
      {
        path: "audio-books",
        loadChildren: "./audiobooks/audiobooks.module#AudiobooksPageModule",
      },
      {
        path: "books",
        loadChildren: "./books/books.module#BooksPageModule",
      },
      {
        path: "games",
        loadChildren: "./games/games-page.module#GamesPageModule",
      },
      {
        path: "game-description/:id",
        loadChildren:
          "./game-description/game-description.module#GameDescriptionModule",
      },
      {
        path: "blogers",
        loadChildren: "./blogers/blogers.module#BlogersPageModule",
      },
      {
        path: "radio",
        loadChildren: "./radio/radio.module#RadioPageModule",
      },
    ],
  },
];
