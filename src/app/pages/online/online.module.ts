import { GenreFilterModule } from './genre-filter/genre-filter.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { onlineRoutes } from './online-routes';
import { OnlineComponent } from './online.component';


@NgModule({
  declarations: [
    OnlineComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(onlineRoutes),
    GenreFilterModule
  ]
})

export class OnlineModule {}
