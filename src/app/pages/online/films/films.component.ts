import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, Inject, PLATFORM_ID } from '@angular/core';
import { Location, isPlatformBrowser } from '@angular/common';
import { Country, RatingFilter, Genre } from '../../../models';
import { LoadingService, DataService, MetaService, TranslitService } from '../../../services';
import { ratingFilters, dateFilters } from '../filtersdata';
import { setGenreName } from './metadata';
import { Router } from '@angular/router';

@Component({
  selector: 'app-films',
  templateUrl: './films.component.html',
  styleUrls: ['./films.component.scss']
})

export class FilmsComponent implements OnInit {
  public genres: Genre[] = [];
  public activeGenre: Genre;
  public film_ids: any[] = [];
  public isBrowser = false;
  public countries: Country[] = [];
  private selectedCountry: Country;
  private selectedRatingFilter: RatingFilter;

  public ratingFilters: RatingFilter[] = ratingFilters;
  public dateFilter: RatingFilter[] = dateFilters;

  private category_id: number;
  private totalCount: number;
  private skip = 0;
  private readonly loadPart: number = 16; // Количество разово загружаемых видео на отображение в ленте

  constructor(
    private dataService: DataService,
    private activatedRoute: ActivatedRoute,
    private translitService: TranslitService,
    private loadingService: LoadingService,
    private location: Location,
    @Inject(PLATFORM_ID) private platform: any,
    private metaService: MetaService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.metaService.updateMeta(setGenreName());
    this.metaService.createCanonicalURL();
    if (isPlatformBrowser(this.platform)) {
      this.isBrowser = true;
    }
    this.loadingService.startLoading();
    this.dataService.loadVodCategories().then(categories => {
      if (categories[0]) {
        this.category_id = categories[0].id;
        this.genres = categories[0].genres.filter(genre => genre.is_main).map(genre => {
          return {
            id: genre.id,
            is_main: true,
            name: genre.name.replace(/[0-9]\)/gmi,''),
            name_en: genre.name_en
          }
        });
        this.loadCountryList();
        this.activeGenre = this.genres[0];
        this.checkRouteFilter();
        this.loadContent();
      }
    });
  }

  public get isHaveContent(): boolean {
    return this.film_ids.length > 0;
  }

  public get isHaveGenres(): boolean {
    return this.genres.length > 0;
  }

  public onGenreChange(genre: Genre): void {
    if (genre) {
      this.activeGenre = genre;
      if (genre.name === 'Все жанры') {
        this.activeGenre.id = 1;
        this.metaService.updateMeta(setGenreName());
        this.location.go(`/films`);
      } else {
        const routeName = genre.name_en ? genre.name_en : '';
        const cpu: string = this.translitService.convertCyrillToLatin(routeName);
        this.location.go(`/films?genre=${cpu}`);
        this.metaService.updateMeta(setGenreName(genre.name));
      }
    } else {
      this.activeGenre = this.genres[0];
      this.metaService.updateMeta(setGenreName());
      this.location.go(`/films`);
    }
    this.skip = 0;
    this.film_ids = [];
    this.loadContent();
  }

  private checkRouteFilter(): void {
    const param = this.activatedRoute.snapshot.queryParams.genre;
    if (param) {
      this.activeGenre = this.genres.find(genre => this.translitService.convertCyrillToLatin(genre.name_en) === param);
      if (!this.activeGenre) {
        this.router.navigate(['404']);
      }
    }
  }

  private loadContent(): void {
    if (this.activeGenre) {
      this.dataService.getVideoContent(this.category_id, this.activeGenre.id, this.loadPart,
        this.skip, this.selectedCountry, this.selectedRatingFilter).then(result => {
        this.totalCount = result.total;
        this.film_ids.push(...result.videos.map(item => {
          if (item.tvshow_id) {
            return item.tvshow_id;
          } return item.video_id;
        }));
        this.loadingService.stopLoading();
        this.skip += this.loadPart;
      });
    }
  }

  public get isHaveCountries(): boolean {
    return this.countries.length > 0;
  }

  public onCountryFilterChange(country: Country): void {
    this.skip = 0;
    this.selectedCountry = country;
    this.film_ids = [];
    this.loadContent();
  }

  public onRatingFilterChange(rating: RatingFilter): void {
    this.skip = 0;
    this.selectedRatingFilter = rating;
    this.film_ids = [];
    this.loadContent();
  }

  public onDateFilterChange(period: RatingFilter): void {
    this.skip = 0;
    this.selectedRatingFilter = period;
    this.film_ids = [];
    this.loadContent();
  }

  private loadCountryList(): void {
    this.loadingService.startLoading();
    this.dataService.getCountryList(this.category_id).then(res => {
      this.loadingService.stopLoading();
      this.selectedCountry = res[0];
      this.countries = res;
    });
  }

  public onScroll() {
    if ((this.totalCount - this.skip) > 0) {
      this.loadContent();
    }
  }
}
