import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { NgSelectModule } from '@ng-select/ng-select';
import { GenreFilterModule } from './../genre-filter/genre-filter.module';
import { FilmsComponent } from './films.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { FilmCardModule } from '../../../components/content-slider/film-card/film-card.module';

@NgModule({
  declarations: [
    FilmsComponent
  ],
  imports: [
    CommonModule,
    GenreFilterModule,
    NgSelectModule,
    FormsModule,
    FilmCardModule,
    InfiniteScrollModule,
    RouterModule.forChild([
      {
        path: '',
        component: FilmsComponent
      }
    ])
  ]
})

export class FilmsPageModule { }
