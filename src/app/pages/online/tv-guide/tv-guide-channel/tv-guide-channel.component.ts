import { Component, Input, EventEmitter, Output, OnInit, OnDestroy } from '@angular/core';
import { Channel } from '../../../../models';
import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-tv-guide-channel',
  templateUrl: './tv-guide-channel.component.html',
  styleUrls: ['./tv-guide-channel.component.scss']
})

export class TvGuideChannelComponent implements OnInit, OnDestroy {

  static activeChannelId: number;

  public isParentControlNeeded = false;
  private checkPinEvent: Subject<boolean> = new Subject();
  private checkPinSubscriber: Subscription;

  @Input() channel: Channel;
  @Output() changeChannel: EventEmitter<number> = new EventEmitter();

  constructor() { }

  ngOnInit() {

  }

  public onAdultCheckerComplete(result: boolean): void {
    if (result) {
      this.checkPinEvent.next(true);
    }
    this.isParentControlNeeded = false;
  }

  public setActiveChannel(): void {
    if (!this.channel.age_rating.includes('18')) {
      this.isParentControlNeeded = false;
      if (this.checkPinSubscriber) {
        this.checkPinSubscriber.unsubscribe();
      }
      TvGuideChannelComponent.activeChannelId = this.channel.channel_id;
      this.changeChannel.emit(this.channel.channel_id);
    } else {
      this.isParentControlNeeded = true;
      this.checkPinSubscriber = this.checkPinEvent.subscribe(() => {
        TvGuideChannelComponent.activeChannelId = this.channel.channel_id;
        this.changeChannel.emit(this.channel.channel_id);
      });
    }
    // TvGuideChannelComponent.activeChannelId = this.channel.channel_id;
    // this.changeChannel.emit(this.channel.channel_id);
  }

  public get isActive(): boolean {
    return TvGuideChannelComponent.activeChannelId === this.channel.channel_id;
  }

  ngOnDestroy() {

  }
}
