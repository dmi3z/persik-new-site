import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { CheckWidthService } from '../../../../services';

@Component({
  selector: 'app-tv-guide-day',
  templateUrl: './tv-guide-day.component.html',
  styleUrls: ['./tv-guide-day.component.scss']
})

export class TvGuideDayComponent implements OnInit {

  static activeDayId: number;

  public dateFirstPart: string;
  public dateSecondPart: string;
  public isMobile: boolean = false;
  private windowWidth: number;

  @Input() day: TvshowDay;
  @Output() changeDay: EventEmitter<TvshowDay> = new EventEmitter();

  constructor(private checkWindowService: CheckWidthService) { }

  ngOnInit() {

    this.dateFirstPart = this.day.dateString.split('-')[0];
    this.dateSecondPart = this.day.dateString.split('-')[1];

    this.windowWidth = this.checkWindowService.getWidth();
    if (this.windowWidth <= 992) {
      this.isMobile = true;
    }
  }


  public setActiveDay(): void {
    TvGuideDayComponent.activeDayId = this.day.id;
    this.changeDay.emit(this.day);
  }

  public get isActive(): boolean {
    return TvGuideDayComponent.activeDayId === this.day.id;
  }

  public get isToday(): boolean {
    return this.day.isToday;
  }

}

interface TvshowDay {
  id: number;
  date: string;
  dateString: string;
  dayMonth: string;
  day: string;
  isToday: boolean;
}

