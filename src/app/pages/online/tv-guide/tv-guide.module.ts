import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TvGuideComponent } from './tv-guide.component';
import { RouterModule } from '@angular/router';
import { MalihuScrollbarModule } from 'ngx-malihu-scrollbar';
import { PlayerModule } from '../../../components/player/player.module';
import { PipesModule } from '../../../pipes/pipes.module';
import { AdultControlModule } from '../../../components/adult-control/adult-control.module';
import { SlickModule } from 'ngx-slick';

import {
  TvGuideChannelComponent,
  TvGuideDayComponent,
  TvGuideTvshowComponent,
} from './';

@NgModule({
  declarations: [
    TvGuideChannelComponent,
    TvGuideDayComponent,
    TvGuideTvshowComponent,
    TvGuideComponent,
  ],
  imports: [
    SlickModule.forRoot(),
    AdultControlModule,
    CommonModule,
    PlayerModule,
    PipesModule,
    RouterModule.forChild([
      {
        path: '',
        component: TvGuideComponent
      }
    ]),
    MalihuScrollbarModule.forRoot(),
  ]
})

export class TvGuideModule {}
