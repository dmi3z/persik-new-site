import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Tvshow } from '../../../../models';
import { TimeService, TranslitService } from '../../../../services';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-guide-tvshow',
  templateUrl: './tv-guide-tvshow.component.html',
  styleUrls: ['./tv-guide-tvshow.component.scss']
})

export class TvGuideTvshowComponent implements OnInit, OnDestroy {

  static archiveDeep: number;
  @Input() tvshow: Tvshow;
  public progress: number;

  private updateTimer: Subscription;
  private timerSubscriber: Subscription;


  constructor(private timeService: TimeService, private router: Router, private transliteService: TranslitService, ) { }

  ngOnInit(): void {
    this.updateTimer = this.timeService.timeControllerFast.subscribe(() => {
      this.calculateProgress();
    });
  }

  public setActiveTvShow(): void {
    if (!this.isCurrent) {
      this.router.navigate(['/details', this.contentName]);
    } else {
      this.router.navigate(['video-player', this.contentName]);
    }
  }

  private get contentName(): string {
    if (this.tvshow) {
      if (this.tvshow.title && this.tvshow.title.length > 0) {
        return this.transliteService.convertCyrillToLatin(this.tvshow.title, this.tvshow.tvshow_id.toString());
      } else {
        return this.tvshow.tvshow_id.toString();
      }
    }
  }

  public get isCurrent(): boolean {
    const currentTime: number = this.timeService.currentTime;
    return this.tvshow.start < currentTime && this.tvshow.stop >= currentTime;
  }

  public get isArchive(): boolean {
    const currentTime: number = this.timeService.currentTime;
    return !this.isCurrent &&
      ((currentTime - TvGuideTvshowComponent.archiveDeep) < this.tvshow.start) &&
      this.tvshow.stop < currentTime;
  }

  private calculateProgress(): void {
    if (this.isCurrent) {
      const currentTime = this.timeService.currentTime;
      this.progress = ((currentTime - this.tvshow.start) / (this.tvshow.stop - this.tvshow.start)) * 100;
    }
  }

  ngOnDestroy() {
    if (this.updateTimer) {
      this.updateTimer.unsubscribe();
    }
    if (this.timerSubscriber) {
      this.timerSubscriber.unsubscribe();
    }
  }

}
