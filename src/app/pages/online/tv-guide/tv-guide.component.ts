import { PlayerComponent } from './../../../components/player/player.component';
import { Tvshow, Video, Channel, ContentName, Genre } from './../../../models';
import { DataService, TimeService, TranslitService, CheckWidthService, MetaService } from './../../../services';
import { Component, OnInit, ViewChild, Inject, PLATFORM_ID } from '@angular/core';
import * as moment from 'moment';
import { TvGuideChannelComponent, TvGuideDayComponent, TvGuideTvshowComponent } from './';
import { isPlatformBrowser } from '@angular/common';
import { Router } from '@angular/router';


@Component({
  selector: 'app-tv-guide',
  templateUrl: './tv-guide.component.html',
  styleUrls: ['./tv-guide.component.scss']
})

export class TvGuideComponent implements OnInit {

  public scrollbarOptions = { axis: 'y', theme: 'minimal-dark' };
  public scrollbarMobileOptions = { axis: 'x', theme: 'minimal-dark' };

  private windowWidth: number;

  public channels: Channel[] = [];
  public genres: Genre[];
  public activeGenre: Genre;
  public tvshowDays: TvshowDay[] = [];
  private tvshows: Tvshow[] = [];
  public visibleTvshows: Tvshow[] = [];
  public currentVideo: Video;
  public currentTvshow: Tvshow;
  @ViewChild('player') player: PlayerComponent;
  public isBrowser: boolean;
  public isMobile: boolean;
  public activeChannel: Channel;
  private allChannels: Channel[] = [];
  public videoDescription: string;
  public isVideoDescriptionSliced: boolean;
  public isVideoDescriptionOpen: boolean;
  public isFilterOpen: boolean;

  public viewChannelInfo = false;

  public slideConfig: any = {
    'slidesToShow': 3,
    'slidesToScroll': 1,
    'arrows': false,
    'dots': false,
    'fade': false,
    'centerMode': true,
    'autoplay': false,
    'infinite': false,
  };

  constructor(
    private dataService: DataService,
    private timeService: TimeService,
    private router: Router,
    private translitService: TranslitService,
    private checkWindowService: CheckWidthService,
    @Inject(PLATFORM_ID) private platform: any,
    private metaService: MetaService
  ) {
    moment.locale('ru');
  }

  ngOnInit(): void {
    this.metaService.createCanonicalURL();
    if (isPlatformBrowser(this.platform)) {
      this.windowWidth = this.checkWindowService.getWidth();
      if (this.windowWidth <= 992) {
        this.isMobile = true;
      }
      this.isBrowser = true;
      this.loadChannels();
    }
    this.dataService.loadChannelCategories().then(categories => {
      this.genres = categories;
      this.activeGenre = this.genres[0];
    });
  }

  private scrollMobileDates(): void {
    try {
      setTimeout(() => {
        const dateCurrent = document.querySelector('.dateCurrent');
        if (dateCurrent && dateCurrent.parentElement) {
          dateCurrent.parentElement.scrollIntoView({ block: 'start', inline: 'center', behavior: 'smooth' });
        }
      }, 1000);
    } catch (e) { }
  }

  public closeFilter(): void {
    this.isFilterOpen = false;
  }

  public toggleFilter(): void {
    this.isFilterOpen = !this.isFilterOpen;
  }

  public scrollToCurrentDate(): void {
    setTimeout(() => {
      const currentDateBlock = $('.dateCurrent');
      $('.tv-guide__dates').mCustomScrollbar('scrollTo', currentDateBlock);
    }, 1000);
  }

  public scrollToCurrentTvshow(): void {
    setTimeout(() => {
      const currentDateBlock = $('.tvshowCurrent');
      $('.tv-guide__list').mCustomScrollbar('scrollTo', currentDateBlock);
    }, 1000);
  }

  public onGenreChange(genre: Genre, toggleMobileView?: boolean): void {
    if (genre.name === 'Все жанры') {
      this.activeGenre.id = 1;
    }
    this.activeGenre = genre;
    if (toggleMobileView) {
      this.viewChannelInfo = false;
    }
    this.filterChannels();
    this.isFilterOpen = false;
    $('.tv-guide__channels-scroll').mCustomScrollbar('scrollTo', '0');
  }

  private scrollToCurrentMobileTvShow(): void {
    setTimeout(() => {
      const currentDateBlock = $('.tvshowCurrent');
      $('.tvshows').mCustomScrollbar('scrollTo', currentDateBlock);
    }, 1000);
  }

  private filterChannels(): void {
    if (!this.activeGenre || this.activeGenre.id === 1) {
      this.channels = this.allChannels;
    } else {
      this.channels = this.allChannels.filter(channel => channel.genres.includes(this.activeGenre.id));
    }
  }

  public onChannelChange(activeChannel): void {
    this.activeChannel = activeChannel;
    this.loadTvshows();
    this.viewChannelInfo = true;
  }

  public prevChannel(): void {
    const index = this.channels.indexOf(this.activeChannel);
    if (index >= 1) {
      this.activeChannel = this.channels[index - 1];
      TvGuideChannelComponent.activeChannelId = this.channels[index - 1].channel_id;
      this.loadTvshows();
      this.scrollToCurrentMobileTvShow();
    }
  }

  public nextChannel(): void {
    const index = this.channels.indexOf(this.activeChannel);
    if (index < this.channels.length - 1) {
      this.activeChannel = this.channels[index + 1];
      TvGuideChannelComponent.activeChannelId = this.channels[index + 1].channel_id;
      this.loadTvshows();
      this.scrollToCurrentMobileTvShow();
    }
  }

  public onDayChange(): void {
    this.loadTvshowsForDay();
    this.scrollMobileDates();
  }

  public goToPlayer(): void {
    const cpu: string = this.translitService.convertCyrillToLatin(this.currentTvshow.title);
    this.router.navigate(['channel-player', `${cpu}-${this.currentTvshow.channel_id}`]);
  }

  private loadChannels(): void {
    this.dataService.getChannels().then(channels => {
      if (channels.length > 0) {
        TvGuideChannelComponent.activeChannelId = channels[0].channel_id;
        this.channels = channels;
        this.allChannels = channels;
        this.loadTvshows();
      }
    });
  }

  private sliceVideoDescription(description: string): void {
    this.videoDescription = description;
    this.isVideoDescriptionOpen = true;
    if (description.length > 120) {
      this.videoDescription = description.slice(0, 120) + '...';
      this.isVideoDescriptionSliced = true;
      this.isVideoDescriptionOpen = false;
    }
  }

  public toggleVideoDescription(): void {
    if (this.isVideoDescriptionOpen) {
      this.isVideoDescriptionOpen = false;
      this.sliceVideoDescription(this.videoDescription);
    } else {
      this.isVideoDescriptionOpen = true;
      this.videoDescription = this.currentVideo.description;
    }
  }

  private loadCurrentVideoInfo(): void {
    const currentTime: number = this.timeService.currentTime;
    this.currentTvshow = this.tvshows.find(tvshow => {
      return (tvshow.start < currentTime && tvshow.stop >= currentTime);
    });
    if (this.currentTvshow) {
      this.dataService.getVideoInfo(+this.currentTvshow.video_id).then(res => {
        this.currentVideo = res;
        this.sliceVideoDescription(this.currentVideo.description);
      });
    }
  }

  private loadTvshows(): void {
    this.dataService.getTvshows(TvGuideChannelComponent.activeChannelId).then(tvshows => {
      TvGuideTvshowComponent.archiveDeep = this.channels.find(channel => channel.channel_id ===
        TvGuideChannelComponent.activeChannelId).dvr_sec;
      this.tvshows = tvshows;
      this.tvshowDays = this.getTvhowDays();
      this.setCurrentDayActive();
      this.loadCurrentVideoInfo();
      this.playChannel();
      this.scrollToCurrentDate();
      this.scrollToCurrentTvshow();
    });
  }

  private playChannel(): void {
    this.player.play(TvGuideChannelComponent.activeChannelId, ContentName.CHANNEL);
  }

  private getTvhowDays(): TvshowDay[] { // Получение массива дат для канала
    const uniqDates: TvshowDay[] = [];
    this.tvshows.forEach((tvshow) => {
      const day: TvshowDay = {
        id: 0,
        date: tvshow.date,
        dateString: moment(tvshow.date, 'YYYY-MM-DD').format('D MMM').replace('.', '') + ' - ' +
          moment(tvshow.date, 'YYYY-MM-DD').format('dddd'),
        dayMonth: moment(tvshow.date, 'YYYY-MM-DD').format('D MMMM'),
        day: moment(tvshow.date, 'YYYY-MM-DD').format('ddd'),
        isToday: moment(tvshow.date, 'YYYY-MM-DD').isSame(Date.now(), 'day')
      };
      if (!uniqDates.map(item => item.date).includes(day.date)) {
        day.id = uniqDates.length;
        uniqDates.push(day);
      }
    });
    return uniqDates;
  }

  private setCurrentDayActive() {
    const currentTime: number = this.timeService.currentTime;
    const currentDate: string = moment.unix(currentTime).format('YYYY-MM-DD');
    TvGuideDayComponent.activeDayId = this.tvshowDays.map(day => day.date).indexOf(currentDate);
    this.loadTvshowsForDay();
    this.scrollMobileDates();
  }

  private loadTvshowsForDay(): void {
    const activeDate: string = this.tvshowDays[TvGuideDayComponent.activeDayId] ?
      this.tvshowDays[TvGuideDayComponent.activeDayId].date : null;
    if (activeDate) {
      this.visibleTvshows = this.tvshows.filter(item => item.date === activeDate);
    }
  }

}

interface TvshowDay {
  id: number;
  date: string;
  dateString: string;
  dayMonth: string;
  day: string;
  isToday: boolean;
}
