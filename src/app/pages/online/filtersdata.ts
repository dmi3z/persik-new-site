import { RatingFilter } from '../../models';

export const ratingFilters: RatingFilter[] = [
    {
      id: 0,
      name: 'По рейтингу IMDB',
      value: 'rating_imdb'
    },
    {
      id: 1,
      name: 'По рейтингу Кинопоиск',
      value: 'rating_kinopoisk'
    }
  ];

  export const dateFilters: RatingFilter[] = [
    {
      id: 0,
      name: 'По дате добавления',
      value: 'last'
    },
    {
      id: 1,
      name: 'По году',
      value: 'year'
    },
    {
      id: 2,
      name: 'Все годы',
      value: ' '
    },
    {
      id: 3,
      name: '2018 год',
      value: '2018 2018'
    },
    {
      id: 4,
      name: '2017 год',
      value: '2017 2017'
    },
    {
      id: 5,
      name: '2016 год',
      value: '2016 2016'
    },
    {
      id: 6,
      name: '2015 год',
      value: '2015 2015'
    },
    {
      id: 7,
      name: '2014 год',
      value: '2014 2014'
    },
    {
      id: 8,
      name: '2013 год',
      value: '2013 2013'
    },
    {
      id: 9,
      name: '2010 - 2013',
      value: '2010 2013'
    },
    {
      id: 10,
      name: '2000 - 2010',
      value: '2000 2010'
    },
    {
      id: 11,
      name: '1990 - 2000',
      value: '1990 2000'
    },
    {
      id: 12,
      name: '1980 - 1990',
      value: '1980 1990'
    },
    {
      id: 13,
      name: 'до 1980',
      value: '1900 1980'
    }
  ];

  /*export const yearFilters: RatingFilter[] = [
    {
      id: 0,
      name: 'Все годы',
      value: 'null'
    },
    {
      id: 1,
      name: '2018 год',
      value: '2018 2018'
    },
    {
      id: 2,
      name: '2017 год',
      value: '2017 2017'
    },
    {
      id: 3,
      name: '2016 год',
      value: '2016 2016'
    },
    {
      id: 4,
      name: '2015 год',
      value: '2015 2015'
    },
    {
      id: 5,
      name: '2014 год',
      value: '2014 2014'
    },
    {
      id: 6,
      name: '2013 год',
      value: '2013 2013'
    },
    {
      id: 7,
      name: '2010 - 2013',
      value: '2010 2013'
    },
    {
      id: 8,
      name: '2000 - 2010',
      value: '2000 2010'
    },
    {
      id: 9,
      name: '1990 - 2000',
      value: '1990 2000'
    },
    {
      id: 10,
      name: '1980 - 1990',
      value: '1980 1990'
    },
    {
      id: 11,
      name: 'до 1980',
      value: '1980 null'
    }
  ];*/
