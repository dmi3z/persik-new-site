import { Component, OnInit, Input, PLATFORM_ID, Inject } from '@angular/core';
import { Teacher } from '../../../../models';
import { isPlatformBrowser } from '@angular/common';

@Component({
  selector: 'course-page-teacher',
  templateUrl: './course-page-teacher.component.html',
  styleUrls: ['./course-page-teacher.component.scss']
})

export class CoursePageTeacherComponent implements OnInit {

    constructor(@Inject(PLATFORM_ID) private platform,) {}

    public isBrowser: boolean = false;

    @Input() teacher: Teacher;

    ngOnInit() {
      if (isPlatformBrowser(this.platform)) {
        this.isBrowser = true;
      }
    };
}

