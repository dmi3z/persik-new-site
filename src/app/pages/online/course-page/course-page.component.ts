import { FavoriteService } from './../../../services/favorite.service';
import { FavoriteSettings } from './../../../models/favorite';
import { Episode, ContentName } from './../../../models/vod';
import { AuthService } from './../../../services/auth.service';
import { sliderOptions } from './slider-options';
import { staticCourseData } from './harddata';
import { DataService } from './../../../services/data.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, PLATFORM_ID, Inject, OnDestroy } from '@angular/core';
import { Course } from '../../../models';
import { FormControl, FormGroup } from '@angular/forms';
import { isPlatformBrowser } from '@angular/common';
import { getSkills, getRequirements, getTeachers, getPreview } from './skills';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-course-page',
  templateUrl: './course-page.component.html',
  styleUrls: ['./course-page.component.scss']
})

export class CoursePageComponent implements OnInit, OnDestroy {

  public isBrowser: boolean;

  public name: FormControl;
  public subject: FormControl;
  public email: FormControl;
  public phone: FormControl;
  public message: FormControl;
  public feedbackForm: FormGroup;

  private firstLessonId: number;

  public slideConfig = sliderOptions;
  public course: Course = staticCourseData;
  public isSeries: boolean;
  public anotherCourseIds: number[] = [];

  private activatedRouteSubscriber: Subscription;

  constructor(
    @Inject(PLATFORM_ID) private platform,
    private activatedRoute: ActivatedRoute,
    private dataService: DataService,
    private authService: AuthService,
    private router: Router,
    private favoriteService: FavoriteService
  ) { }

  ngOnInit() {
    if (isPlatformBrowser(this.platform)) {
      this.isBrowser = true;
    }
    this.activatedRouteSubscriber = this.activatedRoute.params.subscribe(param => {
      this.anotherCourseIds = [];
      this.course.id = Number(param.id);
      this.course.skills = getSkills(this.course.id);
      this.course.requirements = getRequirements(this.course.id);
      this.course.teachers = getTeachers(this.course.id);
      this.anotherCourseIds = this.course.anotherCourses.filter(ac => ac !== this.course.id);
      this.dataService.getVideoInfo(this.course.id).then(data => {
        this.dataService.loadVodCategories().then(res => {
          const courseGenres = res.find(c => c.id === 6).genres;
          this.course.type = courseGenres.find(genre => data.genres.includes(genre.id)).name;
        });
        this.course.cover = data.cover;
        this.course.name = data.name;
        this.course.desc = data.description;
        this.firstLessonId = data.episodes[0].video_id;
        this.authService.getTariffs().then(tariffs => {
          const product = tariffs.products.find(prod => prod.product_id === data.in_products[0].product_id);
          const option = product.options[0];
          this.course.price = option.price;
        });
        if (data.is_series) {
          this.course.seasons = [];
          data.episodes.sort((a, b) => a.episode.localeCompare(b.episode));
          const uniqueSeasons = this.getUniqueSeasons(data.episodes);
          uniqueSeasons.forEach((seasonName, index) => {
            const episodeIds = data.episodes.filter(episode => episode.season === seasonName)
              .map(ep => String(ep.video_id));
            this.course.seasons[index] = {
              title: seasonName,
              counterText: 'Уроки',
              lessons: []
            };
            this.dataService.getVideosInfo(episodeIds).then(res => {
              this.course.seasons[index].lessons = res;
            });
            this.isSeries = data.is_series;
          });
        }
      });
    });

    // this.createFormFields();
    // this.createForm();
  }

  public get preview(): string {
    return getPreview(this.course.id);
  }

  public get isHaveAnother(): boolean {
    return this.anotherCourseIds.length > 0;
  }

  public getUniqueSeasons(episodes: Episode[]): string[] {
    const seasonsName = episodes.map(ep => {
      return { season: ep.season, lesson: ep.episode };
    }).sort((a, b) => {
      return a.lesson.localeCompare(b.lesson);
    });
    const uniqueSeasons = seasonsName.map(sn => sn.season).filter((item, index, self) => self.indexOf(item) === index);
    return uniqueSeasons;
  }

  public showCourse(): void {
    if (this.isSeries) {
      this.router.navigate(['video-player', this.firstLessonId]);
    } else {
      this.router.navigate(['video-player', this.course.id]);
    }
  }

  public get isFavorite(): boolean {
    if (this.course && this.course.id) {
      return this.favoriteService.isVideoFavorite(this.course.id);
    }
  }

  public get isLogin(): boolean {
    return this.authService.isLogin;
  }

  public addToFavorite(): void {
    this.favoriteService.addToFavorite(new FavoriteSettings(this.course.id, ContentName.VIDEO));
  }

  public removeFromFavorite(): void {
    this.favoriteService.removeFromFavorite(new FavoriteSettings(this.course.id, ContentName.VIDEO));
  }


  // private createFormFields(): void {
  //   this.name = new FormControl('', [Validators.required]);
  //   this.email = new FormControl('', [Validators.required, emailCorrectValidator]);
  //   this.message = new FormControl('', [Validators.required]);
  // }

  // private createForm(): void {
  //   this.feedbackForm = new FormGroup({
  //     name: this.name,
  //     email: this.email,
  //     message: this.message
  //   });
  // }

  ngOnDestroy() {
    if (this.activatedRouteSubscriber) {
      this.activatedRouteSubscriber.unsubscribe();
    }
  }
}


