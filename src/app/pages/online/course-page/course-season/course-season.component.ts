import { Router } from '@angular/router';
import { Video } from './../../../../models/vod';
import { Component, Input } from '@angular/core';
import { Season } from '../../../../models';
import { getMaterial } from '../skills';

@Component({
  selector: 'app-course-season',
  templateUrl: './course-season.component.html',
  styleUrls: ['./course-season.component.scss']
})

export class CourseSeasonComponent {

  constructor(private router: Router) { }

  @Input() season: Season;
  @Input() courseId: number;
  public isOpen: boolean;


  public toggleSeason(): void {
    this.isOpen = !this.isOpen;
  }

  public showLesson(lesson: Video): void {
    this.router.navigate(['video-player', lesson.video_id]);
  }

  public getMaterial(lessonName: string): string {
    return getMaterial(this.courseId, lessonName);
  }

}
