import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CoursePageComponent } from './course-page.component';
import { SlickModule } from 'ngx-slick';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {
  CourseSeasonComponent,
  CoursePageTeacherComponent,
  CoursePageAnotherComponent,
  CoursePageCommentComponent
} from './';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    CourseSeasonComponent,
    CoursePageComponent,
    CoursePageTeacherComponent,
    CoursePageAnotherComponent,
    CoursePageCommentComponent
  ],
  imports: [
    CommonModule,
    SlickModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forChild([
      {
        path: '',
        component: CoursePageComponent
      }
    ])
  ]
})

export class CoursePageModule { }
