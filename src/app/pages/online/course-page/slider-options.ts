export const sliderOptions: any = {
  'slidesToShow': 2,
  'slidesToScroll': 1,
  'arrows': true,
  'dots': false,
  'fade': false,
  'centerMode': false,
  'autoplay': false,
  'infinite': false,
  'responsive': [
    {
      'breakpoint': 1600,
      'settings': {
        'dots': false,
        'slidesToShow': 2
      }
    },
    {
      'breakpoint': 1400,
      'settings': {
        'dots': false,
        'slidesToShow': 2
      }
    },
    {
      'breakpoint': 1200,
      'settings': {
        'dots': false,
        'slidesToShow': 2
      }
    },
    {
      'breakpoint': 992,
      'settings': {
        'dots': false,
        'slidesToShow': 1
      }
    },
    {
      'breakpoint': 767,
      'settings': {
        'dots': false,
        'slidesToShow': 1,
        'arrows': false,
        'infinity': false
      }
    },
  ]
};
