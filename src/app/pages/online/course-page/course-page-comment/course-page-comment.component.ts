import { Component, OnInit, Input } from '@angular/core';
import { Comment } from '../../../../models';

@Component({
  selector: 'course-page-comment',
  templateUrl: './course-page-comment.component.html',
  styleUrls: ['./course-page-comment.component.scss']
})

export class CoursePageCommentComponent implements OnInit {

    constructor() {}

    @Input() comment: Comment;

    ngOnInit() {
    };
}

