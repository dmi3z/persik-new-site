import { Router } from '@angular/router';
import { DataService } from './../../../../services/data.service';
import { Component, OnInit, Input } from '@angular/core';
import { CoursePreview } from '../../../../models';

@Component({
  selector: 'course-page-another',
  templateUrl: './course-page-another.component.html',
  styleUrls: ['./course-page-another.component.scss']
})

export class CoursePageAnotherComponent implements OnInit {

  constructor(private dataService: DataService, private router: Router) { }

  @Input() courseId: number;
  public course: CoursePreview;

  ngOnInit() {
    this.dataService.getVideoInfo(this.courseId).then(res => {
      this.dataService.loadVodCategories().then(cat => {
        const courseCategories = cat.find(c => c.id === 6);
        this.course = {
          id: this.courseId,
          name: res.name,
          photo: res.cover ? res.cover : '',
          type: courseCategories.genres.find(genre => genre.id === res.genres[0]).name,
          lessonsCount: res.episodes.length
        };
      });
    });
  }

  public viewCourse(): void {
    this.router.navigate(['/course-page', this.courseId]);
  }
}

