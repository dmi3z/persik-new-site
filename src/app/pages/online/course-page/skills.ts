import { Skill, Teacher } from './../../../models/course';

const skills = {
  1047632: [  // Photoshop
    {
      title: 'Вы сможете свободно рисовать в программе Photoshop CC 2017',
      icon: 'assets/images/course-page/skill-1.png'
    },
    {
      title: 'Научитесь выбирать правильные инструменты и команды для рисования интерфейсов',
      icon: 'assets/images/course-page/skill-3.png'
    },
    {
      title: 'Научитесь применять разные функции программы для ускорения работы',
      icon: 'assets/images/course-page/skill-2.png'
    },
    {
      title: 'Узнаете какова роль веб-дизайнера при создании веб-сайта',
      icon: 'assets/images/course-page/skill-5.png'
    },
    {
      title: 'А если завести блокнот заметок в течении курса, в конце у Вас будет готовый настольный справочник по Фотошопу',
      icon: 'assets/images/course-page/skill-4.png'
    },
  ],
  966238: [
    {
      title: 'Вы научитесь разрабатывать XML разметку и UI андроид приложений',
      icon: 'assets/images/course-page/skill-1.png'
    },
    {
      title: 'Вы научитесь работать с аудио, видео и изображениями',
      icon: 'assets/images/course-page/skill-1.png'
    },
    {
      title: 'Вы научитесь сохранять различные виды данных разными способами',
      icon: 'assets/images/course-page/skill-1.png'
    },
    {
      title: 'Вы научитесь использовать библиотеки Volley, Glide, Picasso',
      icon: 'assets/images/course-page/skill-1.png'
    },
    {
      title: 'Вы научитесь использовать Google Location API',
      icon: 'assets/images/course-page/skill-1.png'
    },
    {
      title: 'Вы научитесь создавать приложение заказа такси при помощи Firebase - клон Uber',
      icon: 'assets/images/course-page/skill-1.png'
    },
    {
      title: 'Сможете начать карьеру android разработчика на фрилансе или в IT компании',
      icon: 'assets/images/course-page/skill-1.png'
    },
    {
      title: 'Вы изучите основы Java, и также более продвинутые темы, включая ООП',
      icon: 'assets/images/course-page/skill-1.png'
    },
    {
      title: 'Вы изучите такие элементы Material Design, как RecyclerView, CardView и другие',
      icon: 'assets/images/course-page/skill-1.png'
    },
    {
      title: 'Вы научитесь получать данные из интернета',
      icon: 'assets/images/course-page/skill-1.png'
    },
    {
      title: 'Вы научитесь создавать приложения-мессенджеры при помощи Firebase - такие как Viber, WhatsApp, Telegram',
      icon: 'assets/images/course-page/skill-1.png'
    },
    {
      title: 'Вы научитесь использовать Google Maps API',
      icon: 'assets/images/course-page/skill-1.png'
    },
    {
      title: 'Вы научитесь создавать практически любое андроид приложение, включая игры',
      icon: 'assets/images/course-page/skill-1.png'
    },
    {
      title: 'Сможете опубликовать ваше приложение в Google Play и сможете зарабатывать на рекламе или платных функциях',
      icon: 'assets/images/course-page/skill-1.png'
    }
  ], // Android
  1606986: [
    {
      title: 'Писать простые программы на Python 3',
      icon: 'assets/images/course-page/skill-1.png'
    },
    {
      title: 'Писать простые игры типа крестиков-ноликов',
      icon: 'assets/images/course-page/skill-1.png'
    },
    {
      title: 'Логика с условиями и циклами',
      icon: 'assets/images/course-page/skill-1.png'
    },
    {
      title: 'Объектно-ориентированное программирование на Python',
      icon: 'assets/images/course-page/skill-1.png'
    },
    {
      title: 'Использование Jupyter Notebook',
      icon: 'assets/images/course-page/skill-1.png'
    },
    {
      title: 'Использование коллекций в Python: списки, словари и так далее',
      icon: 'assets/images/course-page/skill-1.png'
    },
    {
      title: 'Декораторы',
      icon: 'assets/images/course-page/skill-1.png'
    },
    {
      title: 'Неизменяемые объекты',
      icon: 'assets/images/course-page/skill-1.png'
    },
    {
      title: 'Лучшие практики по написанию "чистого" кода на Python',
      icon: 'assets/images/course-page/skill-1.png'
    }
  ],
  1041757: [
    {
      title: 'Строить английские предложения, выражая свои мысли корректно',
      icon: 'assets/images/course-page/skill-1.png'
    },
    {
      title: 'Запоминать от 100 слов в день, что значительно ускорит ваш темп обучения и очень увеличит Ваш словарный запас',
      icon: 'assets/images/course-page/skill-1.png'
    },
    {
      title: 'Понимать фильмы и аудиокниги на оригинальном языке',
      icon: 'assets/images/course-page/skill-1.png'
    },
    {
      title: 'Общаться с людьми на абсолютно любые темы',
      icon: 'assets/images/course-page/skill-1.png'
    }
  ]
};

const requirements = {
  1047632: ['Наличие Photoshop CC2017', 'Базовые знания программы не требуются'],
  966238: ['Наличие желания, компьютера и интернета'],
  1606986: ['Современный компьютер с доступом к Интернет'],
  1041757: ['Максимально настроится на обучение', 'Не делать перерывов', 'Не учить больше одного урока в день']
};

const teachers = {
  1047632: [
    {
      agency: 'Моя цель - предоставить вам возможность получить новые знания и быть уверенной, что Вы потратили время не зря!',
      agencyImg: 'assets/images/course-page/agency.png',
      photo: 'assets/images/course-page/narine.jpg',
      name: 'Нарине Мирзаян',
      exp: 'Профессиональный дизайнер Веб-сайтов и Мобильных приложений'
    }
  ],
  966238: [
    {
      agency: 'Профессия разработчика открыла для меня большие возможности, и я буду рад помочь вам стать разработчиком программного обеспечения.',
      agencyImg: 'assets/images/course-page/agency.png',
      photo: 'assets/images/course-page/allakherdov.jpg',
      name: 'YouRa Allakhverdov',
      exp: 'Mobile & Web developer'
    }
  ],
  1606986: [
    {
      agency: 'Самое главное, что характеризует мои курсы - выжимка самого необходимого. Краткость - сестра таланта и я верю в то, что в современную эпоху информационной перегруженности - необходимо сосредотачиваться на самом главном',
      agencyImg: 'assets/images/course-page/agency.png',
      photo: 'assets/images/course-page/iliya_fofanov.jpg',
      name: 'Илья Фофанов',
      exp: 'Инженер-программист, организитор митапов MskDotNet'
    }
  ],
  1041757: [
    {
      agency: 'Работа в сфере образования – более 2 лет. Кол-во учеников – более 200 000. Процент довольных клиентов – 98,5%',
      agencyImg: 'assets/images/course-page/agency.png',
      photo: 'assets/images/course-page/genius_english.jpg',
      name: 'Genius English',
      exp: ''
    }
  ]
};

const preview = {
  1041757: 'https://persik.by/learning_materials/Genius English. Английский за 15 часов для путешественников/1041757.mp4',
  1047632: 'https://persik.by/learning_materials/Нарине Мирзаян. Фотошоп для начинающих Веб-дизайнеров. Трейлер/1047632.mp4',
  1606986: 'https://persik.by/learning_materials/Илья Фофанов. Полное руководство по Python 3 от новичка до специалиста/1606986.mp4', // Phyton
  966238: 'https://persik.by/learning_materials/YouRa Allakhverdov. Андроид разработка с нуля до профессионала/966238.mp4' // Android
};

const materials = {
  1047632: [
    'https://persik.by/learning_materials/Нарине Мирзаян. Фотошоп для начинающих Веб-дизайнеров. Материалы/Урок 9. Панель инструментов Adobe Photoshop, группы инструментов Выделение.zip',
    'https://persik.by/learning_materials/Нарине Мирзаян. Фотошоп для начинающих Веб-дизайнеров. Материалы/Урок 11. Группа инструментов Раскрашивание.zip',
    'https://persik.by/learning_materials/Нарине Мирзаян. Фотошоп для начинающих Веб-дизайнеров. Материалы/Урок 10. Группа инструментов Кадрирование, Измерение.zip',
    'https://persik.by/learning_materials/Нарине Мирзаян. Фотошоп для начинающих Веб-дизайнеров. Материалы/Урок 12. Группа инструментов Ретуширование.zip',
    'https://persik.by/learning_materials/Нарине Мирзаян. Фотошоп для начинающих Веб-дизайнеров. Материалы/Урок 22. Примеры наложения слоев.zip',
    'https://persik.by/learning_materials/Нарине Мирзаян. Фотошоп для начинающих Веб-дизайнеров. Материалы/Урок 24. Работа со Смарт-объектами в Фотошопе. Часть 2.zip',
    'https://persik.by/learning_materials/Нарине Мирзаян. Фотошоп для начинающих Веб-дизайнеров. Материалы/Урок 25. Слои-маски.zip',
    'https://persik.by/learning_materials/Нарине Мирзаян. Фотошоп для начинающих Веб-дизайнеров. Материалы/Урок 26. Слои-маски. Упражнение.zip',
    'https://persik.by/learning_materials/Нарине Мирзаян. Фотошоп для начинающих Веб-дизайнеров. Материалы/Урок 28. Быстрые маски. Упражнение.zip',
    'https://persik.by/learning_materials/Нарине Мирзаян. Фотошоп для начинающих Веб-дизайнеров. Материалы/Урок 29. Обтравочные маски слоев или клиппинг-маски.zip',
    'https://persik.by/learning_materials/Нарине Мирзаян. Фотошоп для начинающих Веб-дизайнеров. Материалы/Урок 30. Обтравочные маски слоев или клиппинг-маски. Упражнение 1.zip',
    'https://persik.by/learning_materials/Нарине Мирзаян. Фотошоп для начинающих Веб-дизайнеров. Материалы/Урок 31. Стили и эффекты слоев. Часть 1. Знакомство.zip',
    'https://persik.by/learning_materials/Нарине Мирзаян. Фотошоп для начинающих Веб-дизайнеров. Материалы/Урок 32. Стили и эффекты слоев. Часть 2. Bevel & Emboss.zip',
    'https://persik.by/learning_materials/Нарине Мирзаян. Фотошоп для начинающих Веб-дизайнеров. Материалы/Урок 33. Стили и эффекты слоев. Упражнение - Bevel & Emboss.zip',
    'https://persik.by/learning_materials/Нарине Мирзаян. Фотошоп для начинающих Веб-дизайнеров. Материалы/Урок 36. Стили и эффекты слоев. Часть 4. Inner and Drop Shadow.zip',
    'https://persik.by/learning_materials/Нарине Мирзаян. Фотошоп для начинающих Веб-дизайнеров. Материалы/Урок 39. Стили и эффекты слоев. Упражнение - Inner and Outer Glow.zip',
    'https://persik.by/learning_materials/Нарине Мирзаян. Фотошоп для начинающих Веб-дизайнеров. Материалы/Урок 41. Стили и эффекты слоев. Часть 7. Color, Gradient, Pattern Overlay.zip',
    'https://persik.by/learning_materials/Нарине Мирзаян. Фотошоп для начинающих Веб-дизайнеров. Материалы/Урок 42. Корректировочные слои. Часть 1. Solid Color, Gradient, Pattern, Brightness.zip',
    'https://persik.by/learning_materials/Нарине Мирзаян. Фотошоп для начинающих Веб-дизайнеров. Материалы/Урок 43. Корректировочные слои. Часть 2. Levels.zip',
    'https://persik.by/learning_materials/Нарине Мирзаян. Фотошоп для начинающих Веб-дизайнеров. Материалы/Урок 44. Корректировочные слои. Часть 3. Curves. Exposure.zip',
    'https://persik.by/learning_materials/Нарине Мирзаян. Фотошоп для начинающих Веб-дизайнеров. Материалы/Урок 45. Корректировочные слои. Часть 4. Hue_Saturation. Vibrance.zip',
    'https://persik.by/learning_materials/Нарине Мирзаян. Фотошоп для начинающих Веб-дизайнеров. Материалы/Урок 46. Корректировочные слои. Часть 5. Color Balance. Black&White. Photo Filter.zip',
    'https://persik.by/learning_materials/Нарине Мирзаян. Фотошоп для начинающих Веб-дизайнеров. Материалы/Урок 47. Корректировочные слои. Часть 6. Invert. Posterize. Threshold. Gradient Map.zip',
    'https://persik.by/learning_materials/Нарине Мирзаян. Фотошоп для начинающих Веб-дизайнеров. Материалы/Урок 48. Использование корректировок. Часть 1. ShadowsHighlights. HDR Toning.zip',
    'https://persik.by/learning_materials/Нарине Мирзаян. Фотошоп для начинающих Веб-дизайнеров. Материалы/Урок 49. Использование корректировок. Часть 2. Desaturate, Match Color, Replace Color.zip',
    'https://persik.by/learning_materials/Нарине Мирзаян. Фотошоп для начинающих Веб-дизайнеров. Материалы/Урок 50. Меню Image.zip',
    'https://persik.by/learning_materials/Нарине Мирзаян. Фотошоп для начинающих Веб-дизайнеров. Материалы/Урок 51. Основы фильтров.zip',
    'https://persik.by/learning_materials/Нарине Мирзаян. Фотошоп для начинающих Веб-дизайнеров. Материалы/Урок 52. Группа фильтров Artistic (Имитация).zip',
    'https://persik.by/learning_materials/Нарине Мирзаян. Фотошоп для начинающих Веб-дизайнеров. Материалы/Урок 53. Группа фильтров Brush Strokes (Штрихи) и Distort (Искажение).zip',
    'https://persik.by/learning_materials/Нарине Мирзаян. Фотошоп для начинающих Веб-дизайнеров. Материалы/Урок 54. Группа фильтров Sketch (Эскиз).zip',
    'https://persik.by/learning_materials/Нарине Мирзаян. Фотошоп для начинающих Веб-дизайнеров. Материалы/Урок 55. Группа фильтров Stylize (Стилизация) и Texture (Текстура).zip',
    'https://persik.by/learning_materials/Нарине Мирзаян. Фотошоп для начинающих Веб-дизайнеров. Материалы/Урок 56. Использование фильтра Адаптивный широкий угол.zip',
    'https://persik.by/learning_materials/Нарине Мирзаян. Фотошоп для начинающих Веб-дизайнеров. Материалы/Урок 57. Liquify - Пластика.zip',
    'https://persik.by/learning_materials/Нарине Мирзаян. Фотошоп для начинающих Веб-дизайнеров. Материалы/Урок 58. Меню Фильтры продолжение. Группы фильтров Blur и Pixelete.zip',
    'https://persik.by/learning_materials/Нарине Мирзаян. Фотошоп для начинающих Веб-дизайнеров. Материалы/Урок 59. Меню Фильтры продолжение. Группы фильтров Render и Sharpen.zip',
    'https://persik.by/learning_materials/Нарине Мирзаян. Фотошоп для начинающих Веб-дизайнеров. Материалы/Урок 60. Меню Фильтры продолжение. Группы фильтров Stylize.zip',
    'https://persik.by/learning_materials/Нарине Мирзаян. Фотошоп для начинающих Веб-дизайнеров. Материалы/Урок 61. Меню Фильтры продолжение. Группы фильтров Other.zip',
    'https://persik.by/learning_materials/Нарине Мирзаян. Фотошоп для начинающих Веб-дизайнеров. Материалы/Урок 62. Панели Actions.zip',
    'https://persik.by/learning_materials/Нарине Мирзаян. Фотошоп для начинающих Веб-дизайнеров. Материалы/Урок 63. Панели Layer Comps.zip',
    'https://persik.by/learning_materials/Нарине Мирзаян. Фотошоп для начинающих Веб-дизайнеров. Материалы/Урок 64. Трансформирование объектов.zip',
    'https://persik.by/learning_materials/Нарине Мирзаян. Фотошоп для начинающих Веб-дизайнеров. Материалы/Урок 65. Сохранение и экспорт файлов.zip',
    'https://persik.by/learning_materials/Нарине Мирзаян. Фотошоп для начинающих Веб-дизайнеров. Материалы/Урок 69. Рисуем кнопки.zip',
    'https://persik.by/learning_materials/Нарине Мирзаян. Фотошоп для начинающих Веб-дизайнеров. Материалы/Урок 70. Пример создания веб-страницы - Часть 1.zip'
  ]
};

export function getSkills(courseId: number): Skill[] {
  return skills[courseId];
}

export function getRequirements(courseId: number): string[] {
  return requirements[courseId];
}

export function getTeachers(courseId: number): Teacher[] {
  return teachers[courseId];
}

export function getPreview(courseId: number): string {
  return preview[courseId];
}

export function getMaterial(courseId: number, lessonName: string): string {
  const courseMaterials = materials[courseId];
  if (courseMaterials && courseMaterials.length > 0) {
    const lessonAnchor = lessonName.match(/урок[ ]*[0-9]+/gmi);
    if (lessonAnchor && lessonAnchor[0]) {
      const materialsLink = courseMaterials.find(link => {
        const linkAchor = link.match(/урок[ ]*[0-9]+/gmi);
        return linkAchor[0] === lessonAnchor[0];
      });
      return materialsLink;
    }
    return null;
  }
  return null;
}
