import { Course } from './../../../models/course';

export const staticCourseData: Course = {
  id: 1,
  name: '',
  type: '',
  price: 0,
  desc: '',
  cover: '',
  skills: [],
  requirements: [],
  seasons: [],
  teachers: [],
  anotherCourses: [ 1047632, 966238, 1606986, 1041757 ],
  // comments: [
  //   {
  //     userName: 'Александр Сахаров',
  //     userJob: 'CTO & Co-founder в LoyaltyLab',
  //     text: 'Lorem ipsum dolor sit amet, consectetur e natus error sit voluptatem accusantium doloremque laudantium.',
  //     photo: 'assets/images/course-page/user.png'
  //   },
  //   {
  //     userName: 'Александр Сахаров',
  //     userJob: 'CTO & Co-founder в LoyaltyLab',
  //     text: 'Lorem ipsum dolor sit amet, consectetur unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.',
  //     photo: 'assets/images/course-page/user.png'
  //   },
  //   {
  //     userName: 'Александр Сахаров',
  //     userJob: 'CTO & Co-founder в LoyaltyLab',
  //     text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et',
  //     photo: 'assets/images/course-page/user.png'
  //   },
  //   {
  //     userName: 'Александр Сахаров',
  //     userJob: 'CTO & Co-founder в LoyaltyLab',
  //     text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore',
  //     photo: 'assets/images/course-page/user.png'
  //   },
  //   {
  //     userName: 'Александр Сахаров',
  //     userJob: 'CTO & Co-founder в LoyaltyLab',
  //     text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et',
  //     photo: 'assets/images/course-page/user.png'
  //   },
  // ]
};
