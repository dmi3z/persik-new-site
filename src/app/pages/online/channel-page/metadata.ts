import { MetaDefinition } from '@angular/platform-browser';
import { MetaData } from '../../../models/meta';


export function setContentData(type: string, content_name: string, logo): MetaData {
  const tags: MetaDefinition[] = [
    {
      name: 'keywords',
      content: `cмотреть ${type} ${content_name} онлайн`
    },
    {
      name: 'description',
      content: `Смотреть ${type} ${content_name} online.
      ✅ Первый месяц всего за 0,30 руб! ✅ Архив 24 дня, Full HD качество, 100 каналов, мультирум.
      info@persik.by`
    },
    {
      property: 'og:image',
      content: logo
    }
  ];

  const title = `Смотреть ${type} ${content_name} онлайн в хорошем качестве`;

  return {
    title,
    tags
  };
}

