import { ModalController } from './../../../services/modal.controller';
import { Channel } from './../../../models/channel';
import { isPlatformBrowser } from '@angular/common';
import { Component, OnInit, PLATFORM_ID, Inject, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DataService, TranslitService, MetaService } from '../../../services';
import { MetaData } from './../../../models/meta';

import { setContentData } from './metadata';
import { Video } from './../../../models/vod';
import { ModalName } from '../../../models';

@Component({
  selector: 'app-channel-page',
  templateUrl: './channel-page.component.html',
  styleUrls: ['./channel-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class ChannelPageComponent implements OnInit {

  public isBrowser: boolean;

  private content_id: string;
  public isCanPlay: boolean;
  public isLoadError: boolean;

  public currentTvShow: Video = new Video;

  public channel: Channel;
  public cover: string;

  public videos: Video[] = [];

  private skip = 0;
  private currentPage = 0;
  private idsByPage: Array<string[]> = [];
  private readonly loadPart: number = 20; // Количество разово загружаемых видео на отображение в ленте

  constructor(
    @Inject(PLATFORM_ID) private platform: any,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private dataService: DataService,
    private transliteService: TranslitService,
    private metaService: MetaService,
    private cdr: ChangeDetectorRef,
    private modalController: ModalController
  ) {
  }

  ngOnInit() {
    if (isPlatformBrowser(this.platform)) {
      this.isBrowser = true;
    }
    this.metaService.createCanonicalURL();
    const id: string = this.activatedRoute.snapshot.params.id;
    const params = id.split('-');
    this.content_id = params[params.length - 1];
    if (this.content_id) {
      this.loadInfo();
      this.loadCurrentTvShow();
    }
  }

  private loadInfo(): void {
    this.dataService.loadChannel(+this.content_id).then(res => {
      this.channel = res;
      let meta: MetaData;
      meta = setContentData('канал', res.name, res.logo);
      this.metaService.updateMeta(meta);
      if (this.isHaveArchive) {
        this.loadTvShows();
      }
    });
  }

  public get getDescrPart(): string {
    if (this.currentTvShow.description && this.currentTvShow.description.length > 230) {
      return this.currentTvShow.description.slice(0, 240) + '...';
    }
  }

  public openChannel(): void {
    if (this.channel.available) {
      const cpu: string = this.transliteService.convertCyrillToLatin(this.channel.name, this.channel.channel_id.toString());
      this.router.navigate(['channel-player', cpu]);
    } else {
      this.modalController.present(ModalName.FAST_AUTH);
    }
  }

  public get isHaveArchive(): boolean {
    if (this.channel) {
      return this.channel.dvr_sec > 0;
    }
    return false;
  }

  private loadCurrentTvShow(): void {
    this.dataService.getCurrentTvShow(+this.content_id).then(res => {
      this.dataService.getVideoInfo(res.tvshow_id).then(result => {
        this.currentTvShow = result;
        this.cdr.markForCheck();
      });
    });
  }

  private loadTvShows(): void {
    this.dataService.getArchiveTvshows(+this.content_id, this.skip).then(res => {
      // реверс для отображения видео с сегодняшнего дня и в прошлое
      res = res.reverse();
      const videoIDs: string[] = [];
      res.forEach(element => {
        videoIDs.push(element.tvshow_id);
      });
      this.idsByPage = this.chunkArray(videoIDs, this.loadPart);
      this.loadPage();
    });
    this.skip += this.loadPart;
  }

  private chunkArray(inputArray: Array<any>, perChunk: number): Array<any[]> {
    return inputArray.reduce((resultArray, item, index) => {
      const chunkIndex = Math.floor(index / perChunk);
      if (!resultArray[chunkIndex]) {
        resultArray[chunkIndex] = [];
      }
      resultArray[chunkIndex].push(item);
      return resultArray;
    }, []);
  }

  private loadPage(): void {
    if (this.idsByPage.length < this.currentPage + 1) {
      return;
    }
    const ids = this.idsByPage[this.currentPage];
    this.dataService.getVideosInfo(ids).then(res => {
      this.videos.push(...res);
      this.currentPage++;
      this.cdr.markForCheck();
    });
  }

  public onScroll() {
    this.loadPage();
  }

}
