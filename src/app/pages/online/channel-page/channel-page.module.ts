import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { ChannelPageComponent } from './channel-page.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ShareButtonsModule } from '@ngx-share/buttons';
import { FilmCardModule } from '../../../components/content-slider/film-card/film-card.module';

@NgModule({
  declarations: [
    ChannelPageComponent
  ],
  imports: [
    CommonModule,
    InfiniteScrollModule,
    ShareButtonsModule,
    FilmCardModule,
    RouterModule.forChild([
      {
        path: '',
        component: ChannelPageComponent
      }
    ])
  ]
})

export class ChannelPageModule { }
