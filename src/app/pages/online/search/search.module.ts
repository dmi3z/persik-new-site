import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ContentSliderModule } from '../../../components/content-slider/content-slider.module';
import { FormsModule } from '@angular/forms';
import { SearchPageComponent } from './search-page.component';

@NgModule({
  declarations: [
    SearchPageComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ContentSliderModule,
    RouterModule.forChild([
      {
        path: '',
        component: SearchPageComponent
      }
    ])
  ]
})

export class SearchPageModule {}
