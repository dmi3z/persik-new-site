import { LitresService } from './../audiobooks/litres.service';
import { Book } from './../audiobooks/litres.model';
import { Location } from '@angular/common';
import { SearchData } from './../../../models/search';
import { SearchService, DataService } from './../../../services';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-search-page',
  templateUrl: './search-page.component.html',
  styleUrls: ['./search-page.component.scss']
})

export class SearchPageComponent implements OnInit, OnDestroy {

  public searchPhrase: string;
  public searchResult: SearchData;
  public books: Book[] = [];

  private searchEmitSubscriber: Subscription;

  constructor(
    private searchService: SearchService,
    private dataService: DataService,
    private location: Location,
    private litresService: LitresService
  ) {}

  ngOnInit(): void {
    this.searchPhrase = this.searchService.searchPhrase;
    this.trySearch();
    this.searchEmitSubscriber = this.searchService.searchEmitter.subscribe((phrase) => {
      this.searchPhrase = phrase;
      this.trySearch();
    });
    if (this.searchPhrase && this.searchPhrase.length > 2) {
      return;
    } else {
      this.location.back();
    }
  }

  public get isHaveChannels(): boolean {
    return this.searchResult && (this.searchResult.channels.length > 0);
  }

  public get isHaveVideos(): boolean {
    return this.searchResult && (this.searchResult.videos.length > 0);
  }

  public get isHaveTvshows(): boolean {
    return this.searchResult && (this.searchResult.tvshows.length > 0);
  }

  public get isHaveBooks(): boolean {
    return this.searchResult && this.searchResult.litres.length > 0;
  }

  public setSearchPhrase(): void {
    this.searchService.searchPhrase = this.searchPhrase;
  }

  private trySearch(): void {
    if (this.searchPhrase && this.searchPhrase.length > 0) {
      this.dataService.searchContent(this.searchPhrase).then(result => {
        this.searchResult = result;
        if (this.searchResult.litres.length > 0) {
          this.litresService.getBookInfo(this.searchResult.litres).then(data => {
            this.books = data;
          });
        }
      });
    }
  }

  ngOnDestroy() {
    if (this.searchEmitSubscriber) {
      this.searchEmitSubscriber.unsubscribe();
    }
  }
}
