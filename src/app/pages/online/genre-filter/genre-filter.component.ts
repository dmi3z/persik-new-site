import { Genre } from '../../../models';
import { Component, Input, Output, EventEmitter, OnInit, OnChanges, SimpleChanges, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-genre-filter',
  templateUrl: './genre-filter.component.html',
  styleUrls: ['./genre-filter.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class GenreFilterComponent implements OnInit, OnChanges {

  @Input() active: Genre;
  @Input() genres: Genre[];
  @Input() isShowAll: boolean;
  @Input() isHideToggleButton: boolean;
  public isShowMore: boolean;
  public genresForShow: Genre[] = [];

  @Output() genreChange: EventEmitter<Genre> = new EventEmitter();

  constructor() {}

  ngOnInit(): void {
    this.genresForShow = this.genres;
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['genres']) {
      this.genresForShow = changes['genres'].currentValue;
    }
  }

  public selectFilter(genre: Genre): void {
    this.genreChange.emit(genre);
  }

  public getIsActive(genre: Genre): boolean {
    if (genre && genre.id && this.active && this.active.id) {
      return genre.id === this.active.id;
    }
    return false;
  }

  public toggleMore(): void {
    this.isShowMore = !this.isShowMore;
  }

}
