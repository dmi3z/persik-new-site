import { CommonModule } from '@angular/common';
import { GenreFilterComponent } from './genre-filter.component';
import { NgModule } from '@angular/core';

@NgModule({
  declarations: [
    GenreFilterComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    GenreFilterComponent
  ]
})

export class GenreFilterModule {}
