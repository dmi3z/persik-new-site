// import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { NgModule } from '@angular/core';
import { FeaturedComponent } from './featured.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ContentSliderModule } from '../../../components/content-slider/content-slider.module';
import { VideobannerComponentModule } from '../../../components/videobanner-slider/videobanner-slider.module';

@NgModule({
  declarations: [
    FeaturedComponent
  ],
  imports: [
    CommonModule,
    ContentSliderModule,
    VideobannerComponentModule,
    // InfiniteScrollModule,
    RouterModule.forChild([
      {
        path: '',
        component: FeaturedComponent
      }
    ])
  ]
})

export class FeaturedPageModule { }
