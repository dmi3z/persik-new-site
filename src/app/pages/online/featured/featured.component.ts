import { AuthService } from './../../../services/auth.service';
import { isPlatformBrowser } from '@angular/common';
import { VodSection, Channel, VideoInterface } from '../../../models';
import { Component, OnInit, PLATFORM_ID, Inject } from '@angular/core';
import { RouterBackService, DataService, LoadingService } from '../../../services';


@Component({
  selector: 'app-featured',
  templateUrl: './featured.component.html',
  styleUrls: ['./featured.component.scss']
})

export class FeaturedComponent implements OnInit {

  public featuredChannels: Channel[];
  public vodSections: VodSection[];
  public isBrowser: boolean;

  public films: VideoInterface;
  public cartoons: VideoInterface;
  public series: VideoInterface;

  public comedies: VideoInterface;
  public biography: VideoInterface;
  public horrors: VideoInterface;

  public blogs: VideoInterface;

  // public featuredChannelsTapes: FeaturedChannelsTape[] = [];
  // public featuredFilmsTapes: FeaturedVideosTape[] = [];
  // public featuredSeriesTapes: FeaturedVideosTape[] = [];
  // public featuredCartoonsTapes: FeaturedVideosTape[] = [];
  // public featuredTvshowsTapes: FeaturedVideosTape[] = [];

  // public favoriteChannels: FeaturedChannelsTape;
  // public favoriteVideos: FeaturedVideosTape;

  // public lastSeen: any[] = [];
  // public lastFinished: any[] = [];
  // public viewedChannels: Channel[] = [];

  public isLogin: boolean;

  // public loadingStep = 0;

  constructor(
    private routerBackService: RouterBackService,
    private dataService: DataService,
    private loadingService: LoadingService,
    @Inject(PLATFORM_ID) private platform: any,
    private authService: AuthService
  ) { }

  ngOnInit() {
    if (isPlatformBrowser(this.platform)) {
      this.isBrowser = true;
    }
    this.loadingService.startLoading();
    this.routerBackService.from = '';
    this.loadFeaturedChannels();
    this.loadFilms();
    this.loadBiography();
    this.loadComedies();
    this.loadHorrors();
    this.loadSeries();
    this.loadCartoons();
    this.loadBlogs();
    // this.loadChannelsForFeatured();
    // this.loadFeaturedVideos();
    if (this.authService.isLogin) {
      this.isLogin = true;
      //   this.loadFavoriteChannels();
      //   this.loadFavoriteVideos();

      // this.lastFinished = this.localStorageService.getFinished().map(i => {
      //   if (i.type === 'video') {
      //     return {
      //       video_id: i.id,
      //       seek: i.seek
      //     };
      //   } else {
      //     return {
      //       tvshow_id: i.id,
      //       seek: i.seek
      //     };
      //   }
      // });

      // this.lastSeen = this.localStorageService.getRecently().map(i => {
      //   return {
      //     video_id: i.id,
      //     seek: i.seek
      //   };
      // });

      // this.viewedChannels = this.localStorageService.getViewedChannels().map(i => {
      //   return i.channel;
      // });
    }
  }

  public get isHaveFilms(): boolean {
    return this.films && this.films.videos && this.films.videos.length > 0;
  }

  public get isHaveSeries(): boolean {
    return this.series && this.series.videos && this.series.videos.length > 0;
  }

  public get isHaveCartoons(): boolean {
    return this.cartoons && this.cartoons.videos && this.cartoons.videos.length > 0;
  }

  public get isHaveBlogs(): boolean {
    return this.blogs && this.blogs.videos && this.blogs.videos.length > 0;
  }

  public get isHaveComedies(): boolean {
    return this.comedies && this.comedies.videos && this.comedies.videos.length > 0;
  }

  public get isHaveBiography(): boolean {
    return this.biography && this.biography.videos && this.biography.videos.length > 0;
  }

  public get isHaveHorrors(): boolean {
    return this.horrors && this.horrors.videos && this.horrors.videos.length > 0;
  }

  /* public get isHaveLastFinished(): boolean {
    return this.lastFinished.length > 0;
  } */

  /* public get isHaveLastSeen(): boolean {
    return this.lastSeen.length > 0;
  } */

  /* public get isHaveViewedChannels(): boolean {
    return this.viewedChannels.length > 0;
  } */

  /*/ public get isHaveFavoriteChannels(): boolean {
    if (this.favoriteChannels && this.favoriteChannels.channels.length > 0) {
      return true;
    }
    return false;
  } */

  /* public get isHaveFavoriteVideos(): boolean {
    if (this.favoriteVideos && this.favoriteVideos.videos.length > 0) {
      return true;
    }
    return false;
  } */

  // private loadFavoriteChannels(): void {
  //   this.favoriteService.getFavoriteChannels().then(res => {
  //     this.favoriteChannels = {
  //       title: 'Избранные каналы',
  //       channels: []
  //     };
  //     this.dataService.getChannels().then(channels => {
  //       res.forEach(ch => {
  //         const fchannel = channels.find(c => c.channel_id === ch.channel_id);
  //         if (fchannel) {
  //           this.favoriteChannels.channels.push(fchannel);
  //         }
  //       });
  //     });
  //   });
  // }

  // private loadFavoriteVideos(): void {
  //   this.favoriteService.getFavoriteVideos().then(videos => {
  //     const ids = videos.map(v => v.video_id.toString());
  //     this.favoriteVideos = {
  //       title: 'Избранные фильмы и передачи',
  //       videos: []
  //     };
  //     this.dataService.getVideosInfo(ids).then(res => {
  //       this.favoriteVideos.videos = res;
  //     });
  //   });
  // }

  private loadFeaturedChannels(): void {
    this.dataService.loadFeatureChannels().then(res => {
      if (res.length > 0) {
        this.dataService.getChannels().then(chs => {
          this.featuredChannels = chs.filter(ch => res.map(c => c.channel_id).includes(ch.channel_id));
          this.featuredChannels.push({
            channel_id: -1,
            age_rating: null,
            available: true,
            dvr_sec: null,
            genres: [],
            logo: null,
            name: 'stub',
            priority: 0,
            rank: 0,
            stream_url: null
          });
        });
      }
    });
  }

  /* private loadChannelsForFeatured(): void {
    const genres: Genre[] = [
      {
        id: 602,
        name: 'Белорусские',
        name_en: 'belorusskie',
        is_main: true
      },
      {
        id: 98,
        name: 'Детские',
        name_en: 'detskie',
        is_main: true
      },
      {
        id: 99,
        name: 'Кино и сериалы',
        name_en: 'films-i-serialy',
        is_main: true
      },
      {
        id: 100,
        name: 'Музыкальные',
        name_en: 'musical',
        is_main: true
      },
      {
        id: 102,
        name: 'Познавательные',
        name_en: 'poznovatelnie',
        is_main: true
      },
      {
        id: 103,
        name: 'Развлекательные',
        name_en: 'razvlekatelnie',
        is_main: true
      },
      {
        id: 105,
        name: 'Спортивные',
        name_en: 'pro-sport',
        is_main: true
      }
    ];
    this.dataService.getChannelsForFeatured(genres).then(res => {
      this.featuredChannelsTapes = res;
      this.loadingService.stopLoading();
    });
  } */

  /* public loadNext(): void {
    this.loadingStep += 1;
    switch (this.loadingStep) {
      case 1:
        this.loadingService.startLoading();
        this.loadFilmsTapes();
        break;
      case 2:
        this.loadingService.startLoading();
        this.loadSeriesTapes();
        break;
      case 3:
        this.loadingService.startLoading();
        this.loadCartoonsTapes();
        break;
      case 4:
        this.loadingService.startLoading();
        this.loadTvshowsTapes();
        break;
      default:
        break;
    }
  } */

  /* private loadFeaturedVideos(): void {
    this.dataService.loadVodFeatured().then(res => {
      if (res.sections.length > 0) {
        this.vodSections = res.sections;
        this.loadingService.stopLoading();
      }
    });
  } */

  private loadFilms(): void {
    this.dataService.getVideoContent(1, 882, 10).then(res => {
      if (res && res.videos && res.videos.length > 0) {
        this.films = res;
        if (this.films.videos.length === 10) {
          this.films.videos.push({ video_id: -1 });
        }
      }
    });
  }

  private loadComedies(): void {
    this.dataService.getVideoContent(1, 47, 10).then(res => {
      if (res && res.videos && res.videos.length > 0) {
        this.comedies = res;
        if (this.comedies.videos.length === 10) {
          this.comedies.videos.push({ video_id: -1 });
        }
      }
    });
  }

  private loadBiography(): void {
    this.dataService.getVideoContent(1, 37, 10).then(res => {
      if (res && res.videos && res.videos.length > 0) {
        this.biography = res;
        if (this.biography.videos.length === 10) {
          this.biography.videos.push({ video_id: -1 });
        }
      }
    });
  }

  private loadHorrors(): void {
    this.dataService.getVideoContent(1, 56, 10).then(res => {
      if (res && res.videos && res.videos.length > 0) {
        this.horrors = res;
        if (this.horrors.videos.length === 10) {
          this.horrors.videos.push({ video_id: -1 });
        }
      }
    });
  }

  private loadSeries(): void {
    this.dataService.getVideoContent(2, 883, 10).then(res => {
      if (res && res.videos && res.videos.length > 0) {
        this.series = res;
        if (this.series.videos.length === 10) {
          this.series.videos.push({ video_id: -1 });
        }
      }
    });
  }

  private loadCartoons(): void {
    this.dataService.getVideoContent(3, 887, 10).then(res => {
      if (res && res.videos && res.videos.length > 0) {
        this.cartoons = res;
        if (this.cartoons.videos.length === 10) {
          this.cartoons.videos.push({ video_id: -1 });
        }
      }
      this.loadingService.stopLoading();
    });
  }

  private loadBlogs(): void {
    this.dataService.getVideoContent(4, 892, 10).then(res => {
      if (res && res.videos && res.videos.length > 0) {
        this.blogs = res;
        if (this.blogs.videos.length === 10) {
          this.blogs.videos.push({ video_id: -1 });
        }
      }
      this.loadingService.stopLoading();
    });
  }

  /* private loadAllTapes() {
      this.dataService.loadVodCategories().then(cats => {
        const g_films_ids: number[] = [1007, 1008, 1023, 1009, 1010, 1011, 1012, 1013, 1014];
        const g_series_ids: number[] = [1015, 1024];
        const g_cartoons_ids: number[] = [1016];
        const g_tvshows_ids: number[] = [655, 447, 449, 96, 892];
        const allgenres_t: Genre[][] = cats.map(cat => cat.genres);
        const allgenres: Genre[] = [];
        for (let i = 0; i < allgenres_t.length; i++) {
          allgenres.push(...allgenres_t[i]);
        }
        this.loadFilmsWithCategory(allgenres.filter(genre => g_films_ids.includes(genre.id))).then(_ =>
        this.loadSeriesWithCategory(allgenres.filter(genre => g_series_ids.includes(genre.id)))).then(_ =>
        this.loadCartoonsWithCategory(allgenres.filter(genre => g_cartoons_ids.includes(genre.id)))).then(_ =>
        this.loadTvshowsWithCategory(allgenres.filter(genre => g_tvshows_ids.includes(genre.id))));
      });
  } */

  /* private loadFilmsTapes(): void {
    this.dataService.loadVodCategories().then(cats => {
      const g_films_ids: number[] = [
        1014, 1023, 1007, 1010, 1009, 1011, 1012, 1008, 1013, 1157, 1158,
        1159, 1160, 1161, 1162, 1163, 1164, 1165, 1166, 1167, 1168, 1169
      ];
      const allgenres_t: Genre[][] = cats.map(cat => cat.genres);
      const allgenres: Genre[] = [];
      for (let i = 0; i < allgenres_t.length; i++) {
        allgenres.push(...allgenres_t[i]);
      }
      const genres = g_films_ids.map(gfi => {
        const genre = allgenres.find(g => g.id === gfi);
        return genre;
      });
      this.loadFilmsWithCategory(genres);
    });

  } */

  /* private loadSeriesTapes(): void {
    this.dataService.loadVodCategories().then(cats => {
      const g_series_ids: number[] = [1024, 1015, 1170, 1172, 1173, 1174, 1175];
      const allgenres_t: Genre[][] = cats.map(cat => cat.genres);
      const allgenres: Genre[] = [];
      for (let i = 0; i < allgenres_t.length; i++) {
        allgenres.push(...allgenres_t[i]);
      }
      const genres = g_series_ids.map(gfi => {
        const genre = allgenres.find(g => g.id === gfi);
        return genre;
      });
      this.loadSeriesWithCategory(genres);
    });
  } */

  /* private loadCartoonsTapes(): void {
    this.dataService.loadVodCategories().then(cats => {
      const g_cartoons_ids: number[] = [1016, 1155, 1156];
      const allgenres_t: Genre[][] = cats.map(cat => cat.genres);
      const allgenres: Genre[] = [];
      for (let i = 0; i < allgenres_t.length; i++) {
        allgenres.push(...allgenres_t[i]);
      }
      const genres = g_cartoons_ids.map(gfi => {
        const genre = allgenres.find(g => g.id === gfi);
        return genre;
      });
      this.loadCartoonsWithCategory(genres);
    });
  } */

  /* private loadTvshowsTapes(): void {
    this.dataService.loadVodCategories().then(cats => {
      const g_tvshows_ids: number[] = [892, 96, 447, 449, 655];
      const allgenres_t: Genre[][] = cats.map(cat => cat.genres);
      const allgenres: Genre[] = [];
      for (let i = 0; i < allgenres_t.length; i++) {
        allgenres.push(...allgenres_t[i]);
      }
      const genres = g_tvshows_ids.map(gfi => {
        const genre = allgenres.find(g => g.id === gfi);
        return genre;
      });
      this.loadTvshowsWithCategory(genres);
    });
  }

  private loadFilmsWithCategory(genres: Genre[]): void {
    for (let i = 0; i < genres.length; i++) {
      if (genres[i]) {
        this.dataService.getVideoContent(1, genres[i].id, 10).then(v => {
          this.featuredFilmsTapes[i] = {
            title: genres[i].name,
            videos: v.videos
          };
        });
      }
    }
    setTimeout(() => {
      this.loadingService.stopLoading();
    }, 700);
  }

  private loadSeriesWithCategory(genres: Genre[]): void {
    for (let i = 0; i < genres.length; i++) {
      if (genres[i]) {
        this.dataService.getVideoContent(2, genres[i].id, 10).then(v => {
          this.featuredSeriesTapes[i] = {
            title: genres[i].name,
            videos: v.videos
          };
        });
      }
    }
    setTimeout(() => {
      this.loadingService.stopLoading();
    }, 700);
  }

  private loadCartoonsWithCategory(genres: Genre[]): void {
    for (let i = 0; i < genres.length; i++) {
      if (genres[i]) {
        this.dataService.getVideoContent(3, genres[i].id, 10).then(v => {
          this.featuredCartoonsTapes[i] = {
            title: genres[i].name,
            videos: v.videos
          };
        });
      }
    }
    setTimeout(() => {
      this.loadingService.stopLoading();
    }, 700);
  }

  private loadTvshowsWithCategory(genres: Genre[]): void {
    for (let i = 0; i < genres.length; i++) {
      if (genres[i]) {
        this.dataService.getVideoContent(4, genres[i].id, 10).then(v => {
          this.featuredTvshowsTapes[i] = {
            title: genres[i].name,
            videos: v.videos
          };
        });
      }
    }
    setTimeout(() => {
      this.loadingService.stopLoading();
    }, 700);
  } */

}

