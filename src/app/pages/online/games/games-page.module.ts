import { NgModule } from "@angular/core";
import { GamesPageComponent } from './games-page.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';
import { GenreFilterModule } from '../genre-filter/genre-filter.module';

@NgModule({
    declarations: [
        GamesPageComponent
    ],
    imports: [
        CommonModule,
        NgSelectModule,
        FormsModule,
        GenreFilterModule,
        RouterModule.forChild([
            {
                path: '',
                component: GamesPageComponent
            }
        ])
    ]
})

export class GamesPageModule { }
