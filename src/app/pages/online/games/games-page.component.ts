import { Component, OnInit } from '@angular/core';
import { GAMES_DATA } from './games-data';
import { Router, ActivatedRoute } from '@angular/router';
import { Genre } from '../../../models';
import { MetaService } from '../../../services';
import { Location } from '@angular/common';

@Component({
    selector: 'app-games-page',
    templateUrl: 'games-page.component.html',
    styleUrls: ['games-page.component.scss']
})

export class GamesPageComponent implements OnInit {

    private games: Game[] = GAMES_DATA;
    public gamesForShow: Game[] = [];
    public genres: Genre[] = [
        {
            id: 1,
            is_main: true,
            name: 'Все жанры',
            name_en: 'All genres'
        },
        {
            id: 2,
            is_main: true,
            name_en: 'Play & earn',
            name: 'Играй и зарабатывай'
        },
        {
            id: 3,
            is_main: true,
            name_en: 'Mobile',
            name: 'Mobile'
        },
        {
            id: 4,
            is_main: true,
            name: 'Машины',
            name_en: 'Driving'
        },
        {
            id: 5,
            is_main: true,
            name: 'Мультиплеер',
            name_en: 'Multiplayer'
        },
        {
            id: 6,
            is_main: true,
            name: 'Бои',
            name_en: 'Fightings'
        },
        {
            id: 7,
            is_main: true,
            name_en: 'Sports',
            name: 'Спорт'
        },
        {
            id: 8,
            is_main: true,
            name_en: 'Action',
            name: 'Экшн'
        },
        {
            id: 9,
            is_main: true,
            name_en: 'Strategy',
            name: 'Стратегии'
        },
        {
            id: 10,
            is_main: true,
            name: 'GTA',
            name_en: 'GTA'
        },
        {
            id: 11,
            is_main: true,
            name_en: 'Logic',
            name: 'Логика'
        },
        {
            id: 12,
            is_main: true,
            name_en: 'Kids',
            name: 'Детям'
        }
    ];

    public activeGenre: Genre;

    constructor(private router: Router, private metaService: MetaService, public activatedRoute: ActivatedRoute, private location: Location) { }

    ngOnInit() {
        const paramGenreId = this.activatedRoute.snapshot.queryParams.genreId;
        if (paramGenreId) {
            const genre = this.genres.find(genre => genre.id === Number(paramGenreId));
            if (genre) {
                this.onGenreChange(genre);
            } else {
                this.loadStandart();
            }
        } else {
            this.loadStandart();
        }
        this.metaService.createCanonicalURL();
    }

    private loadStandart(): void {
        this.activeGenre = this.genres[0];
        this.gamesForShow = this.games;
    }

    public openGame(gameId: number): void {
        this.router.navigate(['game-description', gameId]);
    }

    public onGenreChange(genre: Genre): void {
        this.activeGenre = genre;
        if (genre.name_en === 'All genres') {
            this.gamesForShow = this.games;
        } else {
            if (genre.id !== 3) {
                this.gamesForShow = this.games.filter(game => game.category === genre.name_en);
            } else {
                this.gamesForShow = this.games.filter(game => game.mobile);
            }            
        }
        this.location.go('/games', `genreId=${genre.id}`);
    }
}

export interface Game {
    id: number;
    name: string;
    url: string;
    developer: string;
    developer_id: number;
    published: string;
    updated: string;
    technology: string;
    filesize: number;
    desktop: boolean;
    mobile: boolean;
    category: string;
    leaderboard: boolean;
    achievements: boolean;
    links: boolean;
    brand: boolean;
    revenue_share: number;
    google_play: string;
    asset_store: string;
    steam: string;
    video: string;
    thumbnail: string;
    image: string;
    description: string;
    controls: string;
    devices?: string[];
    stock?: string;
}
