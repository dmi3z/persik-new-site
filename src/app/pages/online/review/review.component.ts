import { Subscription } from 'rxjs';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Genre, Channel } from '../../../models';
import { LoadingService, TranslitService, DataService, MetaService, RouterBackService, AuthService, ImageService } from '../../../services';
import { setGenreName } from './metadata';
import { Location } from '@angular/common';

@Component({
  selector: 'app-review',
  templateUrl: 'review.component.html',
  styleUrls: ['review.component.scss']
})

export class ReviewComponent implements OnInit, OnDestroy {

  public channels: Channel[] = [];
  public genres: Genre[] = [];
  public activeGenre: Genre;

  private allChannels: Channel[] = [];
  private authServiceSubscriber: Subscription;

  public onAir = true;

  constructor(
    private translitService: TranslitService,
    private router: Router,
    private routerBackService: RouterBackService,
    private loadingService: LoadingService,
    private dataService: DataService,
    private metaService: MetaService,
    private authService: AuthService,
    private activatedRoute: ActivatedRoute,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.metaService.updateMeta(setGenreName());
    this.loadingService.startLoading();
    this.loadData();
    this.authServiceSubscriber = this.authService.loginState.subscribe(_ => {
      this.dataService.channels = [];
      this.allChannels = [];
      this.channels = [];
      this.loadChannels();
    });
  }

  private async loadData() {
    this.dataService.loadChannelCategories().then(categories => {
      this.genres = categories;
      this.activeGenre = this.genres[0];
      const queryGenre = this.activatedRoute.snapshot.queryParams.genre;
      if (queryGenre) {
        const selectedGenre = this.genres.find(genre => this.translitService.convertCyrillToLatin(genre.name_en) === queryGenre);
        if (selectedGenre) {
          this.activeGenre = selectedGenre;
        }
      }
      this.loadChannels();
    });
  }


  private loadChannels(): void {
    this.dataService.getChannels().then(channels => {
      this.allChannels = channels;
      this.loadingService.stopLoading();
      this.getOnAirState();
      // this.checkRouteFilter();
      this.filterChannels();
      this.routerBackService.from = this.router.url;
    });
  }

  private getOnAirState(): void {
    const onAir = localStorage['onAir'];
    if (onAir) {
      this.onAir = JSON.parse(onAir);
    } else {
      this.onAir = true;
    }
  }

  public toggleOnAir(state: boolean): void {
    this.onAir = state;
    localStorage.setItem('onAir', JSON.stringify(state));
  }

  public channelsViewToggle(): void {
    this.onAir = !this.onAir;
  }

  public onGenreChange(genre: Genre): void {
    this.activeGenre = genre;
    if (genre.name === 'Все жанры') {
      this.activeGenre.id = 1;
      this.metaService.updateMeta(setGenreName());
    }
    this.metaService.updateMeta(setGenreName(genre.name));
    const routeName = genre.name_en ? genre.name_en : '';
    const cpu: string = this.translitService.convertCyrillToLatin(routeName);
    // this.router.navigate(['/tv-review'], {queryParams: routeName ? { genre: this.translitService.convertCyrillToLatin(routeName)} : {}});
    this.location.go(`/tv-review?genre=${cpu}`);
    // this.checkRouteFilter(routeName);
    this.filterChannels();
  }

  // private checkRouteFilter(genreName?: string): void {
  //   const param = genreName ? genreName : this.activatedRoute.snapshot.queryParams.genre;
  //   // this.activeGenre = this.genres.find(genre => this.translitService.convertCyrillToLatin(genre.name_en) === param);
  //   this.filterChannels();
  // }

  private filterChannels(): void {
    if (!this.activeGenre || this.activeGenre.id === 1) {
      this.channels = this.allChannels;
    } else {
      this.channels = this.allChannels.filter(channel => channel.genres.includes(this.activeGenre.id));
    }
  }

  ngOnDestroy() {
    if (this.authServiceSubscriber) {
      this.authServiceSubscriber.unsubscribe();
    }
  }
}

