import { ChannelCardModule } from './../../../components/content-slider/channel-card/channel-card.module';
import { GenreFilterModule } from './../genre-filter/genre-filter.module';
import { NgModule } from '@angular/core';
import { ReviewComponent } from './review.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ChannelCardComponent } from './channel-card/channel-card.component';

@NgModule({
  declarations: [
    ReviewComponent,
    ChannelCardComponent
  ],
  imports: [
    CommonModule,
    GenreFilterModule,
    ChannelCardModule,
    RouterModule.forChild([
      {
        path: '',
        component: ReviewComponent
      }
    ])
  ]
})

export class ReviewPageModule {}
