import { Component, OnInit, Input } from "@angular/core";
import { Channel } from '../../../../models';
import * as moment from 'moment';
import { ImageService, DataService, TranslitService } from '../../../../services';
import { Router } from '@angular/router';

@Component({
    selector: 'app-channel-card-full',
    templateUrl: 'channel-card.component.html',
    styleUrls: ['channel-card.component.scss']
})

export class ChannelCardComponent implements OnInit {

    public cover: string;
    public tvshowName: string;
    @Input() channel: Channel;

    constructor(
        private imageService: ImageService,
        private dataService: DataService,
        private translitService: TranslitService,
        private router: Router
    ) { }

    ngOnInit() {
        this.getCover();
        this.loadCurrentTvshow();
    }

    private getCover(): void {
        const currentTime = moment().unix();
        this.cover = this.imageService.getChannelFrame(this.channel.channel_id, currentTime, 'crop', 2000, 2000);
    }

    private loadCurrentTvshow(): void {
        this.dataService.getCurrentTvShow(this.channel.channel_id).then(response => {
            const tvshow = response;
            this.tvshowName = tvshow && tvshow.title && tvshow.title.length > 0 ? tvshow.title : 'Нет данных';
        });
    }

    public openChannel(): void {
        const cpu: string = this.translitService.convertCyrillToLatin(this.channel.name, this.channel.channel_id.toString());
        this.router.navigate(['channel-player', cpu]);
      }
}
