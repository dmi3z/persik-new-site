import { MetaDefinition } from '@angular/platform-browser';
import { MetaData } from '../../../models/meta';

export function setGenreName(genre_name?: string): MetaData {
  const tags: MetaDefinition[] = [
    {
      name: 'keywords',
      content: `ТВ онлайн${genre_name ? ' в жанре ' + genre_name : ''}`
    },
    {
      name: 'description',
      content: `ТВ онлайн${genre_name ? ' в жанре ' + genre_name : ''} - Смотрите Persik TV online через интернет.
      Подключайте телевидение и смотрите более 350 каналов в hd качестве.✅Первый месяц всего за 0,30 руб!
      ✅Архив 24 дня, Full HD качество, программа передач.✅Мультирум. info@persik.by`
    }
  ];

  const title = `${genre_name ? genre_name + ' - c' : 'C'}мотреть тв онлайн в прямом эфире`;

  return {
    title,
    tags
  };
}


