import { NgModule } from '@angular/core';
import { FavoritesPageComponent } from './favorites-page.component';
import { CommonModule } from '@angular/common';
import { ContentSliderModule } from '../../../components/content-slider/content-slider.module';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    FavoritesPageComponent
  ],
  imports: [
    CommonModule,
    ContentSliderModule,
    RouterModule.forChild([
      {
        path: '',
        component: FavoritesPageComponent
      }
    ])
  ]
})

export class FavoritePageModule { }
