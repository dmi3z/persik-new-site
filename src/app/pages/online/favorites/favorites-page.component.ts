import { Book } from './../audiobooks/litres.model';
import { LitresService } from './../audiobooks/litres.service';
import { FavoriteChannel, FavoriteVideo, FavoriteTvshow } from './../../../models/favorite';
import { Component, OnInit } from '@angular/core';
import { FavoriteService, MetaService } from '../../../services';

@Component({
  selector: 'app-favorites-page',
  templateUrl: './favorites-page.component.html',
  styleUrls: ['./favorites-page.component.scss']
})

export class FavoritesPageComponent implements OnInit {

  public slideConfig = {
    'slidesToShow': 4,
    'slidesToScroll': 1,
    'arrows': true,
    'dots': false,
    'fade': false,
    'centerMode': false,
    'autoplay': false,
    'infinity': false,
    'responsive': [
      {
        'breakpoint': 1200,
        'settings': {
          'dots': false,
          'slidesToShow': 3
        }
      },
      {
        'breakpoint': 992,
        'settings': {
          'dots': false,
          'slidesToShow': 2
        }
      },
      {
        'breakpoint': 767,
        'settings': {
          'dots': false,
          'slidesToShow': 1,
          'arrows': false
        }
      },
    ]
  };

  public channels: FavoriteChannel[] = [];
  public videos: FavoriteVideo[] = [];
  public tvshows: FavoriteTvshow[] = [];
  public boughtBooks: Book[] = [];
  public favoriteBooks: Book[] = [];

  constructor(private favoriteService: FavoriteService, private litresService: LitresService, private metaService: MetaService) {}

  ngOnInit() {
    this.metaService.createCanonicalURL();
    this.getContent();
  }

  public get isHaveFavoriteBooks(): boolean {
    if (this.favoriteBooks) {
      return this.favoriteBooks.length > 0;
    }
    return false;
  }

  public get isHaveBoughtBooks(): boolean {
    if (this.boughtBooks) {
      return this.boughtBooks.length > 0;
    }
    return false;
  }

  private getContent(): void {
    this.favoriteService.getFavoriteChannels().then(channels => {
      this.channels = channels;
    });
    this.favoriteService.getFavoriteVideos().then(videos => {
      this.videos = videos;
    });
    this.favoriteService.getFavoriteTvshows().then(tvshows => {
      this.tvshows = tvshows;
    });
    this.litresService.getUsersBooks().then(books => this.boughtBooks = books);
    this.favoriteService.getFavoriteBooks().then(data => {
      if (data && data.length > 0) {
        const ids: number[] = data.map(book => book.litres_item_id);
        this.litresService.getBookInfo(ids).then(result => this.favoriteBooks = result);
      }
    });
  }


}
