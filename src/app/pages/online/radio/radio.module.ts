import { ChannelCardModule } from './../../../components/content-slider/channel-card/channel-card.module';
import { GenreFilterModule } from './../genre-filter/genre-filter.module';
import { NgModule } from '@angular/core';
import { RadioComponent } from './radio.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    RadioComponent,
  ],
  imports: [
    CommonModule,
    GenreFilterModule,
    ChannelCardModule,
    RouterModule.forChild([
      {
        path: '',
        component: RadioComponent
      }
    ])
  ]
})

export class RadioPageModule {}
