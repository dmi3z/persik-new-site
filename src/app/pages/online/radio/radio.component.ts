import { Subscription } from 'rxjs';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Genre, Channel } from '../../../models';
import { LoadingService, TranslitService, DataService, RouterBackService, AuthService } from '../../../services';
import { Location } from '@angular/common';

@Component({
  selector: 'app-radio',
  templateUrl: 'radio.component.html',
  styleUrls: ['radio.component.scss']
})

export class RadioComponent implements OnInit, OnDestroy {

  public radios: Channel[] = [];
  private authServiceSubscriber: Subscription;

  constructor(
    private router: Router,
    private routerBackService: RouterBackService,
    private loadingService: LoadingService,
    private dataService: DataService,
    private authService: AuthService,
  ) {}

  ngOnInit(): void {
    this.loadingService.startLoading();
    this.loadRadios();
    this.authServiceSubscriber = this.authService.loginState.subscribe(_ => {
      this.loadRadios();
    });
  }

  private loadRadios(): void {
    this.dataService.getChannels().then(channels => {
      this.radios = channels.filter(ch => ch.genres.includes(1299));
      this.loadingService.stopLoading();
      this.routerBackService.from = this.router.url;
    });
  }

  ngOnDestroy() {
    if (this.authServiceSubscriber) {
      this.authServiceSubscriber.unsubscribe();
    }
  }
}

