import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { NgSelectModule } from '@ng-select/ng-select';
import { GenreFilterModule } from './../genre-filter/genre-filter.module';
import { BooksPageComponent } from './books.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { AudiobookCardModule } from '../audiobooks/audiobook-card/audiobook-card.module';
import { BooksPipeModule } from '../audiobooks/books-pipe.module';

@NgModule({
  declarations: [
    BooksPageComponent,
  ],
  imports: [
    CommonModule,
    GenreFilterModule,
    NgSelectModule,
    FormsModule,
    RouterModule,
    AudiobookCardModule,
    InfiniteScrollModule,
    BooksPipeModule,
    RouterModule.forChild([
      {
        path: '',
        component: BooksPageComponent
      }
    ])
  ]
})

export class BooksPageModule { }
