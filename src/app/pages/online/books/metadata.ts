import { MetaDefinition } from '@angular/platform-browser';
import { MetaData } from '../../../models/meta';

export function setGenreName(genre_name?: string): MetaData {
  const tags: MetaDefinition[] = [
    {
      name: 'keywords',
      content: `книги онлайн${genre_name ? ' в жанре ' + genre_name : ''}`
    },
    {
      name: 'description',
      content: `Читайте книги ${genre_name ? 'в жанре ' + genre_name : ''} вместе с Persik. ✅Выгодные тарифы.
      ✅Всего 30 копеек за новые фильмы! ✅Архив 24 дня, Full HD качество, программа передач.
      info@persik.by`
    }
  ];

  const title = `Читать книги онлайн${genre_name ? ' в жанре ' + genre_name : ''}`;

  return {
    title,
    tags
  };
}

