import { Component, OnInit, Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { Genre } from '../../../models';
import { LoadingService, MetaService, DataService } from '../../../services';
import { setGenreName } from './metadata';
import { BookGenre, Book } from '../audiobooks/litres.model';
import { LitresService } from '../audiobooks/litres.service';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.scss']
})

export class BooksPageComponent implements OnInit {

  public bookGenres: BookGenre[] = [];
  public activeCategory: BookGenre;
  public isBrowser = false;
  public books: Book[] = [];
  private totalCount: number;
  private skip = 0;
  private readonly loadPart: number = 16; // Количество разово загружаемых аудиокниг на отображение в ленте
  public bannerUrl: string;

  constructor(
    private loadingService: LoadingService,
    @Inject(PLATFORM_ID) private platform: any,
    private metaService: MetaService,
    private litresService: LitresService,
    private dataService: DataService
  ) {}

  ngOnInit(): void {
    this.metaService.updateMeta(setGenreName());
    this.metaService.createCanonicalURL();
    if (isPlatformBrowser(this.platform)) {
      this.isBrowser = true;
    }
    this.loadingService.startLoading();
    this.dataService.getBannersForBook().then(res => this.bannerUrl = res[1].img_url_desktop);
    this.litresService.getGenres('ebook').then(genres => {
      this.bookGenres = genres;
      this.activeCategory = genres[0];
      this.loadContent();
    });
  }

  public onCategoryChange(category: Genre): void {
    this.activeCategory = this.bookGenres.find(bg => bg.litres_genre_id === category.id);
  // this.activeCategory.genres[0];
    this.skip = 0;
    this.books = [];
    this.loadContent();
  }

  public get isHaveGenres(): boolean {
    return this.bookGenres.length > 0;
  }

  private loadContent(): void {
    if (this.activeCategory) {
      this.loadingService.startLoading();
      this.litresService.getList(this.activeCategory.litres_genre_id, this.skip, this.loadPart, 'ebook').then(res => {
        this.totalCount = this.activeCategory.count;
        this.skip += this.loadPart;
        this.books.push(...res);
      }).finally(() => this.loadingService.stopLoading());
    }
  }

  public onScroll() {
    if ((this.totalCount - this.skip) > 0) {
      this.loadContent();
    }
  }
}
