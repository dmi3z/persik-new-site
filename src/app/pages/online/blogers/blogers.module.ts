import { NgModule } from "@angular/core";
import { BlogersPageComponent } from './blogers.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { FilmCardModule } from '../../../components/content-slider/film-card/film-card.module';
import { GenreFilterModule } from '../genre-filter/genre-filter.module';

@NgModule({
    declarations: [
        BlogersPageComponent
    ],
    imports: [
        CommonModule,
        InfiniteScrollModule,
        FilmCardModule,
        GenreFilterModule,
        RouterModule.forChild([
            {
                path: '',
                component: BlogersPageComponent
            }
        ])
    ]
})

export class BlogersPageModule { }
