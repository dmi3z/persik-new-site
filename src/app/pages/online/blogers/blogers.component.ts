import { Component, OnInit, Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser, Location } from '@angular/common';
import { DataService, TranslitService } from '../../../services';
import { Genre } from '../../../models';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'app-blogers-page',
    templateUrl: 'blogers.component.html',
    styleUrls: ['blogers.component.scss']
})

export class BlogersPageComponent implements OnInit {

    public contentIds: any[] = [];
    public isBrowser: boolean;
    public genres: Genre[] = [];
    public activeGenre: Genre;
    private totalCount: number;
    private skip = 0;

    private readonly loadPart: number = 16; // Количество разово загружаемых видео на отображение в ленте
    private readonly categoryId = 4;

    constructor(
        @Inject(PLATFORM_ID) private platform,
        private dataService: DataService,
        private translitService: TranslitService,
        private location: Location,
        private activatedRoute: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() {
        if (isPlatformBrowser(this.platform)) {
            this.isBrowser = true;
        }
        this.dataService.loadVodCategories().then(categories => {
            if (categories[3]) {
                this.genres = categories[3].genres.filter(genre => genre.is_main && genre.name.toLowerCase().includes('блог'));
                this.activeGenre = this.genres[0];
                this.checkRouteFilter();
                this.loadContent();
            }
        });
    }

    public onGenreChange(genre?: Genre): void {
        if (genre) {
          this.activeGenre = genre;
          if (genre.name === 'Все жанры') {
            this.activeGenre.id = 1;
            // this.metaService.updateMeta(setGenreName());
          }
          // this.metaService.updateMeta(setGenreName(genre.name));
          const routeName = genre.name_en ? genre.name_en : '';
          const cpu: string = this.translitService.convertCyrillToLatin(routeName);
          this.location.go(`/blogers?genre=${cpu}`);
        } else {
          this.activeGenre = this.genres[0];
          this.location.go(`/blogers`);
          // this.metaService.updateMeta(setGenreName());
        }
        this.skip = 0;
        this.contentIds = [];
        this.loadContent();
      }

    public get isHaveContent(): boolean {
        return this.contentIds.length > 0;
    }

    public onScroll() {
        console.log(this.totalCount, this.skip);
        if ((this.totalCount - this.skip) > 0) {
            this.loadContent();
        }
    }

    private checkRouteFilter(): void {
        const param = this.activatedRoute.snapshot.queryParams.genre;
        if (param) {
          this.activeGenre = this.genres.find(genre => this.translitService.convertCyrillToLatin(genre.name_en) === param);
          if (!this.activeGenre) {
            this.router.navigate(['404']);
          }
        }
      }

    private loadContent(): void {
        this.dataService.getVideoContent(this.categoryId, this.activeGenre.id, this.loadPart, this.skip).then(result => {
            this.totalCount = result.total;
            const contentIds = result.videos.map(item => {
                if (item.tvshow_id) {
                    return item.tvshow_id;
                }
                return item.video_id;
            });
            this.contentIds.push(...contentIds);
            this.skip += this.loadPart;
            // this.loadingService.stopLoading();
        });
    }


}
