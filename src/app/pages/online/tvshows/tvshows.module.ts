import { CommonModule } from '@angular/common';
import { TvshowsComponent } from './tvshows.component';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { GenreFilterModule } from '../genre-filter/genre-filter.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { FilmCardModule } from '../../../components/content-slider/film-card/film-card.module';

@NgModule({
  declarations: [
    TvshowsComponent
  ],
  imports: [
    CommonModule,
    GenreFilterModule,
    NgSelectModule,
    FormsModule,
    FilmCardModule,
    InfiniteScrollModule,
    RouterModule.forChild([
      {
        path: '',
        component: TvshowsComponent
      }
    ])
  ]
})

export class TvshowsPageModule { }

