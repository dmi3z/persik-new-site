import { Component, OnInit, Inject, PLATFORM_ID } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location, isPlatformBrowser } from '@angular/common';
import { Country, RatingFilter, Genre } from './../../../models';
import { TranslitService, DataService, LoadingService, MetaService } from '../../../services';
import { dateFilters } from '../filtersdata';
import { setGenreName } from './metadata';

@Component({
  selector: 'app-tvshows',
  templateUrl: './tvshows.component.html',
  styleUrls: ['./tvshows.component.scss']
})

export class TvshowsComponent implements OnInit {
  public genres: Genre[] = [];
  public activeGenre: Genre;
  public film_ids: any[] = [];
  public isBrowser = false;
  public countries: Country[] = [];
  public selectedCountry: Country;
  public selectedRatingFilter: RatingFilter;

  public ratingFilters: RatingFilter[] = dateFilters;

  private category_id: number;
  private totalCount: number;
  private skip = 0;
  private readonly loadPart: number = 16; // Количество разово загружаемых видео на отображение в ленте

  constructor(
    private dataService: DataService,
    private activatedRoute: ActivatedRoute,
    private translitService: TranslitService,
    private location: Location,
    private loadingService: LoadingService,
    @Inject(PLATFORM_ID) private platform: any,
    private metaService: MetaService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.metaService.updateMeta(setGenreName());
    this.metaService.createCanonicalURL();
    this.selectedRatingFilter = this.ratingFilters[0];
    if (isPlatformBrowser(this.platform)) {
      this.isBrowser = true;
    }
    this.loadingService.startLoading();
    this.dataService.loadVodCategories().then(categories => {
      if (categories[3]) {
        this.category_id = categories[3].id;
        this.genres = categories[3].genres.filter(genre => genre.is_main).map(genre => {
          return {
            id: genre.id,
            is_main: true,
            name: genre.name.replace(/[0-9]\)/gmi,''),
            name_en: genre.name_en
          }
        });
        this.loadCountryList();
        this.activeGenre = this.genres[0];
        this.checkRouteFilter();
        this.loadContent();
      }
    });
  }

  public get isHaveContent(): boolean {
    return this.film_ids.length > 0;
  }

  public get isHaveGenres(): boolean {
    return this.genres.length > 0;
  }

  public onGenreChange(genre?: Genre): void {
    if (genre) {
      this.activeGenre = genre;
      if (genre.name === 'Все жанры') {
        this.activeGenre.id = 1;
        this.metaService.updateMeta(setGenreName());
      }
      this.metaService.updateMeta(setGenreName(genre.name));
      const routeName = genre.name_en ? genre.name_en : '';
      const cpu: string = this.translitService.convertCyrillToLatin(routeName);
      this.location.go(`/peredachi?genre=${cpu}`);
    } else {
      this.activeGenre = this.genres[0];
      this.location.go(`/peredachi`);
      this.metaService.updateMeta(setGenreName());
    }
    this.skip = 0;
    this.film_ids = [];
    this.loadContent();
  }

  private checkRouteFilter(): void {
    const param = this.activatedRoute.snapshot.queryParams.genre;
    if (param) {
      this.activeGenre = this.genres.find(genre => this.translitService.convertCyrillToLatin(genre.name_en) === param);
      if (!this.activeGenre) {
        this.router.navigate(['404']);
      }
    }
  }

  private loadContent(): void {
    if (this.activeGenre) {
      this.dataService.getVideoContent(this.category_id, this.activeGenre.id, this.loadPart,
        this.skip, this.selectedCountry, this.selectedRatingFilter).then(result => {
        this.totalCount = result.total;
        this.film_ids.push(...result.videos.map(item => {
          if (item.tvshow_id) {
            return item.tvshow_id;
          } return item.video_id;
        }));
        this.skip += this.loadPart;
        this.loadingService.stopLoading();
      });
    }
  }

  public get isHaveCountries(): boolean {
    return this.countries.length > 0;
  }

  public onCountryFilterChange(country: Country): void {
    this.skip = 0;
    this.selectedCountry = country;
    this.film_ids = [];
    this.loadContent();
  }

  private loadCountryList(): void {
    this.dataService.getCountryList(this.category_id).then(res => {
      this.selectedCountry = res[0];
      this.countries = res;
    });
  }

  public onRatingFilterChange(rating: RatingFilter): void {
    this.skip = 0;
    this.selectedRatingFilter = rating;
    this.film_ids = [];
    this.loadContent();
  }

  public onScroll() {
    if ((this.totalCount - this.skip) > 0) {
        this.loadContent();
    }
  }
}
