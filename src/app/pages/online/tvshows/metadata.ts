import { MetaDefinition } from '@angular/platform-browser';
import { MetaData } from '../../../models/meta';

export function setGenreName(genre_name?: string): MetaData {
  const tags: MetaDefinition[] = [
    {
      name: 'keywords',
      content: `передачи онлайн${genre_name ? ' в жанре ' + genre_name : ''}`
    },
    {
      name: 'description',
      content: `Смотрите новые передачи ${genre_name ? 'в жанре ' + genre_name : ''} вместе с Persik. ✅Выгодные тарифы.
      ✅Новые передачи всего за 30 копеек! ✅Архив 24 дня, Full HD качество, программа передач.
      info@persik.by`
    }
  ];

  const title = `Смотреть передачи онлайн${genre_name ? ' в жанре ' + genre_name : ''}`;

  return {
    title,
    tags
  };
}



