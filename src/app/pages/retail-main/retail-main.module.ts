import { NgModule } from '@angular/core';
import { RetailMainPageComponent } from './retail-main.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    RetailMainPageComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      {
        path: '',
        component: RetailMainPageComponent
      }
    ])
  ]
})

export class RetailMainPageModule { }
