import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { emailCorrectValidator } from "../../validators";
import { MetaService, AuthService } from "./../../services";
import { meta } from "../metadata";

@Component({
  selector: "app-retail-main-component",
  templateUrl: "./retail-main.component.html",
  styleUrls: ["./retail-main.component.scss"],
})
export class RetailMainPageComponent implements OnInit {
  constructor(
    private metaService: MetaService,
    private authService: AuthService
  ) {}

  public code: FormControl;
  public phone: FormControl;
  public email: FormControl;
  public codeForm: FormGroup;

  public codeFormElectro: FormGroup;
  public phoneElectro: FormControl;
  public emailElectro: FormControl;

  public isElementForm: boolean;

  public formCheckboxes: FormCheckbox[] = [
    {
      name: "1 месяц",
      count: 0,
      checked: false,
      id: 1,
    },
    {
      name: "6 месяцев",
      count: 0,
      checked: false,
      id: 2,
    },
    {
      name: "6 месяцев",
      count: 0,
      checked: false,
      id: 3,
    },
    {
      name: "1 год",
      count: 0,
      checked: false,
      id: 4,
    },
  ];

  ngOnInit() {
    // this.authService.reta
    this.createFormFields();
    this.createForm();
    this.metaService.updateMeta(meta);
  }

  private toggleRadio(targetItem): void {
    targetItem.checked = true;
    this.formCheckboxes.forEach((item) => {
      if (targetItem.id === item.id) {
        return;
      }
      item.checked = false;
      item.count = 0;
    });
  }

  public labelClick(item): void {
    this.toggleRadio(item);
    if (item.count === 0) {
      item.count = 1;
    }
  }

  public minusCount(item): void {
    if (item.count - 1 <= 0) {
      item.count = 0;
      item.checked = false;
    } else {
      item.count--;
    }
  }

  public plusCount(item): void {
    this.toggleRadio(item);
    if (item.count > 99) {
      item.count = 99;
    } else if (item.count <= 0) {
      item.count = 1;
    } else {
      item.count++;
    }
  }

  public sendCode(): void {
    console.log(this.codeForm.value);
  }

  public sendCodeElectro(): void {
    console.log(this.codeFormElectro.value);
  }

  private createFormFields(): void {
    this.email = new FormControl("", [emailCorrectValidator]);
    this.phone = new FormControl("", [Validators.pattern("^[0-9 +-]$")]);
    this.code = new FormControl("", [Validators.required]);
    this.phoneElectro = new FormControl("", [Validators.required]);
    this.emailElectro = new FormControl("", [emailCorrectValidator]);
  }

  private createForm(): void {
    this.codeForm = new FormGroup({
      email: this.email,
      code: this.code,
      phone: this.phone,
    });

    this.codeFormElectro = new FormGroup({
      phoneElectro: this.phoneElectro,
      emailElectro: this.emailElectro,
    });
  }
}

interface FormCheckbox {
  name: string;
  count: number;
  checked: boolean;
  id: number;
}
