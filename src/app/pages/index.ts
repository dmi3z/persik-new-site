export { PrivateClientComponent } from './private-client/private-client.component';
export { OnlineComponent } from './online/online.component';
export { ChannelPlayerComponent } from './channel-player/channel-player.component';
export { VideoPlayerComponent } from './video-player/video-player.component';
export { SpeedTestPageComponent } from './speed-test/speed-test.component';
export { IptvPageComponent } from './iptv/iptv.component';
export { EmptyPageComponent } from './empty-page/empty-page.component';
export { RetailAuthPageComponent } from './retail-auth/retail-auth.component';
export { RetailMainPageComponent } from './retail-main/retail-main.component';
export { RetailInfoPageComponent } from './retail-info/retail-info.component';
export { RetailStatsPageComponent } from './retail-stats/retail-stats.component';
export { RetailAdminPageComponent } from './retail-admin/retail-admin.component';
export { RetailAdminActsPageComponent } from './retail-admin-acts/retail-admin-acts.component';

