import { NgModule } from '@angular/core';
import { RetailInfoPageComponent } from './retail-info.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    RetailInfoPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: RetailInfoPageComponent
      }
    ])
  ]
})

export class RetailInfoPageModule { }
