import { Component, OnInit } from '@angular/core';
import { MetaService } from './../../services';
import { meta } from '../metadata';

@Component({
  selector: 'retail-info-component',
  templateUrl: './retail-info.component.html',
  styleUrls: ['./retail-info.component.scss']
})

export class RetailInfoPageComponent implements OnInit {

  constructor(private metaService: MetaService) { }

  ngOnInit(): void {
    this.metaService.updateMeta(meta);
  }

}
