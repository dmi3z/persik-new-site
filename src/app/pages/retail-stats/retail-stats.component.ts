import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup } from '@angular/forms';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { MetaService } from './../../services';
import { meta } from '../metadata';

@Component({
  selector: 'retail-stats-component',
  templateUrl: './retail-stats.component.html',
  styleUrls: ['./retail-stats.component.scss']
})

export class RetailStatsPageComponent implements OnInit {

  public startTime: FormControl;
  public endTime: FormControl;

  public timeForm: FormGroup;

  constructor(private router: Router, private localeService: BsLocaleService, private metaService: MetaService) { }



  ngOnInit(): void {
    this.localeService.use('ru');
    this.createFormControls();
    this.createForm();
    this.metaService.updateMeta(meta);
  }

  private createFormControls(): void {
    this.startTime = new FormControl('');
    this.endTime = new FormControl('');
  }

  private createForm(): void {
    this.timeForm = new FormGroup({
      startTime: this.startTime,
      endTime: this.endTime
    });
  }


}
