import { NgModule } from '@angular/core';
import { RetailStatsPageComponent } from './retail-stats.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

@NgModule({
  declarations: [
    RetailStatsPageComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    BsDatepickerModule.forRoot(),
    RouterModule.forChild([
      {
        path: '',
        component: RetailStatsPageComponent
      }
    ])
  ]
})

export class RetailStatsPageModule { }
