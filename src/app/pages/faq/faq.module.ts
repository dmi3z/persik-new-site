import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FaqPageComponent } from './faq.component';

@NgModule({
  declarations: [
    FaqPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: FaqPageComponent
      }
    ])
  ]
})

export class FaqPageModule {}

