import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MetaService } from '../../services/meta.service';

@Component({
  selector: 'app-faq-component',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss']
})

export class FaqPageComponent implements OnInit {
    constructor(private router: Router, private metaService: MetaService) {}

    ngOnInit() {
      this.metaService.setDefaultMeta('Часто задаваемые вопросы');
    }

    public goTo(path: string, fragment?: string): void {
      if (fragment) {
        this.router.navigate([path], {fragment});
      } else {
        this.router.navigate([path]);
      }
    }
}
