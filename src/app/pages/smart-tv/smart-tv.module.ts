import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SmartTvPageComponent } from './smart-tv.component';

@NgModule({
  declarations: [
    SmartTvPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: SmartTvPageComponent
      }
    ])
  ]
})

export class SmartTvModule {}

