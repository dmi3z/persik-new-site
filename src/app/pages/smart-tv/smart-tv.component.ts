import { MetaService } from './../../services/meta.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-smart-tv-component',
  templateUrl: './smart-tv.component.html',
  styleUrls: ['./smart-tv.component.scss']
})

export class SmartTvPageComponent implements  OnInit {

    constructor(private router: Router, private metaService: MetaService) {}

    ngOnInit() {
      this.metaService.setDefaultMeta('Смарт ТВ');
    }

    public goTo(path: string, fragment?: string): void {
      if (fragment) {
        this.router.navigate([path], {fragment});
      } else {
        this.router.navigate([path]);
      }
    }
}
