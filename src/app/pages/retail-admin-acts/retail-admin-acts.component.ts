import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup } from '@angular/forms';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { MetaService } from './../../services';
import { meta } from '../metadata';

@Component({
    selector: 'retail-admin-acts-component',
    templateUrl: './retail-admin-acts.component.html',
    styleUrls: ['./retail-admin-acts.component.scss']
})

export class RetailAdminActsPageComponent implements OnInit {

    public startTime: FormControl;
    public endTime: FormControl;
    public acts: FormControl;
    public packages: FormControl;
    public status: FormControl;

    public timeForm: FormGroup;

    public actsDropdown: any = [
        {
            id: '1',
            name: 'Акт 1'
        },
        {
            id: '2',
            name: 'Акт 2'
        }
    ];
    public packagesDropdown: any = [
        {
            id: '1',
            name: 'Пакет 1'
        },
        {
            id: '2',
            name: 'Пакет 2'
        }
    ];
    public statusDropdown: any = [
        {
            id: '1',
            name: 'Продан'
        },
        {
            id: '2',
            name: 'Не продан'
        },
        {
            id: '2',
            name: 'Возврат'
        }
    ];

    constructor(private router: Router, private localeService: BsLocaleService, private metaService: MetaService) { }



    ngOnInit(): void {
        this.localeService.use('ru');
        this.createFormControls();
        this.createForm();
        this.metaService.updateMeta(meta);
    }

    private createFormControls(): void {
        this.startTime = new FormControl('');
        this.endTime = new FormControl('');
        this.acts = new FormControl('');
        this.packages = new FormControl('');
        this.status = new FormControl('');
    }

    private createForm(): void {
        this.timeForm = new FormGroup({
            startTime: this.startTime,
            endTime: this.endTime,
            acts: this.acts,
            packages: this.packages,
            status: this.status,
        });
    }

}