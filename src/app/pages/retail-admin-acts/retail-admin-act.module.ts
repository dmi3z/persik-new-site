import { NgSelectModule } from '@ng-select/ng-select';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { NgModule } from '@angular/core';
import { RetailAdminActsPageComponent } from './retail-admin-acts.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    RetailAdminActsPageComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    BsDatepickerModule,
    NgSelectModule,
    RouterModule.forChild([
      {
        path: '',
        component: RetailAdminActsPageComponent
      }
    ])
  ]
})

export class RetailAdminActsPageModule { }
