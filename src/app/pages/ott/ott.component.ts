import { MetaService } from './../../services/meta.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ott-component',
  templateUrl: './ott.component.html',
  styleUrls: ['./ott.component.scss']
})

export class OttPageComponent implements OnInit {
    constructor(private router: Router, private metaService: MetaService) {}

    ngOnInit() {
      this.metaService.setDefaultMeta('О технологии OTT');
    }

    public goTo(path: string): void {
      this.router.navigate([path]);
    }
}
