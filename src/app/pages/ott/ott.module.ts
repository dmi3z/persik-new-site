import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { OttPageComponent } from './ott.component';

@NgModule({
  declarations: [
    OttPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: OttPageComponent
      }
    ])
  ]
})

export class OttModule {}

