import { PlayerModule } from '../../components/player/player.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VideoPlayerComponent } from './';
import { PipesModule } from '../../pipes/pipes.module';
import { RouterModule } from '@angular/router';


@NgModule({
  declarations: [
    VideoPlayerComponent,
  ],
  imports: [
    CommonModule,
    PlayerModule,
    PipesModule,
    RouterModule.forChild([
      {
        path: '',
        component: VideoPlayerComponent
      }
    ])
  ]
})

export class VideoPlayerModule {}
