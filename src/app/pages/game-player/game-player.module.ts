import { NgModule } from "@angular/core";
import { GamePlayerComponent } from './game-player.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

@NgModule({
    declarations: [
        GamePlayerComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild([
            {
                path: '',
                component: GamePlayerComponent
            }
        ])
    ]
})

export class GamePlayerModule { }
