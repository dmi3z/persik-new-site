import { Component, OnInit, Renderer2, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Game } from '../online/games/games-page.component';
import { GAMES_DATA } from '../online/games/games-data';

@Component({
    selector: 'app-game-player',
    templateUrl: 'game-player.component.html',
    styleUrls: ['game-player.component.scss']
})

export class GamePlayerComponent implements OnInit, OnDestroy {

    private data: Game[] = GAMES_DATA;
    @ViewChild('area') gamearea: ElementRef;
    private iframe: HTMLIFrameElement;

    constructor(private activatedRoute: ActivatedRoute, private renderer: Renderer2) { }

    ngOnInit() {
        const gameId = this.activatedRoute.snapshot.params.id;
        const game = this.data.find(game => game.id === Number(gameId));
        if (game) {
            this.createIframe(game.url);
        }
    }

    private createIframe(url: string): void {
        this.iframe = this.renderer.createElement('iframe');
        this.renderer.addClass(this.iframe, 'gameframe');
        this.renderer.setAttribute(this.iframe, 'allow', 'fullscreen');
        this.renderer.setAttribute(this.iframe, 'src', url);
        this.renderer.appendChild(this.gamearea.nativeElement, this.iframe);
    }

    private removeIframe(): void {
        this.renderer.removeChild(this.gamearea.nativeElement, this.iframe);
    }

    ngOnDestroy() {
        this.removeIframe();
    }
}
