import { NgSelectModule } from '@ng-select/ng-select';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { NgModule } from '@angular/core';
import { RetailAdminPageComponent } from './retail-admin.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    RetailAdminPageComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    BsDatepickerModule,
    NgSelectModule,
    RouterModule.forChild([
      {
        path: '',
        component: RetailAdminPageComponent
      }
    ])
  ]
})

export class RetailAdminPageModule { }
