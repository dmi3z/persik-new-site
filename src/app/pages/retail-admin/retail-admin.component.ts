import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup } from '@angular/forms';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { MetaService } from './../../services';
import { meta } from '../metadata';

@Component({
  selector: 'retail-admin-component',
  templateUrl: './retail-admin.component.html',
  styleUrls: ['./retail-admin.component.scss']
})

export class RetailAdminPageComponent implements OnInit {

  public startTime: FormControl;
  public endTime: FormControl;
  public shops: FormControl;

  public timeForm: FormGroup;

  public shopsDropdown: any = [
    {
      id: '1',
      name: 'Магазин 1'
    },
    {
      id: '2',
      name: 'Магазин 2'
    }
  ];

  constructor(private router: Router, private localeService: BsLocaleService, private metaService: MetaService) { }



  ngOnInit(): void {
    this.localeService.use('ru');
    this.createFormControls();
    this.createForm();
    this.metaService.updateMeta(meta);
  }

  private createFormControls(): void {
    this.startTime = new FormControl('');
    this.endTime = new FormControl('');
    this.shops = new FormControl('');
  }

  private createForm(): void {
    this.timeForm = new FormGroup({
      startTime: this.startTime,
      endTime: this.endTime,
      shops: this.shops,
    });
  }

}