import { ParticlesModule } from 'angular-particle';
import { NgModule } from '@angular/core';
import { RetailAuthPageComponent } from './retail-auth.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    RetailAuthPageComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    ParticlesModule,
    RouterModule.forChild([
      {
        path: '',
        component: RetailAuthPageComponent
      }
    ])
  ]
})

export class RetailAuthPageModule { }
