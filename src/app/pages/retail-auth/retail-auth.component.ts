import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MetaService, AuthService } from './../../services';
import { meta } from '../metadata';

@Component({
  selector: 'app-retail-auth-component',
  templateUrl: './retail-auth.component.html',
  styleUrls: ['./retail-auth.component.scss']
})

export class RetailAuthPageComponent implements OnInit {

  public name: FormControl;
  public password: FormControl;

  public authForm: FormGroup;

  constructor(private router: Router, private metaService: MetaService, private authService: AuthService) { }

  myStyle: object = {
    'position': 'fixed',
    'width': '100%',
    'height': '100%',
    'z-index': 1,
    'top': 0,
    'left': 0,
    'right': 0,
    'bottom': 0,
  };

  myParams: object = {
    'particles': {
      'number': {
        'value': 80
      },
      'color': {
        'value': '#f9f3f4'
      },
      'shape': {
        'type': 'circle',
        'stroke': {
          'width': 1,
          'color': '#ccc'
        }
      },
      'opacity': {
        'value': 0.6,
        'random': false
      },
      'size': {
        'value': 1
      },
      'line_linked': {
        'enable': true,
        'distance': 110,
        'opacity': 0.4,
      },
      'move': {
        'enable': true,
        'speed': 0.3
      }
    }
  };
  public width = 100;
  public height = 100;

  ngOnInit(): void {
    this.createFormFields();
    this.createForm();
    this.metaService.updateMeta(meta);
  }

  public sendAuth(): void {
    if (this.authForm.valid) {
      this.authService.retailLogin(this.name.value, this.password.value).then(_ => {
        this.router.navigate(['retail/main']);
      });
    }
  }

  private createFormFields(): void {
    this.name = new FormControl('', [Validators.required]);
    this.password = new FormControl('', [Validators.required]);
  }

  private createForm(): void {
    this.authForm = new FormGroup({
      name: this.name,
      password: this.password
    });
  }

}
