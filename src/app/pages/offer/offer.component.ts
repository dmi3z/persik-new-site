import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MetaService } from '../../services/meta.service';

@Component({
  selector: 'app-offer-component',
  templateUrl: './offer.component.html',
  styleUrls: ['./offer.component.scss']
})

export class OfferPageComponent implements OnInit {
    constructor(private router: Router, private metaService: MetaService) {}

    ngOnInit() {
      this.metaService.setDefaultMeta('Пользовательское соглашение');
    }

    public goTo(path: string): void {
      this.router.navigate([path]);
    }
}
