import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { OfferPageComponent } from './offer.component';

@NgModule({
  declarations: [
    OfferPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: OfferPageComponent
      }
    ])
  ]
})

export class OfferPageModule {}

