import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TariffsRetailYearComponent } from './tariffs-retail-year.component';
import { FilmCardModule } from '../../../components/content-slider/film-card/film-card.module';

@NgModule({
  declarations: [
    TariffsRetailYearComponent
  ],
  imports: [
    CommonModule,
    FilmCardModule,
    RouterModule.forChild([
      {
        path: '',
        component: TariffsRetailYearComponent
      }
    ])
  ]
})

export class TariffsRetailYearPageModule { }
