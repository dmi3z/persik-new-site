import { PackageTariffOption } from './../../../models/user';
import { Package } from '../../../models';

export const packages: Package[] = [
  {
    id: 2,
    name: 'На год (скидка 50% + 1 год в подарок)',
    items: [
      {
        id: 176,
        name: ' Все ТВ 1 год + 1 год в подарок',
        cost: 39.9,
        cost_display: 39.9,
        customBigPrice: '39,9 руб/2 года',
        bigLinedText: '79.80',
        // smallLinedText: '5,8',
        customSmallPrice: '1.7 руб/мес.',
        // imageSrc: 'assets/images/promo/green.png',
        imageSrc: 'assets/images/promo/50-blue.png',
        paymentLinedText: true,
        period: 'На год'
      },
      {
        id: 248,
        name: 'Премиум 1 год + 1 год в подарок',
        cost: 64.95,
        cost_display: 64.95,
        customBigPrice: '64,95 руб/2 года',
        customSmallPrice: '2.7 руб/мес.',
        bigLinedText: '129.9',
        // smallLinedText: '10,8',
        // imageSrc: 'assets/images/promo/50-red.png',
        imageSrc: 'assets/images/promo/50-red.png',
        paymentLinedText: true,
        period: 'На год'
      },
    ]
  }
];


export const options: PackageTariffOption[] = [
  {
    id: 126,
    states: [
      {
        name: '100+ каналов',
        value: true
      },
      {
        name: 'Full HD качество',
        value: true
      },
      {
        name: 'UHD качество',
        value: false
      },
      {
        name: 'Видеотека Persik',
        value: true
      },
      {
        name: '10 000+ кино',
        value: true
      },
      {
        name: 'Архив 24 дня',
        value: true
      },
      {
        name: 'Мультирум',
        value: true
      }
    ]
  },
  {
    id: 168,
    states: [
      {
        name: '100+ каналов',
        value: true
      },
      {
        name: 'Full HD качество',
        value: true
      },
      {
        name: 'UHD качество',
        value: false
      },
      {
        name: 'Видеотека Persik',
        value: true
      },
      {
        name: '10 000+ кино',
        value: true
      },
      {
        name: 'Архив 24 дня',
        value: true
      },
      {
        name: 'Мультирум',
        value: true
      }
    ]
  },
  {
    id: 180,
    states: [
      {
        name: '100+ каналов',
        value: true
      },
      {
        name: 'Full HD качество',
        value: true
      },
      {
        name: 'UHD качество',
        value: true
      },
      {
        name: 'Видеотека Persik',
        value: true
      },
      {
        name: '10 000+ кино',
        value: true
      },
      {
        name: 'Архив 24 дня',
        value: true
      },
      {
        name: 'Мультирум',
        value: true
      }
    ]
  }, // На год с помесячной
  {
    id: 192,
    states: [
      {
        name: '100+ каналов',
        value: true
      },
      {
        name: 'Full HD качество',
        value: true
      },
      {
        name: 'UHD качество',
        value: false
      },
      {
        name: 'Видеотека Persik',
        value: true
      },
      {
        name: '10 000+ кино',
        value: true
      },
      {
        name: 'Архив 24 дня',
        value: true
      },
      {
        name: 'Мультирум',
        value: true
      }
    ]
  },
  {
    id: 193,
    states: [
      {
        name: '100+ каналов',
        value: true
      },
      {
        name: 'Full HD качество',
        value: true
      },
      {
        name: 'UHD качество',
        value: true
      },
      {
        name: 'Видеотека Persik',
        value: true
      },
      {
        name: '10 000+ кино',
        value: true
      },
      {
        name: 'Архив 24 дня',
        value: true
      },
      {
        name: 'Мультирум',
        value: true
      }
    ]
  }, // На год
  {
    id: 50,
    states: [
      {
        name: '70 каналов',
        value: true
      },
      {
        name: 'Full HD качество',
        value: false
      },
      {
        name: 'UHD качество',
        value: false
      },
      {
        name: 'Видеотека Persik',
        value: false
      },
      {
        name: '10 000+ кино',
        value: true
      },
      {
        name: 'Архив 24 дня',
        value: true
      },
      {
        name: 'Мультирум',
        value: false
      }
    ]
  },
  {
    id: 226,
    states: [
      {
        name: '70 каналов',
        value: true
      },
      {
        name: 'Full HD качество',
        value: false
      },
      {
        name: 'UHD качество',
        value: false
      },
      {
        name: 'Видеотека Persik',
        value: false
      },
      {
        name: '10 000+ кино',
        value: true
      },
      {
        name: 'Архив 24 дня',
        value: true
      },
      {
        name: 'Мультирум',
        value: false
      }
    ]
  },
  /* {
    id: 50,
    states: [
      {
        name: '70 каналов',
        value: true
      },
      {
        name: 'Full HD качество',
        value: false
      },
      {
        name: 'UHD качество',
        value: false
      },
      {
        name: 'Видеотека Persik',
        value: false
      },
{
name: '10 000+ кино',
value: true
},
      {
        name: 'Кино START',
        value: false
      },
      {
        name: 'Архив 24 дня',
        value: true
      },
      {
        name: 'Мультирум',
        value: false
      }
    ]
  }, */
  {
    id: 176,
    states: [
      {
        name: '100+ каналов',
        value: true
      },
      {
        name: 'Full HD качество',
        value: true
      },
      {
        name: 'UHD качество',
        value: false
      },
      {
        name: 'Видеотека Persik',
        value: true
      },
      {
        name: '10 000+ кино',
        value: true
      },
      {
        name: 'Архив 24 дня',
        value: true
      },
      {
        name: 'Мультирум',
        value: true
      }
    ]
  },
  {
    id: 197,
    states: [
      {
        name: '100+ каналов',
        value: true
      },
      {
        name: 'Full HD качество',
        value: true
      },
      {
        name: 'UHD качество',
        value: false
      },
      {
        name: 'Видеотека Persik',
        value: true
      },
      {
        name: '10 000+ кино',
        value: true
      },
      {
        name: 'Архив 24 дня',
        value: true
      },
      {
        name: 'Мультирум',
        value: true
      }
    ]
  },

  {
    id: 203,
    states: [
      {
        name: '100+ каналов',
        value: true
      },
      {
        name: 'Full HD качество',
        value: true
      },
      {
        name: 'UHD качество',
        value: false
      },
      {
        name: 'Видеотека Persik',
        value: true
      },
      {
        name: '10 000+ кино',
        value: true
      },
      {
        name: 'Архив 24 дня',
        value: true
      },
      {
        name: 'Мультирум',
        value: true
      }
    ]
  },
  {
    id: 181,
    states: [
      {
        name: '100+ каналов',
        value: true
      },
      {
        name: 'Full HD качество',
        value: true
      },
      {
        name: 'UHD качество',
        value: false
      },
      {
        name: 'Видеотека Persik',
        value: true
      },
      {
        name: '10 000+ кино',
        value: true
      },
      {
        name: 'Архив 24 дня',
        value: true
      },
      {
        name: 'Мультирум',
        value: true
      }
    ]
  },
  {
    id: 248,
    states: [
      {
        name: '100+ каналов',
        value: true
      },
      {
        name: 'Full HD качество',
        value: true
      },
      {
        name: 'UHD качество',
        value: true
      },
      {
        name: 'Видеотека Persik',
        value: true
      },
      {
        name: '10 000+ кино',
        value: true
      },
      {
        name: 'Архив 24 дня',
        value: true
      },
      {
        name: 'Мультирум',
        value: true
      }
    ]
  },
  {
    id: 213,
    states: [
      {
        name: '100+ каналов',
        value: true
      },
      {
        name: 'Full HD качество',
        value: true
      },
      {
        name: 'UHD качество',
        value: true
      },
      {
        name: 'Видеотека Persik',
        value: true
      },
      {
        name: '10 000+ кино',
        value: true
      },
      {
        name: 'Архив 24 дня',
        value: true
      },
      {
        name: 'Мультирум',
        value: true
      }
    ]
  }, // Year + Android
  {
    id: 188,
    states: [
      {
        name: '70 каналов',
        value: true
      },
      {
        name: 'Full HD качество',
        value: false
      },
      {
        name: 'UHD качество',
        value: false
      },
      {
        name: 'Видеотека Persik',
        value: false
      },
      {
        name: '10 000+ кино',
        value: true
      },
      {
        name: 'Архив 24 дня',
        value: true
      },
      {
        name: 'Мультирум',
        value: false
      }
    ]
  },
  {
    id: 189,
    states: [
      {
        name: '100+ каналов',
        value: true
      },
      {
        name: 'Full HD качество',
        value: true
      },
      {
        name: 'UHD качество',
        value: false
      },
      {
        name: 'Видеотека Persik',
        value: true
      },
      {
        name: '10 000+ кино',
        value: true
      },
      {
        name: 'Архив 24 дня',
        value: true
      },
      {
        name: 'Мультирум',
        value: true
      }
    ]
  },
  {
    id: 190,
    states: [
      {
        name: '100+ каналов',
        value: true
      },
      {
        name: 'Full HD качество',
        value: true
      },
      {
        name: 'UHD качество',
        value: false
      },
      {
        name: 'Видеотека Persik',
        value: true
      },
      {
        name: '10 000+ кино',
        value: true
      },
      {
        name: 'Архив 24 дня',
        value: true
      },
      {
        name: 'Мультирум',
        value: true
      }
    ]
  },
  {
    id: 191,
    states: [
      {
        name: '100+ каналов',
        value: true
      },
      {
        name: 'Full HD качество',
        value: true
      },
      {
        name: 'UHD качество',
        value: true
      },
      {
        name: 'Видеотека Persik',
        value: true
      },
      {
        name: '10 000+ кино',
        value: true
      },
      {
        name: 'Архив 24 дня',
        value: true
      },
      {
        name: 'Мультирум',
        value: true
      }
    ]
  },
  // films to month
  {
    id: 184,
    states: []
  },
  {
    id: 171,
    states: []
  },
  {
    id: 179,
    states: []
  }
];
