import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PublishMediaContentPageComponent } from './publish-media-content.component';

@NgModule({
  declarations: [
    PublishMediaContentPageComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      {
        path: '',
        component: PublishMediaContentPageComponent
      }
    ])
  ]
})

export class PublishMediaContentPageModule {}
