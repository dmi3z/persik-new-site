import { MetaDefinition } from '@angular/platform-browser';
import { MetaData } from '../../../models/meta';

const tags: MetaDefinition[] = [
    {
      name: 'keywords',
      content: 'партнерская программа с авторами медиаконтента'
    },
    {
      name: 'description',
      content: `Сотрудничество с авторами и владельцами медиаконтента, контентмейкерами - телевидение Персик.`
    }
  ];

const title = 'Сотрудничество с авторами и владельцами медиаконтента';

export const meta: MetaData = {
  title,
  tags
};


