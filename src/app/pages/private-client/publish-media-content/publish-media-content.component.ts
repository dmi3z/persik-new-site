import { DataService, ModalController, MetaService } from './../../../services';
import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { emailCorrectValidator } from '../../../validators';
import { ModalName } from '../../../models';
import { meta } from './metadata';

@Component({
  selector: 'app-publish-online-courses-component',
  templateUrl: './publish-media-content.component.html',
  styleUrls: ['./publish-media-content.component.scss']
})

export class PublishMediaContentPageComponent implements OnInit {
  public email: FormControl;
  public name: FormControl;
  public phone: FormControl;
  public message: FormControl;
  private page: FormControl;

  public userForm: FormGroup;

  constructor(
    private dataService: DataService,
    private modalCtrl: ModalController,
    private metaService: MetaService
  ) {}

  ngOnInit() {
    this.metaService.updateMeta(meta);
    this.createFormFields();
    this.createForm();
  }

  private createFormFields(): void {
    this.email = new FormControl('', [emailCorrectValidator]);
    this.phone = new FormControl('', [Validators.pattern('^[0-9 +-]*$')]);
    this.name = new FormControl('', [Validators.required]);
    this.message = new FormControl('');
    this.page = new FormControl('publish-media-content');
  }

  private createForm(): void {
    this.userForm = new FormGroup({
      name: this.name,
      email: this.email,
      phone: this.phone,
      message: this.message,
      page: this.page
    });
  }

  public sendData(): void {
    if (this.userForm.valid) {
      this.dataService.sendPublishFormData(this.userForm.value).then(() => {
        this.modalCtrl.present(ModalName.SEND_COMPLETE);
        this.userForm.reset();
      }).catch(() => {
        this.modalCtrl.present(ModalName.SEND_ERROR);
      });
    }
  }


}
