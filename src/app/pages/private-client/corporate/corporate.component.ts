import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { emailCorrectValidator } from '../../../validators';
import { MetaService, ModalController } from '../../../services';
import { DataService } from './../../../services/data.service';
import { meta } from './metadata';
import { ModalName } from '../../../models';

@Component({
  selector: 'app-corporate-client',
  templateUrl: './corporate.component.html',
  styleUrls: ['./corporate.component.scss']
})

export class CorporateClientComponent implements OnInit {

  public corporateForm: FormGroup;

  public corporateFormName: FormControl;
  public corporateFormBusiness: FormControl;
  public corporateFormEmail: FormControl;
  public corporateFormPhone: FormControl;
  public corporateFormPage: FormControl;

  public bottomForm: FormGroup;

  public name: FormControl;
  public business: FormControl;
  public email: FormControl;
  public phone: FormControl;
  public message: FormControl;

  private page: FormControl;

  constructor(
    private dataService: DataService,
    private metaService: MetaService,
    private modalCtrl: ModalController
  ) { }


  ngOnInit() {
    this.metaService.updateMeta(meta);
    this.createFormFields();
    this.createForm();
  }

  private createFormFields(): void {
    this.corporateFormEmail = new FormControl('', [emailCorrectValidator]);
    this.corporateFormPhone = new FormControl('', [Validators.required, Validators.pattern('^[0-9 +-]*$')]);
    this.corporateFormName = new FormControl('', []);
    this.corporateFormBusiness = new FormControl('', []);
    this.corporateFormPage = new FormControl('korporativnym-klientam');

    this.name = new FormControl('', []);
    this.business = new FormControl('', []);
    this.message = new FormControl('', []);
    this.email = new FormControl('', [emailCorrectValidator]);
    this.phone = new FormControl('', [Validators.required, Validators.pattern('^[0-9 +-]*$')]);
    this.page = new FormControl('korporativnym-klientam');
  }

  private createForm(): void {
    this.corporateForm = new FormGroup({
      corporateFormEmail: this.corporateFormEmail,
      corporateFormBusiness: this.corporateFormBusiness,
      corporateFormPhone: this.corporateFormPhone,
      corporateFormName: this.corporateFormName,
      corporateFormPage: this.corporateFormPage
    });

    this.bottomForm = new FormGroup({
      name: this.name,
      business: this.business,
      message: this.message,
      email: this.email,
      phone: this.phone,
      page: this.page
    });
  }

  public sendData(): void {

  }

  public sendCode(): void {
    const formData = {
      name: this.corporateForm.value.corporateFormName,
      message: this.corporateForm.value.corporateFormBusiness,
      phone: this.corporateForm.value.corporateFormPhone,
      email: this.corporateForm.value.corporateFormEmail,
      page: this.corporateForm.value.corporateFormPage
    }
    if (this.corporateForm.valid) {
      this.dataService.sendPublishFormData(formData).then(() => {
        this.modalCtrl.present(ModalName.SEND_COMPLETE);
        this.corporateForm.reset();
      }).catch(() => {
        this.modalCtrl.present(ModalName.SEND_ERROR);
      });
    }
  }

  public sendBottomForm(): void {
    if (this.bottomForm.valid) {
      this.dataService.sendPublishFormData(this.bottomForm.value).then(() => {
        this.modalCtrl.present(ModalName.SEND_COMPLETE);
        this.bottomForm.reset();
      }).catch(() => {
        this.modalCtrl.present(ModalName.SEND_ERROR);
      });
    }
  }

  public scrollToBlock(id): void {
    try {
      document.querySelector('#' + id).scrollIntoView({ block: 'center', behavior: 'smooth' });
    } catch (e) { }
  }
}
