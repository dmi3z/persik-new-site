import { MetaDefinition } from '@angular/platform-browser';
import { MetaData } from '../../../models/meta';

const tags: MetaDefinition[] = [
    {
      name: 'keywords',
      content: 'корпоративным клиентам'
    },
    {
      name: 'description',
      content: `Persik TV предлагает организацию телевидения для юридических лиц в гостиницах, санаториях, офисах, ресторанах, кафе. Размещение промо-канала заведения, организация онлайн трансляций. Техническое обслуживание IP-сетей, кабельных сетей, облачная система видеонаблюдения.`
    }
  ];

const title = ' Интернет-телевидение (IPTV) для бизнеса - Persik TV';

export const meta: MetaData = {
  title,
  tags
};

