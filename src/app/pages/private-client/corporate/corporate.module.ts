import { CorporateClientComponent } from './corporate.component';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';


@NgModule({
  declarations: [
    CorporateClientComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: CorporateClientComponent
      }
    ]),
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
})

export class CorporateModule { }
