import { AuthService, MetaService } from './../../../services';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { emailCorrectValidator } from '../../../validators';
import { FeedbackData } from '../../../models/user';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss']
})

export class ContactUsComponent implements OnInit {

  public name: FormControl;
  public subject: FormControl;
  public email: FormControl;
  public phone: FormControl;
  public message: FormControl;
  public feedbackForm: FormGroup;

  public isShowModal: boolean;

  constructor(private authService: AuthService, private metaService: MetaService) {}

  ngOnInit() {
    this.metaService.setDefaultMeta('Контакты');
    this.createFormFields();
    this.createForm();
  }

  public sendFeedback(): void {
    const data = <FeedbackData>this.feedbackForm.value;
    this.authService.sendFeedback(data).then(() => {
      this.feedbackForm.reset();
      this.isShowModal = true;
    });
  }

  public stopPropagation(event): void {
    event.stopPropagation();
  }

  public closeModal(): void {
    this.isShowModal = false;
  }

  private createFormFields(): void {
    this.name = new FormControl('', [Validators.required]);
    this.subject = new FormControl('', [Validators.required]);
    this.email = new FormControl('', [Validators.required, emailCorrectValidator]);
    this.phone = new FormControl('', [Validators.required, Validators.pattern('^[0-9 +-]*$')]);
    this.message = new FormControl('', [Validators.required]);
  }

  private createForm(): void {
    this.feedbackForm = new FormGroup({
      name: this.name,
      email: this.email,
      subject: this.subject,
      phone: this.phone,
      message: this.message
    });
  }
}
