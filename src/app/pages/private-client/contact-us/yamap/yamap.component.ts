import { Component, OnInit, OnDestroy } from '@angular/core';
import { CheckWidthService } from '../../../../services';
import { Subscription } from 'rxjs';

declare var ymaps: any;

@Component({
  selector: 'app-yamap-component',
  templateUrl: './yamap.component.html',
  styleUrls: ['./yamap.component.scss']
})
export class YaMapComponent implements OnInit, OnDestroy {
  public map: any;
  private windowWidth: number;

  private myPlaceMark: any;
  private checkWindowWidthSubscriber: Subscription;

  constructor(private checkWindowService: CheckWidthService) {}

  ngOnInit(): void {
    this.windowWidth = this.checkWindowService.getWidth();
    this.checkWindowWidthSubscriber = this.checkWindowService.screenWidthEvent.subscribe(
      width => {
        this.windowWidth = width;
      }
    );

    ymaps.ready().then(() => {
      (this.map = new ymaps.Map('map', {
        center: [53.93514057063716, 27.494690999999968],
        controls: [],
        zoom: 17
      })),
        (this.myPlaceMark = new ymaps.Placemark(
          this.map.getCenter(),
          {},
          {
            iconLayout: 'default#image',
            iconImageHref: 'assets/images/icons/map-marker.png',
            iconImageSize: [45, 55],
            iconImageOffset: [-22, -55]
          }
        ));

      if (this.windowWidth <= 767) {
        this.map.behaviors.disable('drag');
      }
      this.map.geoObjects.add(this.myPlaceMark);
    });
  }

  ngOnDestroy() {
    if (this.checkWindowWidthSubscriber) {
      this.checkWindowWidthSubscriber.unsubscribe();
    }
  }
}
