import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

import {
  ContactUsComponent,
  YaMapComponent
} from './';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    ContactUsComponent,
    YaMapComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      {
        path: '',
        component: ContactUsComponent
      }
    ])
  ]
})

export class ContactUsModule {}
