import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SlickModule } from 'ngx-slick';
import { ContentSliderModule } from '../../components/content-slider/content-slider.module';
import { AccountModule } from './account/account.module';
import { MainChannelCardModule } from '../../components/main-channel-card/main-channel-card.module';
import { VideobannerComponentModule } from '../../components/videobanner-slider/videobanner-slider.module';

import {
  PrivateClientComponent,
  NotFoundPageComponent
} from './';
import { privateClientRoutes } from './private-client-routes';


@NgModule({
  declarations: [
    PrivateClientComponent,
    NotFoundPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    SlickModule.forRoot(),
    ContentSliderModule,
    AccountModule,
    VideobannerComponentModule,
    MainChannelCardModule,
    RouterModule.forChild([
      {
        path: '',
        component: PrivateClientComponent,
        children: privateClientRoutes
      }
    ])
  ],
  exports: [
    VideobannerComponentModule
  ]
})

export class PrivateClientModule {}
