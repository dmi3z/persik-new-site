import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TariffsRetailMonthComponent } from './tariffs-retail-month.component';
import { FilmCardModule } from '../../../components/content-slider/film-card/film-card.module';

@NgModule({
  declarations: [
    TariffsRetailMonthComponent
  ],
  imports: [
    CommonModule,
    FilmCardModule,
    RouterModule.forChild([
      {
        path: '',
        component: TariffsRetailMonthComponent
      }
    ])
  ]
})

export class TariffsRetailMonthPageModule { }
