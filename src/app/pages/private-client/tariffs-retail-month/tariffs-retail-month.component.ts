import { Option, Product } from '../../../models/tariff';
import { Subscription } from 'rxjs';
import { RatingFilter, Channel } from './../../../models';
import { Component, OnInit, HostListener, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { CheckWidthService, PaymentService, DataService, MetaService, AnalyticService } from '../../../services';
import { meta } from '../tariffs/metadata';

@Component({
  selector: 'app-tariffs-retail',
  templateUrl: './tariffs-retail-month.component.html',
  styleUrls: ['../tariffs/tariffs.component.scss']
})

export class TariffsRetailMonthComponent implements OnInit, OnDestroy {

  public isCompare = false;

  public packages: Product[];
  public videos: any[] = [];
  public tvshows: any[] = [];
  public allChannelLogos: string[] = [];
  public channelLogosForPage: string[] = [];
  public isHaveMovement: boolean;

  private activePackageId: number;
  public selectedTariff: Option;
  private videoSkip = 0;
  private tvshowSkip = 0;

  private windowWidth: number;
  public channelLogosCount = 40;
  private videoCardsCount = 12;

  public mobileTariffsPackOpen = false;
  public mobileTariffsOpen = false;

  private activatedRouteSubscriber: Subscription;
  private checkWidthSubscriber: Subscription;

  constructor(
    private dataService: DataService,
    private router: Router,
    private paymentService: PaymentService,
    private checkWindowService: CheckWidthService,
    private metaService: MetaService,
    private analyticService: AnalyticService
  ) {
    this.metaService.updateMeta(meta);
  }

  @HostListener('window:scroll', ['$event'])
  checkScroll() {
    const scrollPosition = window.pageYOffset;
    if (scrollPosition >= 720) {
      this.isHaveMovement = true;
    } else {
      this.isHaveMovement = false;
    }
  }

  ngOnInit(): void {
    this.dataService.getTariffs().subscribe(tariffs => this.packages = tariffs.products);
    this.windowWidth = this.checkWindowService.getWidth();
    this.calcChannels(this.windowWidth);
    this.loadChannelLogos(this.channelLogosCount);
    this.loadVideoFeatures(this.videoCardsCount);
    this.loadTvshowsFeatures(this.videoCardsCount);
    this.checkWidthSubscriber = this.checkWindowService.screenWidthEvent.subscribe(width => {
      this.calcChannels(width);
    });
    this.setActivePackage(this.packages[0].product_id);
  }

  private calcChannels(width): void {

    switch (true) {
      case width > 1600:
        this.channelLogosCount = 40;
        this.channelLogosForPage = this.allChannelLogos.slice(0, this.channelLogosCount);
        this.videoCardsCount = 12;
        break;

      case width <= 1600 && width >= 1401:
        this.channelLogosCount = 32;
        this.channelLogosForPage = this.allChannelLogos.slice(0, this.channelLogosCount);
        this.videoCardsCount = 10;
        break;

      case width <= 1400 && width >= 1201:
        this.channelLogosCount = 24;
        this.channelLogosForPage = this.allChannelLogos.slice(0, this.channelLogosCount);
        this.videoCardsCount = 8;
        break;

      case width <= 1200 && width >= 993:
        this.channelLogosCount = 20;
        this.channelLogosForPage = this.allChannelLogos.slice(0, this.channelLogosCount);
        this.videoCardsCount = 6;
        break;

      case width <= 992 && width >= 768:
        this.channelLogosCount = 16;
        this.channelLogosForPage = this.allChannelLogos.slice(0, this.channelLogosCount);
        break;

      case width <= 767:
        this.channelLogosCount = 12;
        this.channelLogosForPage = this.allChannelLogos.slice(0, this.channelLogosCount);
        break;

      default:
        this.channelLogosCount = 40;
        this.channelLogosForPage = this.allChannelLogos.slice(0, this.channelLogosCount);
        this.videoCardsCount = 12;
        break;
    }
  }

  public toggleMobileTariffsPack(): void {
    this.mobileTariffsPackOpen = !this.mobileTariffsPackOpen;
  }

  public toggleMobileTariffs(): void {
    this.mobileTariffsOpen = !this.mobileTariffsOpen;
  }

  public get isCanCompare(): boolean {
    return this.activePackageId !== 4;
  }

  public toggleCompare(): void {
    this.isCompare = !this.isCompare;
    setTimeout(() => {
      try {
        document.querySelector('#tariffs').scrollIntoView({ block: 'center', behavior: 'smooth' });
      } catch (e) { }
    }, 100);
  }

  public get activePackage(): Option[] {
    return this.packages.find(t => t.product_id === this.activePackageId).options;
  }

  public get magicActivePackage(): boolean {
    return this.activePackageId === 2;
  }

  public isActivePackage(id: number): boolean {
    return this.activePackageId === id;
  }

  // public getTariffOptions(tariff_id: number): PackageTariffOption {
  //   return this.tariffOptions.find(option => option.id === tariff_id);
  // }

  public setActivePackage(id: number): void {
    this.activePackageId = id;
    if (!this.isCanCompare) {
      this.isCompare = false;
    }
    const activePackage: Product = this.packages.find(pack => pack.product_id === id);
    if (id === 4) {
      this.selectedTariff = activePackage.options[0];
    } else if (id === 2) {
      this.selectedTariff = activePackage.options[7];
    } else {
      this.selectedTariff = activePackage.options[activePackage.options.length - 1];
    }
  }

  public selectTariff(tariff: Option): void {
    this.selectedTariff = tariff;
  }

  public isTariffSelected(tariff_id: number): boolean {
    if (this.selectedTariff) {
      return this.selectedTariff.product_option_id === tariff_id;
    }
    return false;
  }

  public get isHaveSelectedTariff(): boolean {
    if (this.selectedTariff) {
      return true;
    }
    return false;
  }

  public getId(item: any): number | string {
    if (item.video_id) {
      return item.video_id;
    }
    if (item.tvshow_id) {
      return item.tvshow_id;
    }
    return null;
  }

  public get isHaveVideos(): boolean {
    return this.videos.length > 0;
  }

  public get isHaveTvshows(): boolean {
    return this.tvshows.length > 0;
  }

  public get isShowAllChannels(): boolean {
    return (this.channelLogosForPage.length === this.allChannelLogos.length && this.channelLogosForPage.length > 0);
  }

  public toggleChannels(channelsCount): void {
    if (this.isShowAllChannels) {
      this.channelLogosForPage = this.allChannelLogos.slice(0, channelsCount);
    } else {
      this.channelLogosForPage = this.allChannelLogos;
    }
  }

  public goToPay(event: MouseEvent): void {
    this.analyticService.targetSend('vybrat', 47723698);
    event.stopPropagation();
    const activePack: Product = this.packages.find(pack => pack.product_id === this.activePackageId);
    this.paymentService.setTariff(this.selectedTariff, activePack.name);
    this.router.navigate(['private/payment']);
  }

  public showMoreVideos(): void {
    this.videoSkip += 12;
    this.loadVideoFeatures(this.videoCardsCount);
  }

  public showMoreTvshows(): void {
    this.tvshowSkip += 12;
    this.loadTvshowsFeatures(this.videoCardsCount);
  }

  private loadVideoFeatures(videoCardsCount): void {
    const category_id = 1;
    const genre_id = 881; // Кино СТАРТ
    const ratingFilter: RatingFilter = {
      id: 0,
      name: '',
      value: '' // year
    };
    this.dataService.getVideoContent(category_id, genre_id, videoCardsCount, this.videoSkip, null, ratingFilter).then(res => {
      if (res.videos.length > 0) {
        this.videos.push(...res.videos);
      }
    });
  }

  private loadTvshowsFeatures(videoCardsCount): void {
    const category_id = 4;
    const genre_id = 0;
    this.dataService.getVideoContent(category_id, genre_id, videoCardsCount, this.tvshowSkip).then(res => {
      this.tvshows.push(...res.videos);
    });
  }

  private loadChannelLogos(channelsCount): void {
    this.dataService.getChannels().then(channels => {
      const excludesChannelsId = [111, 115, 118, 148, 883, 10431, 10200];
      const withoutTourism: Channel[] = channels.filter(channel => !channel.genres.includes(680));
      // const withoutHd: Channel[] = withoutTourism.filter(channel => !channel.genres.includes(681));
      const finallyFilter: Channel[] = withoutTourism.filter(channel => !excludesChannelsId.includes(channel.channel_id));
      this.allChannelLogos = finallyFilter.map(channel => channel.logo);
      this.channelLogosForPage = this.allChannelLogos.slice(0, channelsCount);
    });
  }

  ngOnDestroy() {
    if (this.checkWidthSubscriber) {
      this.checkWidthSubscriber.unsubscribe();
    }
    if (this.activatedRouteSubscriber) {
      this.activatedRouteSubscriber.unsubscribe();
    }
  }

}

