import { PackageTariffOption } from './../../../models/user';
import { Package } from '../../../models';

export const packages: Package[] = [
  {
    id: 1,
    name: '30 дней за 30 копеек',
    items: [
      {
        id: 192,
        name: 'Все включено ТВ',
        cost: 4.9,
        first_month: 0.3,
        cost_display: 0.3,
        pay_avaliable: ['assist'],
        customBigPrice: 'Подключить тест на 1 месяц',
        smallLinedText: '7,9',
        paymentCondition: 'После тестового периода услуга подключается на 1 год с ежемесячной оплатой',
        period: '30 дней за 30 копеек, с возможностью отключиться от услуги в течение тестового периода'
      },
      {
        id: 193,
        name: 'Премиум',
        cost: 8.9,
        first_month: 0.3,
        cost_display: 0.3,
        pay_avaliable: ['assist'],
        customBigPrice: 'Подключить тест на 1 месяц',
        smallLinedText: '12,9',
        paymentCondition: 'После тестового периода услуга подключается на 1 год с ежемесячной оплатой',
        period: '30 дней за 30 копеек, с возможностью отключиться от услуги в течение тестового периода'
      }
    ]
  }
];


export const options: PackageTariffOption[] = [
  { // На месяц
    id: 169,
    states: [
      {
        name: '70 каналов',
        value: true
      },
      {
        name: 'Full HD качество',
        value: false
      },
      {
        name: 'UHD качество',
        value: false
      },
      {
        name: 'Видеотека Persik',
        value: false
      },
      {
        name: '10 000+ кино',
        value: true
      },
      {
        name: 'Архив 24 дня',
        value: true
      },
      {
        name: 'Мультирум',
        value: false
      }
    ]
  },
  {
    id: 126,
    states: [
      {
        name: '100+ каналов',
        value: true
      },
      {
        name: 'Full HD качество',
        value: true
      },
      {
        name: 'UHD качество',
        value: false
      },
      {
        name: 'Видеотека Persik',
        value: true
      },
      {
        name: '10 000+ кино',
        value: true
      },
      {
        name: 'Архив 24 дня',
        value: true
      },
      {
        name: 'Мультирум',
        value: true
      }
    ]
  },
  {
    id: 168,
    states: [
      {
        name: '100+ каналов',
        value: true
      },
      {
        name: 'Full HD качество',
        value: true
      },
      {
        name: 'UHD качество',
        value: false
      },
      {
        name: 'Видеотека Persik',
        value: true
      },
      {
        name: '10 000+ кино',
        value: true
      },
      {
        name: 'Архив 24 дня',
        value: true
      },
      {
        name: 'Мультирум',
        value: true
      }
    ]
  },
  {
    id: 180,
    states: [
      {
        name: '100+ каналов',
        value: true
      },
      {
        name: 'Full HD качество',
        value: true
      },
      {
        name: 'UHD качество',
        value: true
      },
      {
        name: 'Видеотека Persik',
        value: true
      },
      {
        name: 'Кино tvzavr',
        value: true
      },
      {
        name: 'Архив 24 дня',
        value: true
      },
      {
        name: 'Мультирум',
        value: true
      }
    ]
  }, // На год с помесячной
  {
    id: 192,
    states: [
      {
        name: '100+ каналов',
        value: true
      },
      {
        name: 'Full HD качество',
        value: true
      },
      {
        name: 'UHD качество',
        value: false
      },
      {
        name: 'Видеотека Persik',
        value: true
      },
      {
        name: '10 000+ кино',
        value: false
      },
      {
        name: 'Архив 24 дня',
        value: true
      },
      {
        name: 'Мультирум',
        value: true
      }
    ]
  },
  {
    id: 193,
    states: [
      {
        name: '100+ каналов',
        value: true
      },
      {
        name: 'Full HD качество',
        value: true
      },
      {
        name: 'UHD качество',
        value: true
      },
      {
        name: 'Видеотека Persik',
        value: true
      },
      {
        name: '10 000+ кино',
        value: true
      },
      {
        name: 'Архив 24 дня',
        value: true
      },
      {
        name: 'Мультирум',
        value: true
      }
    ]
  }, // На год
  {
    id: 50,
    states: [
      {
        name: '70 каналов',
        value: true
      },
      {
        name: 'Full HD качество',
        value: false
      },
      {
        name: 'UHD качество',
        value: false
      },
      {
        name: 'Видеотека Persik',
        value: false
      },
      {
        name: '10 000+ кино',
        value: true
      },
      {
        name: 'Архив 24 дня',
        value: true
      },
      {
        name: 'Мультирум',
        value: false
      }
    ]
  },
  {
    id: 226,
    states: [
      {
        name: '70 каналов',
        value: true
      },
      {
        name: 'Full HD качество',
        value: false
      },
      {
        name: 'UHD качество',
        value: false
      },
      {
        name: 'Видеотека Persik',
        value: false
      },
      {
        name: '10 000+ кино',
        value: true
      },
      {
        name: 'Архив 24 дня',
        value: true
      },
      {
        name: 'Мультирум',
        value: false
      }
    ]
  },
  /* {
    id: 50,
    states: [
      {
        name: '70 каналов',
        value: true
      },
      {
        name: 'Full HD качество',
        value: false
      },
      {
        name: 'UHD качество',
        value: false
      },
      {
        name: 'Видеотека Persik',
        value: false
      },
{
name: '10 000+ кино',
value: true
},
      {
        name: 'Архив 24 дня',
        value: true
      },
      {
        name: 'Мультирум',
        value: false
      }
    ]
  }, */
  {
    id: 176,
    states: [
      {
        name: '100+ каналов',
        value: true
      },
      {
        name: 'Full HD качество',
        value: true
      },
      {
        name: 'UHD качество',
        value: false
      },
      {
        name: 'Видеотека Persik',
        value: true
      },
      {
        name: '10 000+ кино',
        value: true
      },
      {
        name: 'Архив 24 дня',
        value: true
      },
      {
        name: 'Мультирум',
        value: true
      }
    ]
  },
  {
    id: 197,
    states: [
      {
        name: '100+ каналов',
        value: true
      },
      {
        name: 'Full HD качество',
        value: true
      },
      {
        name: 'UHD качество',
        value: false
      },
      {
        name: 'Видеотека Persik',
        value: true
      },
      {
        name: '10 000+ кино',
        value: true
      },
      {
        name: 'Архив 24 дня',
        value: true
      },
      {
        name: 'Мультирум',
        value: true
      }
    ]
  },

  {
    id: 203,
    states: [
      {
        name: '100+ каналов',
        value: true
      },
      {
        name: 'Full HD качество',
        value: true
      },
      {
        name: 'UHD качество',
        value: false
      },
      {
        name: 'Видеотека Persik',
        value: true
      },
      {
        name: 'Кино tvzavr',
        value: true
      },
      {
        name: 'Архив 24 дня',
        value: true
      },
      {
        name: 'Мультирум',
        value: true
      }
    ]
  },
  {
    id: 181,
    states: [
      {
        name: '100+ каналов',
        value: true
      },
      {
        name: 'Full HD качество',
        value: true
      },
      {
        name: 'UHD качество',
        value: false
      },
      {
        name: 'Видеотека Persik',
        value: true
      },
      {
        name: 'Кино tvzavr',
        value: true
      },
      {
        name: 'Архив 24 дня',
        value: true
      },
      {
        name: 'Мультирум',
        value: true
      }
    ]
  },
  {
    id: 248,
    states: [
      {
        name: '100+ каналов',
        value: true
      },
      {
        name: 'Full HD качество',
        value: true
      },
      {
        name: 'UHD качество',
        value: true
      },
      {
        name: 'Видеотека Persik',
        value: true
      },
      {
        name: 'Кино tvzavr',
        value: true
      },
      {
        name: 'Архив 24 дня',
        value: true
      },
      {
        name: 'Мультирум',
        value: true
      }
    ]
  },
  {
    id: 213,
    states: [
      {
        name: '100+ каналов',
        value: true
      },
      {
        name: 'Full HD качество',
        value: true
      },
      {
        name: 'UHD качество',
        value: true
      },
      {
        name: 'Видеотека Persik',
        value: true
      },
      {
        name: 'Кино tvzavr',
        value: true
      },
      {
        name: 'Архив 24 дня',
        value: true
      },
      {
        name: 'Мультирум',
        value: true
      }
    ]
  }, // Year + Android
  {
    id: 188,
    states: [
      {
        name: '70 каналов',
        value: true
      },
      {
        name: 'Full HD качество',
        value: false
      },
      {
        name: 'UHD качество',
        value: false
      },
      {
        name: 'Видеотека Persik',
        value: false
      },
      {
        name: '10 000+ кино',
        value: true
      },
      {
        name: 'Архив 24 дня',
        value: true
      },
      {
        name: 'Мультирум',
        value: false
      }
    ]
  },
  {
    id: 189,
    states: [
      {
        name: '100+ каналов',
        value: true
      },
      {
        name: 'Full HD качество',
        value: true
      },
      {
        name: 'UHD качество',
        value: false
      },
      {
        name: 'Видеотека Persik',
        value: true
      },
      {
        name: '10 000+ кино',
        value: true
      },
      {
        name: 'Архив 24 дня',
        value: true
      },
      {
        name: 'Мультирум',
        value: true
      }
    ]
  },
  {
    id: 190,
    states: [
      {
        name: '100+ каналов',
        value: true
      },
      {
        name: 'Full HD качество',
        value: true
      },
      {
        name: 'UHD качество',
        value: false
      },
      {
        name: 'Видеотека Persik',
        value: true
      },
      {
        name: 'Кино tvzavr',
        value: true
      },
      {
        name: 'Архив 24 дня',
        value: true
      },
      {
        name: 'Мультирум',
        value: true
      }
    ]
  },
  {
    id: 191,
    states: [
      {
        name: '100+ каналов',
        value: true
      },
      {
        name: 'Full HD качество',
        value: true
      },
      {
        name: 'UHD качество',
        value: true
      },
      {
        name: 'Видеотека Persik',
        value: true
      },
      {
        name: 'Кино tvzavr',
        value: true
      },
      {
        name: 'Архив 24 дня',
        value: true
      },
      {
        name: 'Мультирум',
        value: true
      }
    ]
  },
  // films to month
  {
    id: 184,
    states: []
  },
  {
    id: 171,
    states: []
  },
  {
    id: 179,
    states: []
  }
];
