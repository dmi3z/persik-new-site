import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CooperationPageComponent } from './cooperation.component';

@NgModule({
  declarations: [
    CooperationPageComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      {
        path: '',
        component: CooperationPageComponent
      }
    ])
  ]
})

export class CooperationPageModule {}
