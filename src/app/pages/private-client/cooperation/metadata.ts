import { MetaDefinition } from '@angular/platform-browser';
import { MetaData } from '../../../models/meta';

const tags: MetaDefinition[] = [
    {
      name: 'keywords',
      content: 'партнерская программа'
    },
    {
      name: 'description',
      content: `Сотрудничество с ритейлом, дилерами, индивидуальными предпринимателями, интернет-провайдерами - телевидение Персик.`
    }
  ];

const title = 'Сотрудничество с телевидением Persik';

export const meta: MetaData = {
  title,
  tags
};

