import { Products } from './../../../../models/user';
import { AuthService, PaymentService, MetaService } from './../../../../services';
import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { UserSubscription } from '../../../../models';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { Option } from '../../../../models/tariff';

@Component({
  selector: 'app-subscriptions',
  templateUrl: './subscriptions.component.html',
  styleUrls: ['./subscriptions.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class SubscriptionsComponent implements OnInit {

  public subscriptions: UserSubscription[] = [];
  private products: Products;

  selectedPersonId1;
  selectedPersonId3;

  constructor(
    private authService: AuthService,
    private router: Router,
    private paymentService: PaymentService,
    private metaService: MetaService,
    private cdr: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.metaService.setDefaultMeta('Подписки');
    this.loadUserSubscriptions();
  }

  public isExpired(subscription: UserSubscription): boolean {
    const currentTime: number = moment().unix();
    const expiredTime: number = moment(subscription.expired_at, 'YYYY-MM-DD HH:mm:ss').unix();
    return expiredTime - currentTime < 0;
  }

  public isActive(subscription: UserSubscription): boolean {
    if (+subscription.is_active === 1) {
      return true;
    }
    return false;
  }

  public openTariffs(subs: UserSubscription): void {
    this.authService.getAllTariffs().then(res => {
      const product = res.products.find(p => +p.product_id === +subs.product_id);
      if (product) {
        const option = product.options.find(o => +o.product_option_id === +subs.product_option_id);
        if (option) {
          const tariff: Option = {
            product_option_id: +subs.product_option_id,
            price: option.price,
            name: subs.product_name
          };
          this.paymentService.setTariff(tariff, subs.product_option_name);
          this.router.navigate(['private/payment']);
        } else {
          this.router.navigate(['private/tv-tarify'], { fragment: 'tariffs' });
        }
      } else {
        this.router.navigate(['private/tv-tarify'], { fragment: 'tariffs' });
      }
    });
  }

  public getIsOptionAvaliable(option_id: number): boolean {
    if (this.products) {
      const product = this.products.products.find(p => p.options.some(o => +o.product_option_id === +option_id));
      if (product) {
        return true;
      }
      return false;
    }
    return false;
  }

  private loadUserSubscriptions(): void {
    this.authService.getUserSubscriptions().then(res => {
      this.authService.getAllTariffs().then(products => {
        this.products = products;
        this.cdr.markForCheck();
      });
      this.subscriptions = res.sort((a, b) => {
        return b.created_at.localeCompare(a.created_at);
      });
    });
  }

}
