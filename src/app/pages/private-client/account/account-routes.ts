import { Routes } from '@angular/router';
import { ProfileComponent, SubscriptionsComponent, ManageTvComponent, AccountComponent } from './';
import { VideoCallPageComponent } from './video-call/video-call.component';

export const accountRoutes: Routes = [
  {
    path: '',
    component: AccountComponent,
    children: [
      {
        path: '',
        redirectTo: 'profile',
        pathMatch: 'full'
      },
      {
        path: 'profile',
        component: ProfileComponent
      },
      {
        path: 'tv-podpiski',
        component: SubscriptionsComponent
      },
      {
        path: 'upravlenie-tv',
        component: ManageTvComponent
      },
      {
        path: 'video-call',
        component: VideoCallPageComponent
      }
    ]
  }

];
