import { Component, OnInit } from '@angular/core';
import { AuthService, MetaService } from '../../../../services';

@Component({
  selector: 'app-manage-tv',
  templateUrl: 'managetv.component.html',
  styleUrls: ['managetv.component.scss']
})

export class ManageTvComponent implements OnInit {

  public playlist_url = '';

  // selectedPersonId2;
  /* public simpleItem: any = [
		"Стандартное",
		"Стандартное1",
		"Стандартное2"
  ] */

  constructor(private authService: AuthService, private metaService: MetaService) {}

  ngOnInit() {
    this.metaService.setDefaultMeta('Управление ТВ');
    this.authService.getUserPlaylist().then(url => {
      this.playlist_url = url;
    });
  }

  public generateUrl(): void {
    this.authService.updateUserPlaylist().then(url => {
      this.playlist_url = url;
    });
  }
}
