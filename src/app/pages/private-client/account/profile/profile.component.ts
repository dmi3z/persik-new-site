import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { emailCorrectValidator } from '../../../../validators';
import { AuthService, MetaService } from '../../../../services';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { ruLocale } from 'ngx-bootstrap/locale';
import * as moment from 'moment';

defineLocale('ru', ruLocale);

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})

export class ProfileComponent implements OnInit {

  selectedPersonId2;

  public sexDropdown: any = [
    {
      id: 'm',
      name: 'Мужской'
    },
    {
      id: 'f',
      name: 'Женский'
    }
  ];

  public profileForm: FormGroup;
  public name: FormControl;
  public email: FormControl;
  public phone: FormControl;
  public year: FormControl;
  public sex: FormControl;
  public pass_code: FormControl;

  public isLoading: boolean;

  constructor(
    private authService: AuthService,
    private localeService: BsLocaleService,
    private metaService: MetaService
  ) {}

  ngOnInit(): void {
    this.metaService.setDefaultMeta('Ваш профиль');
    this.localeService.use('ru');
    this.createFormControls();
    this.createForm();
    this.getAccountInfo();
  }

  public async onSaveChanges() {
    this.isLoading = true;
    await this.authService.setAccountInfo(this.profileForm.value);
    this.isLoading = false;
  }

  private createFormControls(): void {
    this.name = new FormControl('', Validators.maxLength(50));
    this.email = new FormControl({ value: '', disabled: true }, emailCorrectValidator);
    this.pass_code = new FormControl('',
      [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(4),
        Validators.pattern(/\d{4}/g)
      ]);
    this.sex = new FormControl('');
    this.year = new FormControl('');
    this.phone = new FormControl('', [Validators.maxLength(20), Validators.pattern(/^\d*$/)]);
  }

  private createForm(): void {
    this.profileForm = new FormGroup({
      name: this.name,
      email: this.email,
      pass_code: this.pass_code,
      year: this.year,
      sex: this.sex,
      phone: this.phone
    });
  }

  private getAccountInfo() {
    this.isLoading = true;
    this.authService.getAccountInfo().then(res => {
      this.email.setValue(res.email);
      this.pass_code.setValue(res.pass_code);
      this.name.setValue(res.name);
      this.sex.setValue(res.sex);
      this.phone.setValue(res.phone);
      this.year.setValue(+res.year * 1000);
      this.isLoading = false;
    });
  }

}
