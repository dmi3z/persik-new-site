import { AuthService } from './../../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-account-page',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})

export class AccountComponent implements OnInit {

  public userName = '';
  public isShowConfirmModal: boolean;

  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit(): void {
    this.authService.getAccountInfo().then(res => {
      this.userName = res.name;
    });
  }

  public logout(): void {
    this.authService.logout();
    this.router.navigate(['']);
  }

  public tryLogout(): void {
    this.isShowConfirmModal = true;
  }

  public closeConfirm(): void {
    this.isShowConfirmModal = false;
  }

}
