import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AccountComponent } from './account.component';
import { ProfileComponent, SubscriptionsComponent, ManageTvComponent } from './';
import { CommonModule } from '@angular/common';
import { NgSelectModule } from '@ng-select/ng-select';
import { AccountPosterComponent } from '../../../components/account-poster/account-poster.component';
import { PipesModule } from '../../../pipes/pipes.module';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { accountRoutes } from './account-routes';
import { VideoCallPageComponent } from './video-call/video-call.component';
import { FreeProgressModule } from '../tariffs/free-progress/free-progress.module';

@NgModule({
  declarations: [
    AccountComponent,
    ProfileComponent,
    SubscriptionsComponent,
    ManageTvComponent,
    AccountPosterComponent,
    VideoCallPageComponent
  ],
  imports: [
    CommonModule,
    FreeProgressModule,
    RouterModule.forChild(accountRoutes),
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule,
    PipesModule,
    BsDatepickerModule.forRoot()
  ],
  providers: [],
  exports: [
    AccountPosterComponent
  ]
})

export class AccountModule { }
