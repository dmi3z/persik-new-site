export { ProfileComponent } from './profile/profile.component';
export { SubscriptionsComponent } from './subscriptions/subscriptions.component';
export { ManageTvComponent } from './managetv/managetv.component';
export { AccountComponent } from './account.component';
