import { NgModule } from '@angular/core';
import { HomeComponent } from './home.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ContentSliderModule } from '../../../components/content-slider/content-slider.module';
import { VideobannerComponentModule } from '../../../components/videobanner-slider/videobanner-slider.module';
import { MainChannelCardModule } from '../../../components/main-channel-card/main-channel-card.module';

@NgModule({
    declarations: [
        HomeComponent
    ],
    imports: [
        CommonModule,
        ContentSliderModule,
        VideobannerComponentModule,
        MainChannelCardModule,
        RouterModule.forChild([
            {
                path: '',
                component: HomeComponent
            }
        ])
    ]
})

export class HomeModule { }
