import { HardCodeItem } from '../../../models/hardcontent';
import { ContentName } from '../../../models';

export const hard_films: HardCodeItem[] = [
  {
    id: 1518996,
    image: 'assets/images/archive/films/1.jpg',
    name: 'Ван Гог. С любовью, Винсент',
    type: ContentName.VIDEO
  },
  {
    id: 1032087,
    image: 'assets/images/archive/films/3.jpg',
    name: 'Как Витька Чеснок вез Леху...',
    type: ContentName.VIDEO
  },
  {
    id: 1302330,
    image: 'assets/images/archive/films/5.jpg',
    name: 'Любой ценой',
    type: ContentName.VIDEO
  },
  {
    id: 1302500,
    image: 'assets/images/archive/films/6.jpg',
    name: 'Гонка',
    type: ContentName.VIDEO
  },
  {
    id: 1301938,
    image: 'assets/images/archive/films/7.jpg',
    name: 'Антропоид',
    type: ContentName.VIDEO
  },
  {
    id: 1545472,
    image: 'assets/images/archive/films/8.jpg',
    name: 'Русалка. Озеро мёртвых',
    type: ContentName.VIDEO
  },
  {
    id: 1032033,
    image: 'assets/images/archive/films/9.jpg',
    name: 'Тренер',
    type: ContentName.VIDEO
  }
];

export const hard_series: HardCodeItem[] = [
  {
    id: 915463,
    image: 'assets/images/archive/series/5.jpg',
    name: 'Императрица Ки',
    type: ContentName.VIDEO
  },
  {
    id: 915617,
    image: 'assets/images/archive/series/6.jpg',
    name: 'Это – Англия. Год 1986',
    type: ContentName.VIDEO
  },
  {
    id: 915455,
    image: 'assets/images/archive/series/7.jpg',
    name: 'Параллельные миры',
    type: ContentName.VIDEO
  },
  {
    id: 915563,
    image: 'assets/images/archive/series/8.jpg',
    name: 'Эти глаза напротив',
    type: ContentName.VIDEO
  },
  {
    id: 1032417,
    image: 'assets/images/archive/series/10.jpg',
    name: 'Кухня',
    type: ContentName.VIDEO
  },
  {
    id: 1032361,
    image: 'assets/images/archive/series/11.jpg',
    name: 'Ивановы-Ивановы',
    type: ContentName.VIDEO
  },
  {
    id: 1032494,
    image: 'assets/images/archive/series/12.jpg',
    name: 'Екатерина',
    type: ContentName.VIDEO
  },
  {
    id: 1505939,
    image: 'assets/images/archive/series/13.jpg',
    name: 'Бесстыдники',
    type: ContentName.VIDEO
  },
  {
    id: 1032415,
    image: 'assets/images/archive/series/14.jpg',
    name: 'Кровавая барыня',
    type: ContentName.VIDEO
  }
];

export const hard_tvshows: HardCodeItem[] = [
  {
    id: 915534,
    image: 'assets/images/archive/tvshows/1.jpg',
    name: 'Каннские дневники',
    type: ContentName.VIDEO
  },
  {
    id: 1032190,
    image: 'assets/images/archive/tvshows/2.jpg',
    name: 'Это так Милош',
    type: ContentName.VIDEO
  },
  {
    id: 1032483,
    image: 'assets/images/archive/tvshows/3.jpg',
    name: 'Страна советов. Забытые вожди',
    type: ContentName.VIDEO
  },
  {
    id: 1032201,
    image: 'assets/images/archive/tvshows/4.jpeg',
    name: 'Лондонград. Знай наших',
    type: ContentName.VIDEO
  },
  {
    id: 1032435,
    image: 'assets/images/archive/tvshows/5.jpg',
    name: 'Смешное время',
    type: ContentName.VIDEO
  },
  {
    id: 1032255,
    image: 'assets/images/archive/tvshows/6.jpg',
    name: 'Учимся рисовать',
    type: ContentName.VIDEO
  },
  {
    id: 1032121,
    image: 'assets/images/archive/tvshows/8.jpg',
    name: 'Шоу братьев Шумахеров',
    type: ContentName.VIDEO
  },
  {
    id: 1032166,
    image: 'assets/images/archive/tvshows/9.jpg',
    name: 'Романовы',
    type: ContentName.VIDEO
  },
  {
    id: 931680,
    image: 'assets/images/archive/tvshows/10.jpg',
    name: 'Роскосмос',
    type: ContentName.VIDEO
  }
];

export const hard_cartoons: HardCodeItem[] = [
  {
    id: '1032327',
    image: 'assets/images/archive/cartoons/1.jpg',
    name: 'Малыши и Летающие звери',
    type: ContentName.TV
  },
  {
    id: 1032362,
    image: 'assets/images/archive/cartoons/2.jpg',
    name: 'Малышарики',
    type: ContentName.VIDEO
  },
  {
    id: 1032139,
    image: 'assets/images/archive/cartoons/4.jpg',
    name: 'Смешарики',
    type: ContentName.VIDEO
  },
  {
    id: 1032014,
    image: 'assets/images/archive/cartoons/5.jpg',
    name: 'Балерина',
    type: ContentName.VIDEO
  },
  {
    id: 1306005,
    image: 'assets/images/archive/cartoons/6.jpg',
    name: 'Астерикс: Земля Богов',
    type: ContentName.VIDEO
  },
  {
    id: 950588,
    image: 'assets/images/archive/cartoons/7.jpg',
    name: 'Би-Би-Знайки',
    type: ContentName.VIDEO
  },
  {
    id: 1031800,
    image: 'assets/images/archive/cartoons/10.jpg',
    name: 'Иван Царевич и Серый Волк',
    type: ContentName.VIDEO
  }
];
