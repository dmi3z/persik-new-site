import { AuthService, AnalyticService, DataService } from './../../../services';
import { Banner, Channel, VodSection, VideoInterface } from '../../../models';
import { Component, OnInit, PLATFORM_ID, Inject, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { isPlatformBrowser } from '@angular/common';
import { Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
/*
OnPush not work with videobanner component
*/
export class HomeComponent implements OnInit, OnDestroy {

  public slidesToShow = 6;

  public channelsForShow: Observable<Channel[]>;
  public sectionForShow: VodSection;
  public sections: VodSection[] = [];
  public banners: Banner[] = [];
  public isLogin: boolean;
  public isHaveSubscription: boolean;
  // public hard_films: HardCodeItem[] = hard_films;
  // public hard_series: HardCodeItem[] = hard_series;
  // public hard_tvshows: HardCodeItem[] = hard_tvshows;
  // public hard_cartoons: HardCodeItem[] = hard_cartoons;

  public STARTFilms: VideoInterface;
  public STARTCartoons: VideoInterface;
  public STARTSeries: VideoInterface;
  public tvshows: VideoInterface;
  public activeSection;
  public isBrowser: boolean;

  private selectedSection: string;
  private loginStateSubscriber: Subscription;

  constructor(
    private router: Router,
    @Inject(PLATFORM_ID) private platform: any,
    private authService: AuthService,
    private meta: Meta,
    private analitycService: AnalyticService,
    private dataService: DataService
  ) {}

  ngOnInit() {
    if (isPlatformBrowser(this.platform)) {
      this.isBrowser = true;
    }
    this.isLogin = this.authService.isLogin;
    this.loadUserSubscriptions();
    this.loadSTARTFilms();
    this.loadSTARTCartoons();
    this.loadSTARTSeries();
    this.loadTvshows();
    this.loginStateSubscriber = this.authService.loginState.subscribe(() => {
      this.isLogin = this.authService.isLogin;
      this.loadUserSubscriptions();
    });
    this.meta.addTag({
      name: 'keywords',
      content: 'persik'
    });
  }

  public selectSection(name: string): void {
    this.selectedSection = name;
    const index: number = this.sections.indexOf(this.sections.find(s => s.name === name));
    this.sectionForShow = this.sections[index];
  }

  public get isHaveFilms(): boolean {
    return this.STARTFilms && this.STARTFilms.videos && this.STARTFilms.videos.length > 0;
  }

  public get isHaveTvshows(): boolean {
    return this.tvshows && this.tvshows.videos && this.tvshows.videos.length > 0;
  }

  public get isHaveSeries(): boolean {
    return this.STARTSeries && this.STARTSeries.videos && this.STARTSeries.videos.length > 0;
  }

  public get isHaveCartoons(): boolean {
    return this.STARTCartoons && this.STARTCartoons.videos && this.STARTCartoons.videos.length > 0;
  }

  public getId(item: any): string {
    if (item.video_id) {
      return item.video_id;
    }
    if (item.tvshow_id) {
      return item.tvshow_id;
    }
  }

  public isSectionSelected(name: string): boolean {
    return this.selectedSection === name;
  }

  public try(): void {
    this.analitycService.targetSend('poprobovat', 47723575);
    this.openTariffs();
  }

  public openTariffs(): void {
    this.router.navigate(['private/tv-tarify'], { fragment: 'mounth' });
  }

  public showOnline(): void {
    this.router.navigate(['']);
  }

  public goTo(path: string, fragment?: string): void {
    if (fragment) {
      this.router.navigate([path], {fragment});
    } else {
      this.router.navigate([path]);
    }
  }

  public get isFilms(): boolean {
    return this.activeSection === 'films';
  }

  public get isSeries(): boolean {
    return this.activeSection === 'serialy';
  }

  public get isTvshows(): boolean {
    return this.activeSection === 'peredachi';
  }

  public get isCartoons(): boolean {
    return this.activeSection === 'multfilmy';
  }

  public activateSection(section: string): void {
    if (this.activeSection !== section) {
      this.activeSection = section;
    }
  }

  private loadSTARTFilms(): void {
    this.dataService.getVideoContent(1, 881, 10).then(res => {
      if (res && res.videos && res.videos.length > 0) {
        this.STARTFilms = res;
        if (!this.activeSection) {
          this.activateSection('films');
        }
        if (this.STARTFilms.videos.length === 10) {
          this.STARTFilms.videos.push({ video_id: -1 });
        }
      }
    });
  }

  private loadSTARTSeries(): void {
    this.dataService.getVideoContent(2, 886, 10).then(res => {
      if (res && res.videos && res.videos.length > 0) {
        this.STARTSeries = res;
        if (!this.activeSection) {
          this.activateSection('serialy');
        }
        if (this.STARTSeries.videos.length === 10) {
          this.STARTSeries.videos.push({ video_id: -1 });
        }
      }
    });
  }

  private loadTvshows(): void {
    this.dataService.getVideoContent(4, 0, 10).then(res => {
      if (res && res.videos && res.videos.length > 0) {
        this.tvshows = res;
        if (!this.activeSection) {
          this.activateSection('peredachi');
        }
        if (this.tvshows.videos.length === 10) {
          this.tvshows.videos.push({ video_id: -1 });
        }
      }
    });
  }

  private loadSTARTCartoons(): void {
    this.dataService.getVideoContent(3, 890, 10).then(res => {
      if (res && res.videos && res.videos.length > 0) {
        this.STARTCartoons = res;
        if (!this.activeSection) {
          this.activateSection('multfilmy');
        }
        if (this.STARTCartoons.videos.length === 10) {
          this.STARTCartoons.videos.push({ video_id: -1 });
        }
      }
    });
  }

  private loadUserSubscriptions(): void {
    if (this.isLogin) {
      this.authService.getUserSubscriptions().then(res => {
        this.isHaveSubscription = res.length > 0;
      });
    }
  }

  ngOnDestroy() {
    if (this.loginStateSubscriber) {
      this.loginStateSubscriber.unsubscribe();
    }
  }

}

