import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { emailCorrectValidator } from '../../../validators';
import { AuthService, MetaService } from '../../../services';
import { FeedbackData } from '../../../models/user';
import { meta } from './metadata';

@Component({
  selector: 'advertising-page-component',
  templateUrl: './advertising.component.html',
  styleUrls: ['./advertising.component.scss']
})

export class AdvertisingPageComponent implements OnInit {

  public name: FormControl;
  public email: FormControl;
  public phone: FormControl;
  public message: FormControl;
  public feedbackForm: FormGroup;

  public isShowModal: boolean;

  constructor(private authService: AuthService, private metaService: MetaService) { }

  ngOnInit() {
    this.metaService.updateMeta(meta);
    this.createFormFields();
    this.createForm();
  }

  public sendFeedback(): void {
    const data = <FeedbackData>this.feedbackForm.value;
    this.authService.sendFeedback(data).then(() => {
      this.feedbackForm.reset();
      this.isShowModal = true;
    });
  }

  public stopPropagation(event): void {
    event.stopPropagation();
  }

  public closeModal(): void {
    this.isShowModal = false;
  }

  private createFormFields(): void {
    this.name = new FormControl('', [Validators.required]);
    this.email = new FormControl('', [Validators.required, emailCorrectValidator]);
    this.phone = new FormControl('', [Validators.required, Validators.pattern('^[0-9 +-]*$')]);
    this.message = new FormControl('', [Validators.required]);
  }

  private createForm(): void {
    this.feedbackForm = new FormGroup({
      name: this.name,
      email: this.email,
      phone: this.phone,
      message: this.message
    })
  }

  public scrollToForm(): void {
    try {
      document.querySelector('#feedbackForm').scrollIntoView({ block: 'center', behavior: 'smooth' });
    } catch (e) { }
  }

}
