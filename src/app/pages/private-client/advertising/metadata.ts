import { MetaDefinition } from '@angular/platform-browser';
import { MetaData } from '../../../models/meta';

const tags: MetaDefinition[] = [
    {
      name: 'keywords',
      content: 'размещение рекламы на Persik TV'
    },
    {
      name: 'description',
      content: `Вы можете разместить баннерную и видеорекламу, в e-mail рассылке, Smart TV, мобильном приложении. Выгодное предложение от Pesik TV.`
    }
  ];

const title = 'Размещение рекламы в Интернете - Pesik TV';

export const meta: MetaData = {
  title,
  tags
};

