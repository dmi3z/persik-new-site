import { Component, OnInit, Inject, Optional, PLATFORM_ID } from '@angular/core';
import { Router } from '@angular/router';
import { isPlatformServer } from '@angular/common';
import { RESPONSE } from '@nguniversal/express-engine/tokens';
import { Response } from 'express';

@Component({
  selector: 'app-notfound-page',
  templateUrl: './notfound-page.component.html',
  styleUrls: ['./notfound-page.component.scss']
})

export class NotFoundPageComponent implements OnInit {

  constructor(
    @Inject(PLATFORM_ID) private platformId: Object, // <-- comment for develop
    @Optional() @Inject(RESPONSE) private response: Response, // <-- comment for develop
    private router: Router
  ) { }

  ngOnInit() {
    if (isPlatformServer(this.platformId)) {
     this.response.statusCode = 404;
     this.response.statusText = 'Page not found';
     this.response.status(404); // <-- comment for develop
    }
  }

  public goTo(path: string, fragment?: string): void {
    if (fragment) {
      this.router.navigate([path], {fragment});
    } else {
      this.router.navigate([path]);
    }
  }
}

