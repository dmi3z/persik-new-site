import { Subscription } from "rxjs";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { Component, OnInit, OnDestroy } from "@angular/core";
import {
  AuthService,
  ModalController,
  PaymentService,
  AnalyticService,
  MetaService,
} from "./../../../services";
import { ModalName, Payment } from "../../../models";
import { monthValidator } from "./monthValidator";
import { yearValidator } from "./yearValidator";
import * as moment from "moment";
import { Option } from "../../../models/tariff";

@Component({
  selector: "app-payment",
  templateUrl: "./payment.component.html",
  styleUrls: ["./payment.component.scss"],
})
export class PaymentComponent implements OnInit, OnDestroy {
  public payways: Payway[] = [];
  public userEmail: string;
  public contact: FormControl;
  public contactForm: FormGroup;
  public textForSend: string[];
  public notUrgent: boolean;

  private activePaymentId: number;
  public tariff: Option;
  public period: string;
  public paymethods: string[];

  public isShowModal;
  private modalType: string;

  public cardMonth: FormControl;
  public cardYear: FormControl;
  public expiredForm: FormGroup;
  public isPeriodlessTariff: boolean;

  private loginStateSubscriber: Subscription;

  constructor(
    private authService: AuthService,
    private modalController: ModalController,
    private paymentService: PaymentService,
    private analyticService: AnalyticService,
    private router: Router,
    private metaService: MetaService
  ) {}

  ngOnInit(): void {
    this.metaService.setDefaultMeta("Оплата");
    const url = this.router.url;
    const pattern = /id=[0-9]+/;
    const param = pattern.exec(url);
    if (param) {
      // const id = +param[0].split('=')[1];
    } else {
      if (this.paymentService.isHavePackage) {
        this.initializePage();
      } else {
        this.router.navigate(["private/tv-tarify"]);
      }
    }
    this.createFormField();
    this.createContactForm();
  }

  private createFormField(): void {
    this.contact = new FormControl("", [
      Validators.minLength(12),
      Validators.maxLength(20),
      Validators.pattern("^(375)[0-9]+"),
      Validators.required,
    ]);

    this.cardMonth = new FormControl("", [
      Validators.required,
      monthValidator,
      Validators.minLength(2),
    ]);
    this.cardYear = new FormControl("", [
      Validators.required,
      Validators.minLength(2),
      yearValidator,
    ]);
  }

  private createContactForm(): void {
    this.contactForm = new FormGroup({
      contact: this.contact,
    });
    this.expiredForm = new FormGroup({
      cardMonth: this.cardMonth,
      cardYear: this.cardYear,
    });
  }

  public get isHavePayways(): boolean {
    return this.payways.length > 0;
  }

  public get isAssistSelected(): boolean {
    return +this.activePaymentId === 0;
  }

  public get isMountly(): boolean {
    if (this.period) {
      return this.period.includes("месяц");
    }
    return false;
  }

  public onFieldKeyDown(event: Event) {
    this.cardMonth.setErrors({ incorrect: null });
    this.cardMonth.updateValueAndValidity();
    const input = event.target as HTMLInputElement;
    if (input.name === "month" && input.value.length === 2) {
      (document.querySelector('[name="year"]') as HTMLInputElement).focus();
    }
  }

  public get isTestPeriod(): boolean {
    if (this.period) {
      return this.period.includes("месяц с тестовым");
    }
    return false;
  }

  public openAuthModal(): void {
    this.modalController.present(ModalName.LOGIN);
  }

  public get isLogin(): boolean {
    return this.authService.isLogin;
  }

  public login(): void {
    this.modalCancel();
    this.modalController.present(ModalName.LOGIN);
  }

  public register(): void {
    this.modalCancel();
    this.modalController.present(ModalName.REGISTER);
  }

  private loadUserEmail(): void {
    this.authService.getAccountInfo().then((res) => {
      this.userEmail = res.email;
    });
  }

  public setActivePaymentId(value: number) {
    this.activePaymentId = value;
  }

  public isPaymentSelected(id: number): boolean {
    return this.activePaymentId === id;
  }

  public modalCancel(): void {
    this.isShowModal = false;
    this.checkCallback();
  }

  public closeModal(): void {
    this.isShowModal = false;
  }

  public openCardPayments(): void {
    window.open("https://www.mixxx.pro/licence", "_blank");
  }

  public confirm(): void {
    if (this.isAssistSelected) {
      if (this.expiredForm.valid) {
        const currentShortYear = Number(
          new Date().getFullYear().toString().substr(2, 2)
        );
        if (+this.cardYear.value === currentShortYear) {
          const currentMonth = new Date().getMonth() + 1;
          if (currentMonth < +this.cardMonth.value) {
            const expiredUnixDate = moment(
              `${this.cardMonth.value}-${this.cardYear.value}`,
              "MM-YY"
            ).unix();
            this.sendData(expiredUnixDate);
          } else {
            this.cardMonth.setErrors({ incorrect: true });
            return;
          }
        } else {
          const expiredUnixDate = moment(
            `${this.cardMonth.value}-${this.cardYear.value}`,
            "MM-YY"
          ).unix();
          this.sendData(expiredUnixDate);
        }
      } else {
        this.cardMonth.markAsTouched();
        this.cardYear.markAsTouched();
      }
    } else {
      this.sendData();
    }
  }

  private sendData(expiredDate?: number): void {
    this.analyticService.targetSend("podtverdit", 47723728);
    if (this.isLogin) {
      this.paymentService.setPaymentStamp();
      if (this.payways.length > 0) {
        this.standartCheck(expiredDate);
      } else {
        this.checkCallback();
      }
    } else {
      this.modalType = "needlogin";
      this.isShowModal = true;
    }
  }

  private standartCheck(expiredDate?: number): void {
    if (this.activePaymentId >= 0) {
      let payment: Payment;
      if (this.isBook) {
        payment = {
          pay_sys: this.payways.find((p) => p.id === this.activePaymentId)
            .back_name,
          litres_item_id: this.paymentService.selectedTariff.product_option_id,
        };
      } else {
        payment = {
          product_option_id: [this.tariff.product_option_id],
          pay_sys: this.payways.find((p) => p.id === this.activePaymentId)
            .back_name,
        };
      }
      this.authService.createPayment(payment, expiredDate).then((res) => {
        if (res.payment_url && res.payment_url.length > 0) {
          const a = document.createElement("a");
          document.body.appendChild(a);
          a.setAttribute("style", "display: none");
          a.href = res.payment_url;
          a.click();
          a.remove();
        } else {
          this.textForSend = res.description.split("\n");
          this.modalType = "needsms";
          this.isShowModal = true;
        }
      });
    } else {
      this.modalType = "needpayway";
      this.isShowModal = true;
    }
  }

  private checkCallback(): void {
    if (this.contactForm.valid) {
      this.authService.setPhoneCallback(this.contact.value).then((_) => {
        this.modalType = "needcallback";
        this.isShowModal = true;
        this.contact = null;
      });
    } else {
      if (this.payways.length === 0) {
        this.modalType = "needphone";
        this.isShowModal = true;
      }
    }
  }

  public get isNeedLogin(): boolean {
    return this.modalType === "needlogin";
  }

  public get isNeedPayWay(): boolean {
    return this.modalType === "needpayway";
  }

  public get isNeedLink(): boolean {
    return this.modalType === "needlink";
  }

  public get isNeedCallback(): boolean {
    return this.modalType === "needcallback";
  }

  public get isNeedSms(): boolean {
    return this.modalType === "needsms";
  }

  public get isNeedPhone(): boolean {
    return this.modalType === "needphone";
  }

  private makePayWayArray(payways: string[]): void {
    this.payways = payways.map((item, index) => {
      return {
        id: index,
        name: this.getPaySysName(item),
        back_name: item,
        logo: this.getPaySysLogo(item),
      };
    });
  }

  private getPaySysName(name: string): string {
    switch (name) {
      case "assist":
        return "Assist";
      case "ipay_mts":
        return "МТС";
      case "ipay_life":
        return "Life :)";
      case "ipay_erip":
        return "ЕРИП";
      case "yandex":
        return "Yandex";
      default:
        return "";
    }
  }

  private getPaySysLogo(name: string): string {
    const baseUrl = "assets/images/icons/";
    switch (name) {
      case "assist":
        return baseUrl + "credit-cards.svg";
      case "ipay_mts":
        return baseUrl + "mts.png";
      case "ipay_life":
        return baseUrl + "life.png";
      case "ipay_erip":
        return baseUrl + "erip.png";
      case "yandex":
        return baseUrl + "yandex.png";
      default:
        return "";
    }
  }

  public get isBook(): boolean {
    if (this.paymentService.selectedTariff) {
      return this.paymentService.selectedTariff.is_book;
    }
    return false;
  }

  private initializePage(): void {
    if (this.isBook) {
      this.tariff = this.paymentService.selectedTariff;
      this.period = this.paymentService.period;
      this.authService.getAllTariffs().then((res) => {
        this.makePayWayArray(res.pay_sys);
      });
    } else {
      this.tariff = this.paymentService.selectedTariff;
      this.period = this.paymentService.period;

      const { product_option_id } = this.tariff;

      this.isPeriodlessTariff =
        product_option_id === 288 ||
        product_option_id === 289 ||
        product_option_id === 290 ||
        product_option_id === 291;
      this.authService.getAllTariffs().then((res) => {
        this.makePayWayArray(res.pay_sys);
      });
    }
    if (this.isLogin) {
      this.loadUserEmail();
    }
    this.loginStateSubscriber = this.authService.loginState.subscribe(() => {
      if (this.isLogin) {
        this.loadUserEmail();
      }
    });
  }

  ngOnDestroy() {
    if (this.loginStateSubscriber) {
      this.loginStateSubscriber.unsubscribe();
    }
  }
}

interface Payway {
  id: number;
  name: string;
  back_name: string;
  logo: string;
}
