import { FormControl } from '@angular/forms';

export function monthValidator(control: FormControl) {

  const value = +control.value;

  const result = value > 0 && value <= 12;

  if (result) {
    return null;
  } else {
    return {
      'monthValidator': {
        valid: false
      }
    };
  }

}
