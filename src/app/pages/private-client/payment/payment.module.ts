import { NgModule } from '@angular/core';
import { PaymentComponent } from './payment.component';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { OnlyDigitsDirective } from './only-digits.directive';
import { PipesModule } from '../../../pipes/pipes.module';

@NgModule({
  declarations: [
    PaymentComponent,
    OnlyDigitsDirective
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    PipesModule,
    RouterModule.forChild([
      {
        path: '',
        component: PaymentComponent
      }
    ])
  ]
})

export class PaymentPageModule { }
