import { FormControl } from '@angular/forms';

export function yearValidator(control: FormControl) {

  const inputYear = +control.value;
  const currentShortYear = Number(new Date().getFullYear().toString().substr(2, 2));

  const result = currentShortYear <= inputYear && inputYear - currentShortYear <= 5;

  if (result) {
    return null;
  } else {
    return {
      'yearValidator': {
        valid: false
      }
    };
  }

}
