import { MetaDefinition } from '@angular/platform-browser';
import { MetaData } from '../../../models/meta';

const tags: MetaDefinition[] = [
    {
      name: 'keywords',
      content: 'партнерская программа с онлайн-курсами'
    },
    {
      name: 'description',
      content: `Сотрудничество с авторами онлайн-курсов, тренингов, видеоуроков - телевидение Персик.`
    }
  ];

const title = 'Сотрудничество с авторами курсов онлайн';

export const meta: MetaData = {
  title,
  tags
};

