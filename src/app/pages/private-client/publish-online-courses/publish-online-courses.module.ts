import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PublishOnlineCoursesPageComponent } from './publish-online-courses.component';

@NgModule({
  declarations: [
    PublishOnlineCoursesPageComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      {
        path: '',
        component: PublishOnlineCoursesPageComponent
      }
    ])
  ]
})

export class PublishOnlineCoursesPageModule {}
