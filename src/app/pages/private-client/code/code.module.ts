import { CodePageComponent } from './code.component';
import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgxMaskModule } from 'ngx-mask';

@NgModule({
  declarations: [
    CodePageComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    NgxMaskModule.forChild(),
    RouterModule.forChild([
      {
        path: '',
        component: CodePageComponent
      }
    ])
  ]
})

export class CodePageModule { }
