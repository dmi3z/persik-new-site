import { FormControl, FormGroup, Validators, ValidatorFn, FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { emailCorrectValidator } from '../../../validators';
import { CodeData } from '../../../models/user';
import { AuthService, MetaService } from '../../../services';
import { Router } from '@angular/router';

@Component({
  selector: 'app-code-component',
  templateUrl: './code.component.html',
  styleUrls: ['./code.component.scss']
})

export class CodePageComponent implements OnInit {

  public code: FormControl;
  public phone: FormControl;
  public email: FormControl;

  public codeForm: FormGroup;

  public fakeCodeError: string;
  public isCodeSuccess: boolean;
  public successData: CodeResponse;
  public codeOrPhoneError: boolean;

  constructor(
    private authService: AuthService,
    private router: Router,
    private metaService: MetaService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    this.metaService.setDefaultMeta('Активация кода');
    this.createFormFields();
    this.createForm();
  }

  public toggleAddressValidators(): void {
    const email = this.email;
    const emailValidators: ValidatorFn[] = [ emailCorrectValidator ];
    if (email.value.length > 0) {
      email.setValidators(emailValidators);
      email.updateValueAndValidity();
    } else {
      email.clearValidators();
      email.updateValueAndValidity();
    }
  }

  public goTo(path: string, fragment?: string): void {
    if (fragment) {
      this.router.navigate([path], {fragment});
    } else {
      this.router.navigate([path]);
    }
  }

  public sendCode(): void {
    this.code.markAsTouched();
    this.phone.markAsTouched();
    console.log(this.codeForm);
    if (this.codeForm.valid) {
      const data = <CodeData>this.codeForm.value;
      this.authService.sendCode(data).then((res) => {
        this.successData = res;
        this.isCodeSuccess = true;
        this.codeForm.reset();
      }).catch(er =>  this.fakeCodeError = er.error.error);
    }
    // if (this.codeForm.value.email.length == 0 && this.codeForm.value.phone.length == 0){
    //   this.codeOrPhoneError = true;
    // }
    // else{
    //   this.codeOrPhoneError = false;
    //   const data = <CodeData>this.codeForm.value;
    //   this.authService.sendCode(data).then((res) => {
    //     this.successData = res;
    //     this.isCodeSuccess = true;
    //     this.codeForm.reset();
    //   }).catch(er =>  this.fakeCodeError = er.error.error);
    // }
  }

  private createFormFields(): void {
    this.email = new FormControl('', []);
    this.phone = new FormControl('', [Validators.required, Validators.minLength(4)]);
    this.code = new FormControl('', [Validators.required]);
  }

  private createForm(): void {
    this.codeForm = this.formBuilder.group({
      email: this.email,
      code: this.code,
      phone: this.phone
    });
    // this.subscriptionToEmail();
  }

}

interface CodeResponse {
  email?: string;
  password?: string;
  code: string;
  subscription: Subscr;
}

interface Subscr {
  from: string;
  name: string;
  term: string;
  to: string;
}
