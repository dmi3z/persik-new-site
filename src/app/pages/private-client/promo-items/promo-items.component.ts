import { Component, OnInit } from '@angular/core';
import { PaymentService, DataService, MetaService } from '../../../services';
import { Router } from '@angular/router';
import { meta } from './metadata';
import { Option, Product } from '../../../models/tariff';

@Component({
  selector: 'promo-items-component',
  templateUrl: './promo-items.component.html',
  styleUrls: ['./promo-items.component.scss']
})

export class PromoItemsPageComponent implements OnInit {

  public package: Product[];
  public selectedTariff: Option;
  private packageNumber: number = 2; // 0 - на месяц, 1 - На год с ежемесячной оплатой, 2 - На год, 3 - На год + Android приставка, 4 - Кино на месяц

  constructor(private paymentService: PaymentService, private dataService: DataService, private router: Router, private metaService: MetaService,) { }

  ngOnInit() {
    this.dataService.getTariffs().subscribe(tariffs => this.package = tariffs.products);
    this.metaService.updateMeta(meta);
  }

  public scrollToGrid(): void {
    try {
      document.querySelector('.promo-grid').scrollIntoView({ block: 'center', behavior: 'smooth' });
    } catch (e) { }
  }

  public goToPay(itemId): void {
    const activePack: Product = this.package.find(pack => pack.product_id === this.packageNumber);
    this.selectedTariff = activePack.options.find(item => item.product_option_id === itemId);
    this.paymentService.setTariff(this.selectedTariff, activePack.name);
    this.router.navigate(['private/payment']);
  }

}
