import { NgModule } from '@angular/core';
import { PromoItemsPageComponent } from './promo-items.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    PromoItemsPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: PromoItemsPageComponent
      }
    ])
  ]
})

export class PromoItemsPageModule { }
