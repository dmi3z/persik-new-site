import { MetaDefinition } from '@angular/platform-browser';
import { MetaData } from '../../../models/meta';

const tags: MetaDefinition[] = [
  {
    name: 'keywords',
    content: 'акции на тв, скидки'
  },
  {
    name: 'description',
    content: `Купить Persik TV по акции (тв со скидкой). Выгодные предложения на интернет-телевидение,
      скидки на подключение. Архив 24 дня, Full HD качество, 100 каналов, программа передач. info@persik.by`
  }
];

const title = 'Интернет-телевидение - акции и скидки на Persik TV';

export const meta: MetaData = {
  title,
  tags
};

