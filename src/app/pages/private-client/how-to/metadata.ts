import { MetaDefinition } from '@angular/platform-browser';
import { MetaData } from '../../../models/meta';

const title = 'Скачать приложение для просмотра тв – для смартфона, планшета, ноутбука, телевизора';

const tags: MetaDefinition[] = [
  {
    name: 'keywords',
    content: 'скачать приложение для просмотра тв, смотреть тв app'
  },
  {
    name: 'description',
    content: `Смотреть телевидение через приложение (apps) Persik TV для Андроид, IOS, Windows, Smart TV.
      ✅ Архив 24 дня, Full HD качество, 100 каналов, мультирум. info@persik.by`
  }
];

export const meta: MetaData = {
  title,
  tags
};
