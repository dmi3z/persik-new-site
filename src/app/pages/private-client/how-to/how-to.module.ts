import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HowToComponent } from './how-to.component';
import { SlickModule } from 'ngx-slick';
import { SliderItemComponent } from './slider-item/slider-item.component';

@NgModule({
  declarations: [
    HowToComponent,
    SliderItemComponent
  ],
  imports: [
    CommonModule,
    SlickModule.forRoot(),
    RouterModule.forChild([
      {
        path: '',
        component: HowToComponent
      }
    ])
  ]
})

export class HowToPageModule {}

