import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-slider-item',
  templateUrl: './slider-item.component.html',
  styleUrls: ['./slider-item.component.scss']
})

export class SliderItemComponent {

  @Input() item: any;
  @Output() changeActive: EventEmitter<any> = new EventEmitter();

  constructor() {}

  public setPlatform(item: any): void {
    this.changeActive.emit(item);
  }

}
