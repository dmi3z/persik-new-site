export class SmartPlatform {
  id: number;
  small: string;
  image: string;
  orange: string;
  link?: string;
  button?: string;
}

export class PhonePlatform {
  id: number;
  text: string;
  image: string;
  download_button?: string;
}

export class PcPlatform {
  id: number;
  text: string;
  image: string;
  link?: string;
}

export class BoxPlatform {
  id: number;
  small: string;
  big: string;
  image: string;
  download_link?: string;
  connect_link?: string;
  picture: string;
  version?: string;
}

export interface ThreeSteps {
  content_id: number;
  internet: string;
  application: string;
  start: string;
}

export const threeStepsSmart: ThreeSteps[] = [
  {
    content_id: 0,
    internet: 'Подключите телевизор к проводному интернету или Wi-Fi.',
    application: `Установить приложение TV BY Persik можно из официального магазина приложений "Samsung Apps" - раздел "Видео".
    В настройках телевизора должен быть указан регион "Беларусь" или "Россия", иначе приложение может не отображаться в магазине.`,
    start: 'Ура! Теперь Вы сможете смотреть любимое кино, передачи, сериалы, мультфильмы.'
  },
  {
    content_id: 1,
    internet: 'Подключите телевизор к проводному интернету или Wi-Fi.',
    application: 'Установить приложение TV BY Persik можно из официального магазина приложений "LG Store". В настройках телевизора должен быть указан регион "Беларусь" или "Россия", иначе приложение может не отображаться в магазине.',
    start: 'Ура! Теперь Вы сможете смотреть любимое кино, передачи, сериалы, мультфильмы.'
  },
  {
    content_id: 2,
    internet: 'Подключите телевизор к проводному интернету или Wi-Fi.',
    application: 'Вы можете скачать приложение TV BY Persik из магазина приложений "Google Play", введя в поиск "Persik " или перейдя по ссылке "Скачайте в Google Play" выше.',
    start: 'Ура! Теперь Вы сможете смотреть любимое кино, передачи, сериалы, мультфильмы.'
  },
  {
    content_id: 3,
    internet: 'Подключите телевизор к проводному интернету или Wi-Fi.',
    application: 'Вы можете скачать приложение TV BY Persik из магазина приложений "Google Play", введя в поиск "Persik " или перейдя по ссылке "Скачайте в Google Play" выше.',
    start: 'Ура! Теперь Вы сможете смотреть любимое кино, передачи, сериалы, мультфильмы.'
  },
  {
    content_id: 4,
    internet: 'Подключите телевизор к проводному интернету или Wi-Fi.',
    application: 'Вы можете скачать приложение TV BY Persik из магазина приложений "Google Play", введя в поиск "Persik " или перейдя по ссылке "Скачайте в Google Play" выше.',
    start: 'Ура! Теперь Вы сможете смотреть любимое кино, передачи, сериалы, мультфильмы.'
  },
  {
    content_id: 5,
    internet: 'Подключите телевизор к проводному интернету или Wi-Fi.',
    application: 'Вы можете скачать приложение TV BY Persik из магазина приложений "Google Play", введя в поиск "Persik " или перейдя по ссылке "Скачайте в Google Play" выше.',
    start: 'Ура! Теперь Вы сможете смотреть любимое кино, передачи, сериалы, мультфильмы.'
  },
];

export const threeStepsPhone: ThreeSteps[] = [
  {
    content_id: 0,
    internet: 'Подключите смартфон или планшет к мобильному интернету или Wi-Fi.',
    application: 'Вы можете скачать приложение TV BY Persik из магазина приложений "Google Play", введя в поиск "Persik " или перейдя по ссылке "Скачайте в Google Play" выше.',
    start: 'Ура! Теперь приложение всегда будет на рабочем столе смартфона/планшета и его будет легко найти. Запустите TV BY Persik и наслаждайтесь просмотром!'
  },
  {
    content_id: 1,
    internet: 'Подключите смартфон или планшет к мобильному интернету или Wi-Fi.',
    application: 'Вы можете скачать приложение TV BY Persik из магазина приложений "Apple Store", введя в поиск "Persik " или перейдя по ссылке "Скачайте в Apple Store" выше.',
    start: 'Ура! Теперь приложение всегда будет на рабочем столе смартфона/планшета. Запустите TV BY Persik и наслаждайтесь просмотром!'
  }
];

export const threeStepsPc: ThreeSteps[] = [
  {
    content_id: 0,
    internet: 'Подключите компьютер или ноутбук к проводному интернету/Wi-Fi.',
    application: 'Вы можете скачать приложение TV BY Persik, нажав на кнопку "Скачать" выше.',
    start: 'Ура! Теперь приложение всегда будет на рабочем столе компьютера или ноутбука и его будет всегда легко найти. Запустите TV BY Persik и наслаждайтесь просмотром!'
  },
  {
    content_id: 1,
    internet: 'Подключите компьютер или ноутбук к проводному интернету/Wi-Fi.',
    application: 'Вы можете скачать приложение TV BY Persik, нажав на кнопку "Скачать" выше.',
    start: 'Ура! Теперь приложение всегда будет на рабочем столе компьютера или ноутбука и его будет всегда легко найти. Запустите TV BY Persik и наслаждайтесь просмотром!'
  },
];

export const threeStepsBox: ThreeSteps[] = [
  {
    content_id: 0,
    internet: 'Подключите медиаплеер к интернету (если не подключен, то подключаем: настройки/сеть/WiFi - выбираем в списке доступных сетей вашу сеть и вводим пароль).',
    application: 'Вы можете скачать приложение TV BY Persik из магазина приложений "Google Play", введя в поиск "Persik" или перейдя по ссылке "Скачайте в Google Play" выше.',
    start: 'Ура! Теперь Вы сможете смотреть любимое кино, передачи, сериалы, мультфильмы.'
  },
  {
    content_id: 1,
    internet: 'На ТВ-приставках приложение Persik уже установлено. Подключите приставку к ТВ.',
    application: 'Найдите пункт "Добавить IPTV-список". Выберите его.',
    start: 'Вы увидите список каналов, которые можно смотреть на вашем телевизоре.'
  },
  {
    content_id: 2,
    internet: 'На ТВ-приставках приложение Persik уже установлено. Подключите приставку к ТВ.',
    application: 'Найдите пункт "Добавить IPTV-список". Выберите его.',
    start: 'Вы увидите список каналов, которые можно смотреть на вашем телевизоре.'
  },
  {
    content_id: 3,
    internet: 'На ТВ-приставках приложение Persik уже установлено. Подключите приставку к ТВ.',
    application: 'Найдите пункт "Добавить IPTV-список". Выберите его.',
    start: 'Вы увидите список каналов, которые можно смотреть на вашем телевизоре.'
  }
];

export const smarts: SmartPlatform[] = [
  {
    id: 0,
    small: 'Samsung Smart TV',
    orange: 'Samsung Smart TV',
    image: 'assets/images/howto-tv-2.png',
  },
  {
    id: 1,
    small: 'LG Smart TV',
    orange: 'LG webOS',
    image: 'assets/images/howto-tv-1.png',
  },
  {
    id: 2,
    small: 'Sony Android TV',
    orange: 'Sony Android TV',
    image: 'assets/images/howto-tv-3.png',
    link: 'https://play.google.com/store/apps/details?id=by.persik.androidtv',
    button: 'googleplay'
  },
  {
    id: 3,
    small: 'Philips Android TV',
    orange: 'Philips Android TV',
    image: 'assets/images/howto-tv-4.png',
    link: 'https://play.google.com/store/apps/details?id=by.persik.androidtv',
    button: 'googleplay'
  },
  {
    id: 4,
    small: 'Sharp Smart TV',
    orange: 'Sharp Smart TV',
    image: 'assets/images/howto-tv-5.png',
    link: 'https://play.google.com/store/apps/details?id=by.persik.androidtv',
    button: 'googleplay'
  },
  {
    id: 5,
    small: 'Витязь/Горизонт Smart TV',
    orange: 'Витязь и Горизонт Android TV',
    image: 'assets/images/howto-tv-6.png',
    link: 'https://play.google.com/store/apps/details?id=by.persik.androidtv',
    button: 'googleplay'
  }
];

export const phones: PhonePlatform[] = [
  {
    id: 0,
    text: 'Android',
    image: 'assets/images/howto-mobile.png',
    download_button: 'http://persik.by/android/by.persik.android21.apk'
  },
  {
    id: 1,
    image: 'assets/images/howto-mobile1.png',
    text: 'iOS',
    download_button: 'https://itunes.apple.com/ru/app/persik-tv-%D0%BA%D0%B8%D0%BD%D0%BE-%D1%81%D0%B5%D1%80%D0%B8%D0%B0%D0%BB%D1%8B/id1459234651?mt=8'
  }
];

export const pcs: PcPlatform[] = [
  {
    id: 0,
    text: 'Windows',
    image: 'assets/images/howto-desc1.png',
    link: 'https://persik.by/downloads/PersikTVSetup_v1.2.3.exe'
  },
  {
    id: 1,
    text: 'Mac OS',
    image: 'assets/images/howto-desc2.png',
    link: 'https://persik.by/downloads/persik_installer.dmg'
  }
];

export const boxes: BoxPlatform[] = [
  {
    id: 1,
    small: 'с Android',
    big: 'на базе ОС Android',
    download_link: 'https://play.google.com/store/apps/details?id=by.persik.androidtv',
    image: 'assets/images/howto-console.png',
    picture: 'assets/images/console.jpg',
    version: '7 версия и старше'
  },
  {
    id: 0,
    small: 'с Android',
    big: 'на базе ОС Android',
    download_link: 'https://persik.by/android/app-armv7-release.apk',
    image: 'assets/images/howto-console.png',
    picture: 'assets/images/console.jpg',
    version: '4 версия и старше'
  },
  {
    id: 2,
    small: 'AuraHD',
    big: 'AuraHD',
    connect_link: 'https://persik.by/downloads/aura.pdf',
    image: 'assets/images/howto-console-aura.png',
    picture: 'assets/images/console-aura.png'
  },
  {
    id: 3,
    small: 'MAG',
    big: 'MAG',
    connect_link: 'https://persik.by/downloads/mag.pdf',
    image: 'assets/images/howto-console-mag.png',
    picture: 'assets/images/console-mag.png'
  },
  {
    id: 4,
    small: 'Dune',
    big: 'Dune',
    connect_link: 'https://persik.by/downloads/dunehd.pdf',
    image: 'assets/images/howto-console-dune.png',
    picture: 'assets/images/console-dune.png'
  }
];
