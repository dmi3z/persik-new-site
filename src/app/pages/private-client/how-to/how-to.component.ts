import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, AfterViewInit, Inject, PLATFORM_ID, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import {
  smarts,
  phones,
  pcs,
  boxes,
  SmartPlatform,
  PhonePlatform,
  PcPlatform,
  BoxPlatform,
  ThreeSteps,
  threeStepsSmart,
  threeStepsBox,
  threeStepsPhone,
  threeStepsPc
} from './harddata';
import { MetaService } from '../../../services/meta.service';
import { meta } from './metadata';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-how-to',
  templateUrl: './how-to.component.html',
  styleUrls: ['./how-to.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class HowToComponent implements OnInit, AfterViewInit, OnDestroy {

  public slideConfig = {
    'slidesToShow': 4,
    'slidesToScroll': 4,
    'arrows': true,
    'dots': true,
    'fade': false,
    'centerMode': false,
    'autoplay': false,
    'infinity': false,
    'responsive': [
      {
        'breakpoint': 1200,
        'settings': {
          'dots': true,
          'slidesToShow': 3,
          'slidesToScroll': 3,
          'arrows': true,
          'fade': false,
          'centerMode': false,
          'autoplay': false,
          'infinity': false
        }
      },
      {
        'breakpoint': 992,
        'settings': {
          'dots': true,
          'slidesToShow': 3,
          'slidesToScroll': 3,
          'arrows': true,
          'fade': false,
          'centerMode': false,
          'autoplay': false,
          'infinity': false
        }
      },
      {
        'breakpoint': 767,
        'settings': {
          'dots': true,
          'slidesToShow': 2,
          'slidesToScroll': 2,
          'arrows': false,
          'fade': false,
          'centerMode': false,
          'autoplay': false,
          'infinity': false
        }
      },
    ]
  };

  private fragment: string;

  public smarts: SmartPlatform[] = smarts;
  public phones: PhonePlatform[] = phones;
  public pcs: PcPlatform[] = pcs;
  public boxes: BoxPlatform[] = boxes;
  private _threeStepsSmart: ThreeSteps[] = threeStepsSmart;
  private _threeStepsBox: ThreeSteps[] = threeStepsBox;
  private _threeStepsPhone: ThreeSteps[] = threeStepsPhone;
  private _threeStepsPc: ThreeSteps[] = threeStepsPc;
  public activeSmart: SmartPlatform;
  public activePhone: PhonePlatform;
  public activePc: PcPlatform;
  public activeBox: BoxPlatform;

  public isBrowser = false;

  private activatedRouteSubscriber: Subscription;

  constructor(
    private activatedRoute: ActivatedRoute,
    @Inject(PLATFORM_ID) private platform,
    private metaService: MetaService
  ) {}

  ngOnInit() {
    this.metaService.updateMeta(meta);
    if (isPlatformBrowser(this.platform)) {
      this.isBrowser = true;
    }
    this.initStartSelected();
    this.activatedRouteSubscriber = this.activatedRoute.fragment.subscribe(fragment => {
      this.fragment = fragment;
      this.scrollToTitle();
    });
  }

  private initStartSelected(): void {
    this.activeSmart = this.smarts[0];
    this.activePhone = this.phones[0];
    this.activePc = this.pcs[0];
    this.activeBox = this.boxes[0];
  }

  public setSmartPlatform(item: SmartPlatform): void {
    this.activeSmart = item;
    this.scrollToInfo('tv');
  }

  public setPhonePlatform(item: PhonePlatform): void {
    this.activePhone = item;
    this.scrollToInfo('tablet');
  }

  public setPcPlatform(item: PcPlatform): void {
    this.activePc = item;
    this.scrollToInfo('pcinfo');
  }

  public setBoxPlatform(item: BoxPlatform): void {
    this.activeBox = item;
    this.scrollToInfo('box');
  }

  public get threeStepsSmart(): ThreeSteps {
    return this._threeStepsSmart.find(steps => steps.content_id === this.activeSmart.id);
  }

  public get threeStepsBox(): ThreeSteps {
    return this._threeStepsBox.find(steps => steps.content_id === this.activeBox.id);
  }

  public get threeStepsPhone(): ThreeSteps {
    return this._threeStepsPhone.find(steps => steps.content_id === this.activePhone.id);
  }

  public get threeStepsPc(): ThreeSteps {
    return this._threeStepsPc.find(steps => steps.content_id === this.activePc.id);
  }

  private scrollToInfo(name: string): void {
    try {
      document.querySelector('#' + name).scrollIntoView({ block: 'center', behavior: 'smooth' });
    } catch (e) { }
  }

  private scrollToTitle(): void {
    try {
      setTimeout(() => {
        document.querySelector('#' + this.fragment).scrollIntoView({ block: 'center' });
      }, 100);
    } catch (e) { }
  }

  ngAfterViewInit(): void {
    this.scrollToTitle();
  }

  ngOnDestroy() {
    if (this.activatedRouteSubscriber) {
      this.activatedRouteSubscriber.unsubscribe();
    }
  }
}

