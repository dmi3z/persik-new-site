import { Routes } from '@angular/router';
import { AuthGuard } from '../../services/auth.guard';

import {
  NotFoundPageComponent
} from './';

export const privateClientRoutes: Routes = [
  {
    path: '',
    redirectTo: 'tv-tarify',
    pathMatch: 'full'
  },
  {
    path: '404',
    component: NotFoundPageComponent
  },
  {
    path: 'main',
    loadChildren: './home/home.module#HomeModule'
  },
  {
    path: 'user',
    loadChildren: './account/account.module#AccountModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'korporativnym-klientam',
    loadChildren: './corporate/corporate.module#CorporateModule'
  },
  {
    path: 'contacts',
    loadChildren: './contact-us/contact-us.module#ContactUsModule'
  },
  {
    path: 'client/code',
    loadChildren: './code/code.module#CodePageModule'
  },
  {
    path: 'reklama-na-persik-tv',
    loadChildren: './advertising/advertising.module#AdvertisingPageModule'
  },
  {
    path: 'akcionnye-predlozheniya',
    loadChildren: './promo-items/promo-items.module#PromoItemsPageModule'
  },
  {
    path: 'devices',
    loadChildren: './how-to/how-to.module#HowToPageModule'
  },
  {
    path: 'sotrudnichestvo',
    loadChildren: './cooperation/cooperation.module#CooperationPageModule'
  },
  {
    path: 'publish-online-courses',
    loadChildren: './publish-online-courses/publish-online-courses.module#PublishOnlineCoursesPageModule'
  },
  {
    path: 'publish-media-content',
    loadChildren: './publish-media-content/publish-media-content.module#PublishMediaContentPageModule'
  },
  {
    path: 'tv-tarify',
    loadChildren: './tariffs/tariffs.module#TariffsPageModule'
  },
  {
    path: 'tariffs-retail-month',
    loadChildren: './tariffs-retail-month/tariffs-retail-month.module#TariffsRetailMonthPageModule'
  },
  {
    path: 'tariffs-retail-year',
    loadChildren: './tariffs-retail-year/tariffs-retail-year.module#TariffsRetailYearPageModule'
  },
  {
    path: 'payment',
    loadChildren: './payment/payment.module#PaymentPageModule'
  },
  {
    path: 'user/subscriptions',
    redirectTo: 'private/user/tv-podpiski',
    pathMatch: 'full'
  }
];
