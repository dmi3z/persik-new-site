import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name: 'monthPipe'
})

export class MonthPipe implements PipeTransform {
    transform(value: number): number {
        if (value) {
            if (value < 0) {
                return 0;
            }

            if (value > 36) {
                return 36
            }

            return value;
        }
        return 0;
    }
}
