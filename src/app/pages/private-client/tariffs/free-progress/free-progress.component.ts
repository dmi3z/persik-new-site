import { Component, OnInit } from "@angular/core";
import { AuthService } from '../../../../services';
import * as moment from 'moment';
import { UserSubscription } from '../../../../models';

@Component({
    selector: 'app-free-progress',
    templateUrl: 'free-progress.component.html',
    styleUrls: ['free-progress.component.scss']
})

export class FreeProgressComponent implements OnInit {

    public currentPeriod: number;
    public progress: number;

    constructor(private authService: AuthService) { }

    ngOnInit() {
        this.calculate();
    }

    public get isSuccess(): boolean {
        return this.currentPeriod && this.currentPeriod >= 36;
    }

    private calculate(): void {
        this.authService.getUserSubscriptions().then(data => {
            /* const startedDateArrays = subscriptions.map(subs => moment(subs.started_at, 'YYYY-MM-DD HH:mm:ss').valueOf());
            const expiredDateArrays = subscriptions.map(subs => moment(subs.expired_at, 'YYYY-MM-DD HH:mm:ss').valueOf());
            const allPeriod = Math.abs(moment(Math.min(...startedDateArrays)).diff(Math.max(...expiredDateArrays), 'months'));

            console.log(allPeriod);

            let pauseBreak = 0;

            subscriptions.sort((a, b) => {
                return a.created_at.localeCompare(b.created_at);
            });

            subscriptions.forEach((item, index) => {
                if (subscriptions[index + 1]) {
                    const nextStart = moment(subscriptions[index + 1].started_at, 'YYYY-MM-DD HH:mm:ss').valueOf() / 1000;
                    const curStop = moment(item.expired_at, 'YYYY-MM-DD HH:mm:ss').valueOf() / 1000;
                    const diff = nextStart - curStop;
                    if (diff > 0) {
                        pauseBreak += diff;
                    }
                }
            });

            console.log(Math.round(pauseBreak / (30 * 24 * 3600)));

            this.currentPeriod = allPeriod - Math.round(pauseBreak / (30 * 24 * 3600));
            this.progress = this.currentPeriod / 36 * 100; */
            const subsData = JSON.parse(JSON.stringify(data));

            let subscriptions = subsData.map(item => {
                const startTime = moment(item.started_at, 'YYYY-MM-DD HH:mm:ss').valueOf();
                const endTime = moment(item.expired_at, 'YYYY-MM-DD HH:mm:ss').valueOf();
                item.started_at = startTime;
                item.expired_at = endTime;
                return item;
            });

            subscriptions.sort((a, b) => {
                return Number(a.started_at) - Number(b.started_at);
            });

            subscriptions = subscriptions.reduce((accum: UserSubscription[], item: UserSubscription) => {
                if (accum && accum[accum.length - 1]) {
                    const lastItem = accum[accum.length - 1];
                    if (item.expired_at > lastItem.expired_at) {
                        accum.push(item);
                        return accum;
                    }
                    return accum;
                } else {
                    accum.push(item);
                    return accum;
                }
            }, []);

            const subVectors: Array<{ start: number, end: number }> = subscriptions.reduce((accum: any[], item) => {
                if (accum.length === 0) {
                    accum.push({ start: item.started_at, end: item.expired_at });
                    return accum;
                }
                if (item.started_at <= accum[accum.length - 1].end) {
                    accum[accum.length - 1].end = item.expired_at;
                    return accum;
                } else {
                    accum.push({ start: item.started_at, end: item.expired_at });
                    return accum;
                }
            }, []);


            const periods: number[] = subVectors.map(item => {
                return Math.abs(moment(item.start).diff(item.end, 'months'));
            });

            this.currentPeriod = Math.max(...periods);
            this.progress = this.currentPeriod / 36 * 100;
        });
    }
}
