import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { FreeProgressComponent } from './free-progress.component';
import { MonthPipe } from './month.pipe';

@NgModule({
    declarations: [
        FreeProgressComponent,
        MonthPipe
    ],
    imports: [
        CommonModule
    ],
    exports: [
        FreeProgressComponent
    ]
})

export class FreeProgressModule { }
