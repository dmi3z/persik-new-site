import { PackageTariffOption } from "./../../../models/user";
import { Package } from "../../../models";

export const packages: Package[] = [
  // {
  //   id: 0,
  //   name: 'На месяц',
  //   items: [
  //     {
  //       id: 169,
  //       name: 'Подписка ТВ',
  //       cost: 5.9,
  //       first_month: 0.3,
  //       pay_avaliable: ['assist', 'yandex'],
  //       paymentCondition: 'Автопродление с возможностью отмены в любой момент',
  //     },
  //     {
  //       id: 126,
  //       name: 'ТВ',
  //       cost: 7.9,
  //       first_month: 0.3,
  //       pay_avaliable: ['assist', 'yandex'],
  //       paymentCondition: 'Автопродление с возможностью отмены в любой момент',
  //     },
  //     {
  //       id: 168,
  //       name: 'ТВ + Кино',
  //       cost: 9.9,
  //       first_month: 0.3,
  //       pay_avaliable: ['assist', 'yandex'],
  //       paymentCondition: 'Автопродление с возможностью отмены в любой момент',
  //     },
  //     {
  //       id: 180,
  //       name: 'ТВ + КИНО',
  //       cost: 12.9,
  //       first_month: 0.3,
  //       pay_avaliable: ['assist', 'yandex'],
  //       paymentCondition: 'Автопродление с возможностью отмены в любой момент',
  //     }
  //   ]
  // },
  {
    id: 1,
    name: "На месяц с тестовым периодом",
    items: [
      {
        id: 192,
        name: "ТВ",
        cost: 5.9,
        first_month: 0.3,
        pay_avaliable: ["assist"],
        customBigPrice: "Подключить тест на 1 месяц",
        customSmallPrice: "5,9 руб.",
        smallLinedText: "7,9",
        next: "Далее",
        paymentCondition:
          "После тестового периода услуга подключается на 1 год с ежемесячной оплатой",
        period:
          "30 дней за 30 копеек, с возможностью отключиться от услуги в течение тестового периода",
      },
      {
        id: 193,
        name: "ТВ + КИНО",
        cost: 8.9,
        first_month: 0.3,
        pay_avaliable: ["assist"],
        customBigPrice: "Подключить тест на 1 месяц",
        customSmallPrice: "8,9 руб.",
        smallLinedText: "12,9",
        next: "Далее",
        paymentCondition:
          "После тестового периода услуга подключается на 1 год с ежемесячной оплатой",
        period:
          "30 дней за 30 копеек, с возможностью отключиться от услуги в течение тестового периода",
      },
      {
        id: 259,
        name: "ТВ + КИНО",
        cost: "6 месяцев за 30 копеек",
        first_month: 0.3,
        pay_avaliable: ["assist"],
        customBigPrice: "Подключить тест на 6 месяцев",
        customSmallPrice: "9,9 руб.",
        next: "Далее",
        // smallLinedText: '12,9',
        paymentCondition:
          "После теста услуга подключается на 1 год с ежемесячной оплатой 9.9 руб/мес",
        period:
          "90 дней, с возможностью отключиться от услуги в течение тестового периода",
        description:
          "350+ каналов / 10000+ кино / архив передач 20 дней / Full HD",
      },
    ],
  },
  {
    id: 2,
    name: "На год",
    items: [
      /* {
        id: 50,
        name: 'Подписка ТВ',
        cost: 49.9,
        customBigPrice: '49,9 руб/год',
        customSmallPrice: '4,2 руб/мес.',
        // bigLinedText: '49.9',
        // imageSrc: 'assets/images/promo/green.png',
        // smallLinedText: '4,2',
        // paymentLinedText: true,
        period: 'На год'
      },
      {
        id: 226,
        name: 'Подписка ТВ',
        cost: 36.9,
        customBigPrice: '36,9 руб/год',
        customSmallPrice: '3,1 руб/мес.',
        bigLinedText: '49.9',
        // smallLinedText: '4,2',
        paymentLinedText: true,
        onlyPromo: true,
        period: 'На год'
      },*/
      {
        id: 176,
        name: "ТВ",
        cost: 49.9,
        customBigPrice: "49,9 руб/год",
        bigLinedText: "83,0",
        // smallLinedText: '5,8',
        customSmallPrice: "4.2 руб/мес.",
        imageSrc: "assets/images/promo/40-blue.png",
        paymentLinedText: true,
        period: "На год",
      },
      {
        id: 197,
        name: "ТВ",
        cost: 52.0,
        customBigPrice: "52,0 руб/год",
        bigLinedText: "69.9",
        // smallLinedText: '5,8',
        customSmallPrice: "5,8 руб/мес.",
        paymentLinedText: true,
        onlyPromo: true,
        period: "На год",
      },
      /* {
        id: 203,
        name: 'ТВ + Кино',
        cost: 99.9,
        customBigPrice: '99,9 руб/год',
        customSmallPrice: '8,3 руб/мес.',
        // bigLinedText: '99.9',
        // smallLinedText: '8,3',
        period: 'На год'
      },
      {
        id: 181,
        name: 'ТВ + Кино',
        cost: 74.9,
        customBigPrice: '99,9 руб/год',
        customSmallPrice: '8,3 руб/мес.',
        bigLinedText: '99.9',
        // smallLinedText: '8,3',
        paymentLinedText: true,
        onlyPromo: true,
        period: 'На год'
      }, */
      {
        id: 213,
        name: "ТВ + КИНО",
        cost: 99.9,
        customBigPrice: "99,9 руб/год",
        customSmallPrice: "8,3 руб/мес.",
        bigLinedText: "129.9",
        smallLinedText: "10,8",
        // imageSrc: 'assets/images/25.png',
        paymentLinedText: true,
        onlyPromo: true,
        period: "На год",
      },
      {
        id: 248,
        name: "ТВ + КИНО 1 год + 1 год в подарок",
        cost: 64.95,
        customBigPrice: "64,95 руб/2 года",
        customSmallPrice: "2.7 руб/мес.",
        bigLinedText: "129.9",
        // smallLinedText: '10,8',
        // imageSrc: 'assets/images/promo/50-red.png',
        imageSrc: "assets/images/promo/50-red.png",
        paymentLinedText: true,
        period: "На год",
      },
    ],
  },
  {
    id: 3,
    name: "На год + Android приставка",
    items: [
      /* {
        id: 188,
        name: 'Подписка ТВ',
        cost: 114.9,
        customBigPrice: '114,9 руб/год.',
        customSmallPrice: '',
        period: 'На год + Android приставка'
      }, */
      {
        id: 189,
        name: "ТВ",
        cost: 144.9,
        customBigPrice: "144,9 руб/год.",
        customSmallPrice: "",
        period: "На год + Android приставка",
      },
      /* {
        id: 190,
        name: 'ТВ + Кино',
        cost: 174.9,
        customBigPrice: '174,9 руб/год.',
        customSmallPrice: '',
        period: 'На год + Android приставка'
      }, */
      {
        id: 191,
        name: "ТВ + КИНО",
        cost: 204.9,
        customBigPrice: "204,9 руб/год.",
        customSmallPrice: "",
        period: "На год + Android приставка",
      },
    ],
  },
  {
    id: 4,
    name: "Кино на месяц",
    items: [
      // {
      //   id: 184,
      //   name: 'Всё кино',
      //   cost: 9.6,
      //   customBigPrice: '9.6 руб/мес.',
      //   customSmallPrice: '',
      //   period: 'На месяц'
      // },
      {
        id: 171,
        name: "Кино tvzavr",
        cost: 4.9,
        customBigPrice: "4.9 руб/мес.",
        customSmallPrice: "",
        period: "На месяц",
      },
      {
        id: 179,
        name: "Кино START",
        cost: 5.25,
        customBigPrice: "5.25 руб/мес.",
        customSmallPrice: "",
        period: "На месяц",
      },
    ],
  },
];

export const options: PackageTariffOption[] = [
  {
    // На месяц
    id: 169,
    states: [
      {
        name: "70 каналов",
        value: true,
      },
      {
        name: "Full HD качество",
        value: false,
      },
      {
        name: "UHD качество",
        value: false,
      },
      /* {
        name: 'Видеотека Persik',
        value: false
      }, */
      {
        name: "Кино и сериалы",
        value: false,
      },
      {
        name: "Кино START",
        value: false,
      },
      {
        name: "Архив 24 дня",
        value: true,
      },
      {
        name: "Мультирум",
        value: false,
      },
    ],
  },
  {
    id: 126,
    states: [
      {
        name: "350+ каналов",
        value: true,
      },
      {
        name: "Full HD качество",
        value: true,
      },
      {
        name: "UHD качество",
        value: false,
      },
      /* {
        name: 'Видеотека Persik',
        value: true
      }, */
      {
        name: "Кино и сериалы",
        value: false,
      },
      /* {
        name: 'Кино START',
        value: false
      }, */
      {
        name: "Архив 24 дня",
        value: true,
      },
      {
        name: "Мультирум",
        value: true,
      },
    ],
  },
  {
    id: 168,
    states: [
      {
        name: "350+ каналов",
        value: true,
      },
      {
        name: "Full HD качество",
        value: true,
      },
      {
        name: "UHD качество",
        value: false,
      },
      /* {
        name: 'Видеотека Persik',
        value: true
      }, */
      {
        name: "Кино и сериалы",
        value: true,
      },
      /* {
        name: 'Кино START',
        value: false
      }, */
      {
        name: "Архив 24 дня",
        value: true,
      },
      {
        name: "Мультирум",
        value: true,
      },
    ],
  },
  {
    id: 180,
    states: [
      {
        name: "350+ каналов",
        value: true,
      },
      {
        name: "Full HD качество",
        value: true,
      },
      {
        name: "UHD качество",
        value: true,
      },
      /* {
        name: 'Видеотека Persik',
        value: true
      }, */
      {
        name: "Кино и сериалы",
        value: true,
      },
      /* {
        name: 'Кино START',
        value: false
      }, */
      {
        name: "Архив 24 дня",
        value: true,
      },
      {
        name: "Мультирум",
        value: true,
      },
    ],
  }, // На год с помесячной
  {
    id: 192,
    states: [
      {
        name: "350+ каналов",
        value: true,
      },
      {
        name: "Full HD качество",
        value: true,
      },
      {
        name: "UHD качество",
        value: false,
      },
      /* {
        name: 'Видеотека Persik',
        value: true
      }, */
      {
        name: "Кино и сериалы",
        value: false,
      },
      /* {
        name: 'Кино START',
        value: false
      }, */
      {
        name: "Архив 24 дня",
        value: true,
      },
      {
        name: "Мультирум",
        value: true,
      },
      {
        name: "Тест 1 месяц за 30 копеек ",
        value: true,
      },
    ],
  },
  {
    id: 193,
    states: [
      {
        name: "350+ каналов",
        value: true,
      },
      {
        name: "Full HD качество",
        value: true,
      },
      {
        name: "UHD качество",
        value: true,
      },
      /* {
        name: 'Видеотека Persik',
        value: true
      }, */
      {
        name: "Кино и сериалы",
        value: true,
      },
      /* {
        name: 'Кино START',
        value: false
      }, */
      {
        name: "Архив 24 дня",
        value: true,
      },
      {
        name: "Мультирум",
        value: true,
      },
      {
        name: "Тест 1 месяц за 30 копеек ",
        value: true,
      },
    ],
  },
  {
    id: 259,
    states: [
      {
        name: "350+ каналов",
        value: true,
      },
      {
        name: "Full HD качество",
        value: true,
      },
      {
        name: "UHD качество",
        value: true,
      },
      /* {
        name: 'Видеотека Persik',
        value: true
      }, */
      {
        name: "Кино и сериалы",
        value: true,
      },
      /* {
        name: 'Кино START',
        value: false
      }, */
      {
        name: "Архив 24 дня",
        value: true,
      },
      {
        name: "Мультирум",
        value: true,
      },
      {
        name: "Тест 6 месяцев за 30 копеек ",
        value: true,
      },
    ],
  }, // На год
  {
    id: 50,
    states: [
      {
        name: "70 каналов",
        value: true,
      },
      {
        name: "Full HD качество",
        value: false,
      },
      {
        name: "UHD качество",
        value: false,
      },
      /* {
        name: 'Видеотека Persik',
        value: false
      }, */
      {
        name: "Кино и сериалы",
        value: false,
      },
      /* {
        name: 'Кино START',
        value: false
      }, */
      {
        name: "Архив 24 дня",
        value: true,
      },
      {
        name: "Мультирум",
        value: false,
      },
    ],
  },
  {
    id: 226,
    states: [
      {
        name: "70 каналов",
        value: true,
      },
      {
        name: "Full HD качество",
        value: false,
      },
      {
        name: "UHD качество",
        value: false,
      },
      /* {
        name: 'Видеотека Persik',
        value: false
      }, */
      {
        name: "Кино и сериалы",
        value: false,
      },
      /* {
        name: 'Кино START',
        value: false
      }, */
      {
        name: "Архив 24 дня",
        value: true,
      },
      {
        name: "Мультирум",
        value: false,
      },
    ],
  },
  /* {
    id: 50,
    states: [
      {
        name: '70 каналов',
        value: true
      },
      {
        name: 'Full HD качество',
        value: false
      },
      {
        name: 'UHD качество',
        value: false
      },
      {
        name: 'Видеотека Persik',
        value: false
      },
      {
        name: 'Кино и сериалы',
        value: false
      },
      {
        name: 'Кино START',
        value: false
      },
      {
        name: 'Архив 24 дня',
        value: true
      },
      {
        name: 'Мультирум',
        value: false
      }
    ]
  }, */
  {
    id: 176,
    states: [
      {
        name: "350+ каналов",
        value: true,
      },
      {
        name: "Full HD качество",
        value: true,
      },
      {
        name: "UHD качество",
        value: false,
      },
      /* {
        name: 'Видеотека Persik',
        value: true
      }, */
      {
        name: "Кино и сериалы",
        value: false,
      },
      /* {
        name: 'Кино START',
        value: false
      }, */
      {
        name: "Архив 24 дня",
        value: true,
      },
      {
        name: "Мультирум",
        value: true,
      },
    ],
  },
  {
    id: 197,
    states: [
      {
        name: "350+ каналов",
        value: true,
      },
      {
        name: "Full HD качество",
        value: true,
      },
      {
        name: "UHD качество",
        value: false,
      },
      /* {
        name: 'Видеотека Persik',
        value: true
      }, */
      {
        name: "Кино и сериалы",
        value: false,
      },
      /* {
        name: 'Кино START',
        value: false
      }, */
      {
        name: "Архив 24 дня",
        value: true,
      },
      {
        name: "Мультирум",
        value: true,
      },
    ],
  },

  {
    id: 203,
    states: [
      {
        name: "350+ каналов",
        value: true,
      },
      {
        name: "Full HD качество",
        value: true,
      },
      {
        name: "UHD качество",
        value: false,
      },
      /* {
        name: 'Видеотека Persik',
        value: true
      }, */
      {
        name: "Кино и сериалы",
        value: true,
      },
      /* {
        name: 'Кино START',
        value: false
      }, */
      {
        name: "Архив 24 дня",
        value: true,
      },
      {
        name: "Мультирум",
        value: true,
      },
    ],
  },
  {
    id: 181,
    states: [
      {
        name: "350+ каналов",
        value: true,
      },
      {
        name: "Full HD качество",
        value: true,
      },
      {
        name: "UHD качество",
        value: false,
      },
      /* {
        name: 'Видеотека Persik',
        value: true
      }, */
      {
        name: "Кино и сериалы",
        value: true,
      },
      /* {
        name: 'Кино START',
        value: false
      }, */
      {
        name: "Архив 24 дня",
        value: true,
      },
      {
        name: "Мультирум",
        value: true,
      },
    ],
  },
  {
    id: 248,
    states: [
      {
        name: "350+ каналов",
        value: true,
      },
      {
        name: "Full HD качество",
        value: true,
      },
      {
        name: "UHD качество",
        value: true,
      },
      /* {
        name: 'Видеотека Persik',
        value: true
      }, */
      {
        name: "Кино и сериалы",
        value: true,
      },
      /* {
        name: 'Кино START',
        value: false
      }, */
      {
        name: "Архив 24 дня",
        value: true,
      },
      {
        name: "Мультирум",
        value: true,
      },
    ],
  },
  {
    id: 213,
    states: [
      {
        name: "350+ каналов",
        value: true,
      },
      {
        name: "Full HD качество",
        value: true,
      },
      {
        name: "UHD качество",
        value: true,
      },
      /* {
        name: 'Видеотека Persik',
        value: true
      }, */
      {
        name: "Кино и сериалы",
        value: true,
      },
      /* {
        name: 'Кино START',
        value: false
      }, */
      {
        name: "Архив 24 дня",
        value: true,
      },
      {
        name: "Мультирум",
        value: true,
      },
    ],
  }, // Year + Android
  {
    id: 188,
    states: [
      {
        name: "70 каналов",
        value: true,
      },
      {
        name: "Full HD качество",
        value: false,
      },
      {
        name: "UHD качество",
        value: false,
      },
      /* {
        name: 'Видеотека Persik',
        value: false
      }, */
      {
        name: "Кино и сериалы",
        value: false,
      },
      /* {
        name: 'Кино START',
        value: false
      }, */
      {
        name: "Архив 24 дня",
        value: true,
      },
      {
        name: "Мультирум",
        value: false,
      },
    ],
  },
  {
    id: 189,
    states: [
      {
        name: "350+ каналов",
        value: true,
      },
      {
        name: "Full HD качество",
        value: true,
      },
      {
        name: "UHD качество",
        value: false,
      },
      /* {
        name: 'Видеотека Persik',
        value: true
      }, */
      {
        name: "Кино и сериалы",
        value: false,
      },
      /* {
        name: 'Кино START',
        value: false
      }, */
      {
        name: "Архив 24 дня",
        value: true,
      },
      {
        name: "Мультирум",
        value: true,
      },
    ],
  },
  {
    id: 190,
    states: [
      {
        name: "350+ каналов",
        value: true,
      },
      {
        name: "Full HD качество",
        value: true,
      },
      {
        name: "UHD качество",
        value: false,
      },
      /* {
        name: 'Видеотека Persik',
        value: true
      }, */
      {
        name: "Кино и сериалы",
        value: true,
      },
      /* {
        name: 'Кино START',
        value: false
      }, */
      {
        name: "Архив 24 дня",
        value: true,
      },
      {
        name: "Мультирум",
        value: true,
      },
    ],
  },
  {
    id: 191,
    states: [
      {
        name: "350+ каналов",
        value: true,
      },
      {
        name: "Full HD качество",
        value: true,
      },
      {
        name: "UHD качество",
        value: true,
      },
      /* {
        name: 'Видеотека Persik',
        value: true
      }, */
      {
        name: "Кино и сериалы",
        value: true,
      },
      /* {
        name: 'Кино START',
        value: false
      }, */
      {
        name: "Архив 24 дня",
        value: true,
      },
      {
        name: "Мультирум",
        value: true,
      },
    ],
  },
  // films to month
  {
    id: 184,
    states: [],
  },
  {
    id: 171,
    states: [],
  },
  {
    id: 179,
    states: [],
  },
];
