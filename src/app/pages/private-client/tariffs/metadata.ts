import { MetaDefinition } from '@angular/platform-browser';
import { MetaData } from '../../../models/meta';

const tags: MetaDefinition[] = [
  {
    name: 'keywords',
    content: 'персик тв тарифы, тарифы на телевидение'
  },
  {
    name: 'description',
    content: `Выгодные тарифы на телевидение Персик ТВ! Большой выбор пакетов услуг на месяц, год.
      ✅ Первый месяц всего за 30 копеек! ✅ Архив 24 дня, Full HD качество, 100 каналов. info@persik.by`
  }
];

const title = 'Тарифы на интернет-телевидение TV Persik, подписки на ТВ-каналы';

export const meta: MetaData = {
  title,
  tags
};
