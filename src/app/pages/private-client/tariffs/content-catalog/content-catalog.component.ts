import { Component, Input, ChangeDetectionStrategy, ChangeDetectorRef, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { DataService } from '../../../../services';

@Component({
    selector: 'app-content-catalog',
    templateUrl: 'content-catalog.component.html',
    styleUrls: ['content-catalog.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class ContentCatalogComponent implements OnInit, OnChanges {

    private skip = 0;

    @Input() data: ContentCatalogData;
    @Input() loadCount: number;
    public content: any[] = [];

    constructor(private dataService: DataService, private cdr: ChangeDetectorRef) { }

    ngOnInit() {
        this.loadContentFeatures();
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.loadCount.previousValue && changes.loadCount.currentValue !== changes.loadCount.previousValue) {
            this.content = [];
            this.loadContentFeatures();
        }
    }

    public showMoreContent(): void {
        this.loadContentFeatures();
    }

    public getId(item: any): number | string {
        if (item.video_id) {
            return item.video_id;
        }
        if (item.tvshow_id) {
            return item.tvshow_id;
        }
        return null;
    }
    
    public get isHaveContent(): boolean {
        return this.content.length > 0;
    }

    private loadContentFeatures(): void {
        this.dataService.getVideoContent(this.data.categoryId, this.data.genreId, this.loadCount, this.skip).then(res => {
            this.content.push(...res.videos);
            this.cdr.markForCheck();
            this.skip += this.loadCount;
        });
    }
}

export interface ContentCatalogData {
    categoryId: number;
    genreId: number;
    title: string;
    subtitle: string;
}
