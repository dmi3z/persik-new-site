import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TariffsComponent } from './tariffs.component';
import { FilmCardModule } from '../../../components/content-slider/film-card/film-card.module';
import { FreeProgressModule } from './free-progress/free-progress.module';
import { ContentCatalogComponent } from './content-catalog/content-catalog.component';

@NgModule({
  declarations: [
    TariffsComponent,
    ContentCatalogComponent
  ],
  imports: [
    CommonModule,
    FilmCardModule,
    FreeProgressModule,
    RouterModule.forChild([
      {
        path: '',
        component: TariffsComponent
      }
    ])
  ]
})

export class TariffsPageModule {}

