import { Subject } from 'rxjs/Subject';
import { Option } from './../../../models/tariff';
import { takeUntil } from 'rxjs/operators';
import { Channel } from './../../../models';
import { Component, OnInit, HostListener, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { CheckWidthService, PaymentService, DataService, MetaService, AnalyticService, AuthService } from '../../../services';
import { meta } from './metadata';
import { ContentCatalogData } from './content-catalog/content-catalog.component';
import { Product } from '../../../models/tariff';

@Component({
  selector: 'app-tariffs',
  templateUrl: './tariffs.component.html',
  styleUrls: ['./tariffs.component.scss']
})

export class TariffsComponent implements OnInit, OnDestroy {
  public products: Product[] = [];
  public allChannelLogos: string[] = [];
  public channelLogosForPage: string[] = [];
  public isHaveMovement: boolean;
  public activeProduct: Product;
  public selectedOption: Option;
  public channelLogosCount = 40;
  public mobileTariffsPackOpen: boolean;
  public mobileTariffsOpen: boolean;
  public isAuth: boolean;
  public loadCount = 12;

  private windowWidth: number;
  private activeProductId: number;
  private destroy$ = new Subject();

  // ------- data for content catalogs ------

  public videosData: ContentCatalogData = {
    categoryId: 1,
    genreId: 881,
    title: 'Видеокаталог',
    subtitle: 'В наших тарифах вы сможете купить обновляемую видеотеку фильмов: более 10000 единиц контента.'
  };

  public seriesData: ContentCatalogData = {
    categoryId: 2,
    genreId: 886,
    title: 'Сериалы',
    subtitle: 'В наших тарифах вы сможете купить обновляемую видеотеку сериалов: более 10000 единиц контента.'
  };

  public cartoonsData: ContentCatalogData = {
    categoryId: 3,
    genreId: 890,
    title: 'Мультфильмы',
    subtitle: 'В наших тарифах вы сможете купить обновляемую видеотеку мультфильмов: более 10000 единиц контента.'
  };

  public tvshowsData: ContentCatalogData = {
    categoryId: 4,
    genreId: 0,
    title: 'Тв передачи',
    subtitle: 'Подписка на тв каналы позволит смотреть передачи, новости, спорт в режиме реального времени, а также в архиве на любом устройстве — от телевизора до смартфона/планшета.'
  };

  // ----------------------------------------

  constructor(
    private dataService: DataService,
    private router: Router,
    private paymentService: PaymentService,
    private checkWindowService: CheckWidthService,
    private metaService: MetaService,
    private analyticService: AnalyticService,
    private authService: AuthService
  ) {
    this.metaService.updateMeta(meta);
  }

  @HostListener('window:scroll', ['$event'])
  checkScroll() {
    const scrollPosition = window.pageYOffset;
    this.isHaveMovement = scrollPosition >= 720;
  }

  ngOnInit(): void {
    this.dataService.getTariffs().subscribe(tariffs => {
      this.products = tariffs.products;
      this.activeProduct = this.products[0];
      this.activeProductId = this.activeProduct.product_id;
      this.selectedOption = this.activeProduct.options[0];
      this.paymentService.setPaymentMethods(tariffs.pay_sys);
    });
    this.isAuth = this.authService.isLogin;
    this.windowWidth = this.checkWindowService.getWidth();
    this.calcChannels(this.windowWidth);
    this.loadChannelLogos(this.channelLogosCount);
    this.checkWindowService.screenWidthEvent.pipe(takeUntil(this.destroy$)).subscribe(width => this.calcChannels(width));
  }

  private calcChannels(width): void {
    switch (true) {
      case width > 1600:
        this.channelLogosCount = 40;
        this.channelLogosForPage = this.allChannelLogos.slice(0, this.channelLogosCount);
        this.loadCount = 12;
        break;

      case width <= 1600 && width >= 1401:
        this.channelLogosCount = 32;
        this.channelLogosForPage = this.allChannelLogos.slice(0, this.channelLogosCount);
        this.loadCount = 10;
        break;

      case width <= 1400 && width >= 1201:
        this.channelLogosCount = 24;
        this.channelLogosForPage = this.allChannelLogos.slice(0, this.channelLogosCount);
        this.loadCount = 8;
        break;

      case width <= 1200 && width >= 993:
        this.channelLogosCount = 20;
        this.channelLogosForPage = this.allChannelLogos.slice(0, this.channelLogosCount);
        this.loadCount = 6;
        break;

      case width <= 992 && width >= 768:
        this.channelLogosCount = 16;
        this.channelLogosForPage = this.allChannelLogos.slice(0, this.channelLogosCount);
        break;

      case width <= 767:
        this.channelLogosCount = 12;
        this.channelLogosForPage = this.allChannelLogos.slice(0, this.channelLogosCount);
        break;

      default:
        this.channelLogosCount = 40;
        this.channelLogosForPage = this.allChannelLogos.slice(0, this.channelLogosCount);
        this.loadCount = 12;
        break;
    }
  }

  public toggleMobileTariffsPack(): void {
    this.mobileTariffsPackOpen = !this.mobileTariffsPackOpen;
  }

  public toggleMobileTariffs(): void {
    this.mobileTariffsOpen = !this.mobileTariffsOpen;
  }

  public isActivePackage(id: number): boolean {
    return this.activeProductId === id;
  }

  public setActivePackage(id: number): void {
    this.activeProductId = id;
    this.activeProduct = this.products.find(pack => pack.product_id === id);

    this.selectedOption = this.activeProduct.options[0];
  }

  public selectTariff(tariff: Option): void {
    this.selectedOption = tariff;
  }

  public isTariffSelected(tariff_id: number): boolean {
    return this.selectedOption && this.selectedOption.product_option_id === tariff_id;
  }

  public get isHaveSelectedTariff(): boolean {
    return !!this.selectedOption;
  }

  public get isShowAllChannels(): boolean {
    return (this.channelLogosForPage.length === this.allChannelLogos.length && this.channelLogosForPage.length > 0);
  }

  public toggleChannels(channelsCount: number): void {
    if (this.isShowAllChannels) {
      this.channelLogosForPage = this.allChannelLogos.slice(0, channelsCount);
    } else {
      this.channelLogosForPage = this.allChannelLogos;
    }
  }

  public goToPay(event: MouseEvent): void {
    this.analyticService.targetSend('vybrat', 47723698);
    event.stopPropagation();
    const activeProduct: Product = this.products.find(pack => pack.product_id === this.activeProductId);

    this.paymentService.setTariff(this.selectedOption, activeProduct.name);
    this.router.navigate(['private/payment']);
  }

  private loadChannelLogos(channelsCount: number): void {
    this.dataService.getChannels().then(channels => {
      const excludesChannelsId = [111, 115, 118, 148, 883, 10431, 10200];
      const withoutTourism: Channel[] = channels.filter(channel => !channel.genres.includes(680));
      const finallyFilter: Channel[] = withoutTourism.filter(channel => !excludesChannelsId.includes(channel.channel_id));
      this.allChannelLogos = finallyFilter.map(channel => channel.logo);
      this.channelLogosForPage = this.allChannelLogos.slice(0, channelsCount);
    });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
