import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { PlayerModule } from '../../components/player/player.module';
import { AdultControlModule } from '../../components/adult-control/adult-control.module';
import {
  ChannelPlayerComponent,
  // AdultControlComponent
} from './';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    ChannelPlayerComponent,
    // AdultControlComponent
  ],
  imports: [
    AdultControlModule,
    FormsModule,
    CommonModule,
    PlayerModule,
    RouterModule.forChild([
      {
        path: '',
        component: ChannelPlayerComponent
      }
    ])
  ]
})

export class ChannelPlayerModule {}
