import { SpeedTestService } from './../../services/speed-test.service';
import { SpeedTestPageComponent } from './speed-test.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    SpeedTestPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: SpeedTestPageComponent
      }
    ])
  ],
  providers: [
    SpeedTestService
  ],
  exports: [RouterModule]
})

export class SpeedTestModule {

}
