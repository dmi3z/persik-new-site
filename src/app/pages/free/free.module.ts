import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FreePageComponent } from './free.component';

@NgModule({
  declarations: [
    FreePageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: FreePageComponent
      }
    ])
  ]
})

export class FreePageModule {}

