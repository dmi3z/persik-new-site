import { MetaService } from './../../services/meta.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-free-component',
  templateUrl: './free.component.html',
  styleUrls: ['./free.component.scss']
})

export class FreePageComponent implements OnInit {

  constructor(private metaService: MetaService) {}

    ngOnInit() {
      this.metaService.setDefaultMeta('Смотреть бесплатно');
    }
}
