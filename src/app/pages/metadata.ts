import { MetaDefinition } from '@angular/platform-browser';
import { MetaData } from '../models/meta';

const tags: MetaDefinition[] = [
    {
      name: 'description',
      content: `Система генерации (СГК) - тв Persik`
    }
  ];

const title = 'Система генерации (СГК) - телевидение Persik';

export const meta: MetaData = {
  title,
  tags
};

