import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { IptvPageComponent } from './iptv.component';

@NgModule({
  declarations: [
    IptvPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: IptvPageComponent
      }
    ])
  ]
})

export class IptvModule {}

