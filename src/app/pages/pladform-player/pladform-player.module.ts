import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PladformPlayerComponent } from './pladform-player.component';

@NgModule({
  declarations: [
    PladformPlayerComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: PladformPlayerComponent
      }
    ])
  ]
})

export class PladformPlayerModule {}
