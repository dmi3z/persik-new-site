import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-pladform-player',
  templateUrl: './pladform-player.component.html',
  styleUrls: ['./pladform-player.component.scss']
})

export class PladformPlayerComponent implements OnInit, AfterViewInit {

  private id: number;

  constructor(private activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.params.subscribe(param => {
      if (param['id']) {
        this.id = +param['id'];
      }
    });
  }

  ngAfterViewInit() {
    this.createIFrame(this.id);
  }

  private createIFrame(id: number): void {
    const frame = document.createElement('iframe');
    const pladform = document.getElementById('pladform');
    frame.setAttribute('src', `//out.pladform.ru/player?pl=45367&videoid=${id}`);
    frame.setAttribute('webkitAllowFullScreen', '');
    frame.setAttribute('mozallowfullscreen', '');
    frame.setAttribute('allowfullscreen', '');
    frame.setAttribute('frameborder', '0');
    frame.className = '_3WIHbcqSpZ0sOHly _1uSNPo8nDaWhWQnE';
    frame.style.width = '100%';
    frame.style.height = '100%';
    pladform.appendChild(frame);
  }
}
