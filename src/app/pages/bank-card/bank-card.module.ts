import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BankCardPageComponent } from './bank-card.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    BankCardPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: BankCardPageComponent
      }
    ])
  ],
  exports: [
    BankCardPageComponent
  ]
})

export class BankCardPageModule {}
