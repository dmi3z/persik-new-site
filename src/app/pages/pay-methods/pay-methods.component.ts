import { MetaService } from './../../services/meta.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pay-methods-component',
  templateUrl: './pay-methods.component.html',
  styleUrls: ['./pay-methods.component.scss']
})

export class PayMethodsPageComponent implements OnInit {

  constructor(private router: Router, private metaService: MetaService) {}

  ngOnInit() {
    this.metaService.setDefaultMeta('Способы оплаты');
  }

  public goTo(path: string): void {
    this.router.navigate([path]);
  }
}
