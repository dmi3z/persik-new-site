import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PayMethodsPageComponent } from './pay-methods.component';

@NgModule({
  declarations: [
    PayMethodsPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: PayMethodsPageComponent
      }
    ])
  ]
})

export class PayMethodsPageModule {}

