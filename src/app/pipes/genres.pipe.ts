import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'genres'
})

export class GenresPipe implements PipeTransform {
  transform(value: string[]) {
    if (value && value.length > 0) {
      // const firstGenre: string = value[0];
      // firstGenre[0].toUpperCase() + firstGenre[0].slice(1);
      const genresString: string = value.toString();
      return genresString.replace(new RegExp(',', 'g'), ', ');
    }
    return '';
  }
}
