import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'expiredDays'
})

export class ExpiredPipe implements PipeTransform {
  transform(value: string) {
    const currentTime: number = moment().unix();
    const expiredAt: number = moment(value, 'YYYY-MM-DD HH:mm:ss').unix();
    if (expiredAt > currentTime) {
      return Math.round((expiredAt - currentTime) / 86400);
    } else {
      return 'Окончена';
    }
  }
}
