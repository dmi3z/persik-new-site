import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'renamePipe'
})

export class RenamePipe implements PipeTransform {
  transform(value: string) {
    if (value && value.length > 0) {
      const title: string = value.toLowerCase();
      if (title.includes('фильмы')) {
        return 'Недавно добавленные фильмы';
      }
      if (title.includes('сериалы')) {
        return 'Популярные сериалы';
      }
      if (title.includes('передачи')) {
        return 'Передачи вслед за эфиром';
      }
    }
    return value;
  }
}
