import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'subscribeDate'
})

export class SubscribePipe implements PipeTransform {
  transform(value: string) {
    moment.locale('ru');
    const date = moment(value.split(' ')[0], 'YYYY-MM-DD');
    return date.format('MMMM D');
  }
}
