import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'duration'
})

export class DurationPipe implements PipeTransform {
  transform(value: number) {
    return this.getFormattedTime(+value);
  }

  private getFormattedTime(time: number): string {
    if (time) {
        const hours: number = Math.floor(time / 3600);
        const minutes: number = Math.floor((time - hours * 3600) / 60);
        return `${this.checkZero(hours)}:${this.checkZero(minutes)}`;
    }
    return '';
  }

  private checkZero(value: number): string {
      return `${value > 9 ? value : '0' + value}`;
  }
}
