import { NgModule } from '@angular/core';
import {
  GenresPipe,
  TvshowTimePipe,
  DurationPipe,
  FullDurationPipe,
  SubscribePipe,
  ExpiredPipe,
  DatePipe,
  ArchiveAtPipe,
  RenamePipe,
  RoundPipe
} from './';

@NgModule({
  declarations: [
    GenresPipe,
    TvshowTimePipe,
    DurationPipe,
    FullDurationPipe,
    SubscribePipe,
    ExpiredPipe,
    DatePipe,
    ArchiveAtPipe,
    RenamePipe,
    RoundPipe
  ],
  exports: [
    GenresPipe,
    TvshowTimePipe,
    DurationPipe,
    FullDurationPipe,
    SubscribePipe,
    ExpiredPipe,
    DatePipe,
    ArchiveAtPipe,
    RenamePipe,
    RoundPipe
  ]
})

export class PipesModule {}
