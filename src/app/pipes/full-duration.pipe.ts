import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'fullDuration'
})

export class FullDurationPipe implements PipeTransform {
    transform(value: string | number) {
        return this.getFormattedTime(+value);
    }

    private getFormattedTime(time: number): string {
        if (time) {
            const hours: number = Math.floor(time / 3600);
            const minutes: number = Math.floor((time - hours * 3600) / 60);
            const seconds: number = Math.floor(time - hours * 3600 - minutes * 60);
            return `${this.checkZero(hours)}:${this.checkZero(minutes)}:${this.checkZero(seconds)}`;
        }
        return '';
    }

    private checkZero(value: number): string {
        return `${value > 9 ? value : '0' + value}`;
    }
}
