import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'archiveAt'
})

export class ArchiveAtPipe implements PipeTransform {
  transform(value: number) {
    if (value && value > 0) {
      moment.locale('ru');
      return moment.unix(value).format('DD.MM.YYYY, HH:mm');
    }
    return '';
  }
}
