import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'roundPipe'
})

export class RoundPipe implements PipeTransform {
  transform(value: number, tail: number) {
    if (value) {
      if (tail) {
        return value.toFixed(tail);
      }
      return value.toFixed(2);
    }
    return '';
  }
}
