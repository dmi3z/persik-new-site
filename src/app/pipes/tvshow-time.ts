import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
    name: 'tvshowTime'
})

export class TvshowTimePipe implements PipeTransform {
    transform(value: string | number) {
        return this.getFormattedTime(+value);
    }

    private getFormattedTime(time: number): string {
        if (time) {
            return moment.unix(time).format('HH:mm');
        }
        return '';
    }
}
