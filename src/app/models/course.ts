import { Video } from './vod';
export interface Course {
    id: number;
    type: string;
    name: string;
    price: number;
    desc: string;
    video?: string;
    skills?: Skill[];
    requirements?: string[];
    seasons?: Season[];
    teachers?: Teacher[];
    anotherCourses?: number[];
    comments?: Comment[];
    cover?: string;
}

export interface Skill {
    title: string;
    content?: string;
    icon?: string;
}

export interface Season {
    title: string;
    desc?: string;
    counterText?: string;
    lessons: Video[];
}

export interface Teacher {
    agency: string;
    agencyImg: string;
    photo: string;
    name: string;
    exp?: string;
    stuffing?: string;
    graduates?: string;
}

export interface CoursePreview {
    id: number;
    type: string;
    photo: string;
    name: string;
    theoryTime?: string;
    practiceTime?: string;
    lessonsCount?: number;
}

export interface Comment {
    userName: string;
    userJob: string;
    text: string;
    photo?: string;
}
