export interface SliderOptions {
    slidesPerView: number;
    spaceBetween: number;
    freeMode: boolean;
    autoplay?: number;
    loop?: boolean;
    centeredSlides?: true;
    direction?: 'horizontal' | 'vertical';
}