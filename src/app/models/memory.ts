export class ContentInMemory {
    content_id: any;
    time: number;

    constructor(content_id: any, time: number) {
        this.content_id = content_id;
        this.time = time;
    }
}