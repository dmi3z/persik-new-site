import { ContentType } from './vod';

export interface HardCodeItem {
  id: number | string;
  type: ContentType;
  image: string;
  name: string;
}
