export class NavigationOptions {
    page: string;
    from: string;
    options: any;

    constructor(page: string, from?: string, options?: any) {
        this.page = page;
        this.from = from;
        this.options = options;
    }
}