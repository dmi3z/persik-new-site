export namespace HeaderButton {
    export const BUTTON_MENU = 'menu';
    export const BUTTON_BACK = 'back';
    export const BUTTON_SEARCH = 'search';
    export const BUTTON_FAVORITE = 'favorite';
    export const BUTTON_FILTER = 'filter';
}

export type HeaderButtonType = 'menu' | 'back' | 'search' | 'favorite' | 'filter';