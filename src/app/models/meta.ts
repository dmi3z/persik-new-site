import { MetaDefinition } from '@angular/platform-browser';

export interface MetaData {
  title?: string;
  tags?: MetaDefinition[];
}
