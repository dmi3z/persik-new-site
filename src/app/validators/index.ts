export { confirmPasswordValidator } from './confirm-password.validator';
export { emailCorrectValidator } from './email-correct.validator';
export { emailExistValidator } from './email-exist.validator';
export { emailNotExistValidator } from './email-notexist.validator';
export { phoneValidator } from './phone.validator';
