import { FormControl, ValidatorFn, AbstractControl } from '@angular/forms';

export function phoneValidator(): ValidatorFn {

  const pattern = /[0-9]*/i;
  return (control: AbstractControl): {[key: string]: any } => {
    if (!(control.dirty || control.touched)) {
      return null;
    } else {
      return pattern.test(control.value) ? null : { custom: 'Invalid phone number'};
    }
  };
}
