import { TimeService } from '../services/time.service';
import { Tvshow } from './../models/tvshow';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class ImageService {

    constructor(private timeService: TimeService) {}

    getChannelFrame(id: number, time: number, transform: string = 'crop', width: number = 400, height: number = 250): string {
        const t = Math.round(time);
        return `https://old.persik.by/utils/show-frame.php?c=${id}&t=${t}&tr=${transform}&w=${width}&h=${height}`;
    }

    getChannelLogo(id: number) {
        return `https://persik.by/media/channels/logos/${id}.png`;
    }

    getTvshowFrame(tvshow: Tvshow): string {
      const currentTime: number = this.timeService.currentTime;
        const time = +tvshow.start + ((tvshow.stop - tvshow.start) / 2);
        if (+tvshow.start <= currentTime && +tvshow.stop > currentTime) {
          return this.getChannelFrame(tvshow.channel_id, currentTime);
        }
        if (+tvshow.stop < currentTime) {
          return this.getChannelFrame(tvshow.channel_id, time);
        }
        return '';
    }
}
