import { AuthService } from '../services';
import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

@Injectable()
export class RetailAuthGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {}

  canActivate() {
    const isLogin: boolean = this.authService.isRetailLogin;
    if (isLogin) {
      return true;
    } else {
      this.router.navigate(['retail/auth']);
    }
  }
}
