import { Channel } from '../../../models';

export interface MediaStateData {
  id: string;
  type: string;
  state: string; // inProgress, finished, channel
  channel?: Channel;
  seek: number;
  timestamp: number;
}
