import { MediaStateData } from './mediaStateData';

export type MediaState = Array<MediaStateData>;
