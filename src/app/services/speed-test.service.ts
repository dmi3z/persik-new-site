import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()

export class SpeedTestService {

  public streamSpeed: number;

  constructor() {}

  public speed_emitter: Subject<SpeedData> = new Subject();

  private readonly TV_URL = 'http://st-by.persik.by/random3000x3000.jpg';
  private readonly ARC_URL = 'http://st-dc.persik.by/random3000x3000.jpg';
  private readonly IMG_SIZE = 17399000;
  private startTime: number;
  private speedData: SpeedData = {
    tv_speed: 0,
    arch_speed: 0
  };

  public checkTVSpeed(): void {
    const download = new Image();
    this.startTime = (new Date()).getTime();
    const cacheBuster = this.TV_URL + '?st=' + this.startTime;
    download.src = cacheBuster;
    download.addEventListener('load', onloadEvent.bind(this));

    function onloadEvent() {
      const endTime = (new Date()).getTime();
      const duration = (endTime - this.startTime) / 1000;
      const bitsLoaded = this.IMG_SIZE * 8;
      const speedBps = +(bitsLoaded / duration).toFixed(2);
      const speedKbps = +(speedBps / 1024).toFixed(2);
      const speedMbps = (speedKbps / 1024).toFixed(2);
      this.speedData.tv_speed = speedMbps;
      this.speed_emitter.next(this.speedData);
    }
  }

  public checkArchiveSpeed(): void {
    const download = new Image();
    this.startTime = (new Date()).getTime();
    const cacheBuster = this.ARC_URL + '?st=' + this.startTime;
    download.src = cacheBuster;
    download.addEventListener('load', onloadEvent.bind(this));

    function onloadEvent() {
      const endTime = (new Date()).getTime();
      const duration = (endTime - this.startTime) / 1000;
      const bitsLoaded = this.IMG_SIZE * 8;
      const speedBps = +(bitsLoaded / duration).toFixed(2);
      const speedKbps = +(speedBps / 1024).toFixed(2);
      const speedMbps = (speedKbps / 1024).toFixed(2);
      this.speedData.arch_speed = speedMbps;
      this.speed_emitter.next(this.speedData);
    }
  }

}

export interface SpeedData {
  tv_speed: number;
  arch_speed: number;
}
