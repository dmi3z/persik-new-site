import { TimeService } from './time.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { FavoriteChannel, FavoriteVideo, FavoriteTvshow, FavoriteSettings, FavoriteBook } from '../models/favorite';
import { ContentName } from '../models';


@Injectable({ providedIn: 'root' })
export class FavoriteService {

    private readonly BASE_URL = 'https://api.persik.by/';

    private channels: FavoriteChannel[] = [];
    private tvshows: FavoriteTvshow[] = [];
    private videos: FavoriteVideo[] = [];
    private books: FavoriteBook[] = [];

    constructor(private http: HttpClient, private timeService: TimeService) {
      if (this.channels.length === 0) {
        this.loadFavoriteChannels();
      }
      if (this.videos.length === 0) {
        this.loadFavoriteVideos();
      }
      if (this.tvshows.length === 0) {
        this.loadFavoriteTvshows();
      }
      if (this.books.length === 0) {
        this.loadFavoriteBooks();
      }
    }

    private loadFavoriteChannels(): void {
      this.http.get<FavoriteChannels>(this.BASE_URL.concat('v2/favorite/channels')).map(data => data.channels).toPromise().then(res => {
        this.channels = res;
      }).catch(_ => console.log('Get favorite channels error.'));
    }

    private loadFavoriteVideos(): void {
      this.http.get<FavoriteVideos>(this.BASE_URL.concat('v2/favorite/videos')).map(data => data.videos).toPromise().then(result => {
        this.videos = result;
      }).catch(_ => console.log('Get favorite videos error.'));
    }

    private loadFavoriteTvshows(): void {
      this.http.get<FavoriteTvshows>(this.BASE_URL.concat('v2/favorite/tvshows')).map(data => data.tvshows).toPromise().then(result => {
        this.tvshows = result;
      }).catch(_ => console.log('Get favorite tvshows error.'));
    }

    private loadFavoriteBooks(): void {
      this.http.get<FavoriteBooks>(this.BASE_URL.concat('v2/favorite/litres-items')).map(data => data.litres).toPromise().then(result => {
        this.books = result;
      }).catch(_ => console.log('Get favorite tvshows error.'));
    }

    getFavoriteChannels(): Promise<FavoriteChannel[]> {
      return new Promise(resolve => {
        if (this.channels.length > 0) {
          resolve(this.channels);
        } else {
          this.http.get<FavoriteChannels>(this.BASE_URL.concat('v2/favorite/channels')).map(data => data.channels).toPromise().then(res => {
            this.channels = res;
            resolve(res);
          });
        }
      });
    }

    getFavoriteVideos(): Promise<FavoriteVideo[]> {
      return new Promise(resolve => {
        if (this.videos.length > 0) {
          resolve(this.videos);
        } else {
          this.http.get<FavoriteVideos>(this.BASE_URL.concat('v2/favorite/videos')).map(data => data.videos)
            .toPromise().then(result => {
              this.videos = result;
              resolve(result);
            });
        }
      });
    }

    getFavoriteTvshows(): Promise<FavoriteTvshow[]> {
      return new Promise(resolve => {
        if (this.tvshows.length > 0) {
          resolve(this.tvshows);
        } else {
          this.http.get<FavoriteTvshows>(this.BASE_URL.concat('v2/favorite/tvshows')).map(data => data.tvshows)
            .toPromise()
            .then(result => {
              this.tvshows = result;
              resolve(result);
            });
        }
      });
    }

    getFavoriteBooks(): Promise<FavoriteBook[]> {
      return new Promise(resolve => {
        if (this.books.length > 0) {
          resolve(this.books);
        } else {
          this.http.get<FavoriteBooks>(this.BASE_URL.concat('v2/favorite/litres-items')).map(data => data.litres)
          .toPromise()
          .then(result => {
            this.books = result;
            resolve(result);
          });
        }
      });
    }

    isBookFavorite(id: number): boolean {
      if (this.books.length > 0) {
        return this.books.some(book => book.litres_item_id === id);
      }
      return false;
    }

    isTvshowFavorite(id: string): boolean {
      if (this.tvshows.length > 0) {
        return this.tvshows.some(tvshow => tvshow.tvshow_id === id);
      }
      return false;
    }

    isChannelFavorite(id: number): boolean {
      if (this.channels.length > 0) {
        return this.channels.some(channel => channel.channel_id === id);
      }
      return false;
    }

    isVideoFavorite(id: number): boolean {
      if (this.videos.length > 0) {
        return this.videos.some(video => video.video_id === id);
      }
      return false;
    }

    removeFromFavorite(settings: FavoriteSettings) {
      const params: HttpParams = new HttpParams().set('id', settings.id.toString());
      this.http.delete(this.BASE_URL.concat('v2/favorite/', this.getRoute(settings.type)), { params })
        .toPromise().then(() => {
          if (settings.type === 'channel') {
            const item = this.channels.find(channel => channel.channel_id === settings.id);
            const index = this.channels.indexOf(item);
            this.channels.splice(index, 1);
          }
          if (settings.type === 'tv') {
            const item = this.tvshows.find(tvshow => tvshow.tvshow_id === settings.id);
            const index = this.tvshows.indexOf(item);
            this.tvshows.splice(index, 1);
          }
          if (settings.type === 'video') {
            const item = this.videos.find(video => video.video_id === settings.id);
            const index = this.videos.indexOf(item);
            this.videos.splice(index, 1);
          }
          if (settings.type === ContentName.BOOK) {
            const item = this.books.find(book => book.litres_item_id === settings.id);
            const index = this.books.indexOf(item);
            this.books.splice(index, 1);
          }
      });
    }

    private getRoute(type: string): string {
      switch (type) {
        case ContentName.TV:
          return 'tvshow';
        case ContentName.BOOK:
          return 'litres-item';
        default:
          return type;
      }
    }

    addToFavorite(settings: FavoriteSettings) {
      const formData: FormData = new FormData();
      formData.append('id', settings.id.toString());
      this.http.post(this.BASE_URL.concat('v2/favorite/', this.getRoute(settings.type)), formData)
      .toPromise()
      .then(() => {
        if (settings.type === 'channel') {
          const ch: FavoriteChannel = {
            channel_id: +settings.id,
            added_time: this.timeService.currentTime
          };
          this.channels.push(ch);
        }
        if (settings.type === 'tv') {
          const tv: FavoriteTvshow = {
            tvshow_id: settings.id.toString(),
            added_time: this.timeService.currentTime
          };
          this.tvshows.push(tv);
        }
        if (settings.type === 'video') {
          const video: FavoriteVideo = {
            video_id: +settings.id,
            added_time: this.timeService.currentTime
          };
          this.videos.push(video);
        }
        if (settings.type === 'book') {
          const book: FavoriteBook = {
            litres_item_id: +settings.id,
            added_time: this.timeService.currentTime
          };
          this.books.push(book);
        }
      });
    }

}

interface FavoriteChannels {
    channels: FavoriteChannel[];
}

interface FavoriteVideos {
    videos: FavoriteVideo[];
}

interface FavoriteTvshows {
    tvshows: FavoriteTvshow[];
}

interface FavoriteBooks {
  litres: FavoriteBook[];
}
