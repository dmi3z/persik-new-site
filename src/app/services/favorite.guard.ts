import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable()
export class FavoriteGuard implements CanActivate {

  private favoriteCount = 0;

  constructor(private authService: AuthService) {
    this.isHaveFavoriteContent();
  }

  canActivate() {
    const isLogin: boolean = this.authService.isLogin;
    if (isLogin) {
      if (this.favoriteCount > 0) {
        return true;
      }
      return false;
    }
    return false;
  }

  private isHaveFavoriteContent(): void {
    this.favoriteCount = 0;
  }
}
